// Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MPParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MPVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MPParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MPParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#decls}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecls(MPParser.DeclsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#varDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDecl(MPParser.VarDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#varList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarList(MPParser.VarListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#varName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarName(MPParser.VarNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#mpType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMpType(MPParser.MpTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#priType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPriType(MPParser.PriTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#compoType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompoType(MPParser.CompoTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#arrType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrType(MPParser.ArrTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#retType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRetType(MPParser.RetTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#funcDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDecl(MPParser.FuncDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#paramList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamList(MPParser.ParamListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#singleParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleParam(MPParser.SingleParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#procDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcDecl(MPParser.ProcDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#procMain}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcMain(MPParser.ProcMainContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#compoStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompoStmt(MPParser.CompoStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(MPParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(MPParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#ifStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStmt(MPParser.IfStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#ifElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElse(MPParser.IfElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#ifNoElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfNoElse(MPParser.IfNoElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#nonIfStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonIfStmt(MPParser.NonIfStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#whileDo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileDo(MPParser.WhileDoContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#forloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForloop(MPParser.ForloopContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#withDo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWithDo(MPParser.WithDoContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#retNoExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRetNoExp(MPParser.RetNoExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#retWExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRetWExp(MPParser.RetWExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#dataTypeLit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeLit(MPParser.DataTypeLitContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#funCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunCall(MPParser.FunCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link MPParser#funCalPara}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunCalPara(MPParser.FunCalParaContext ctx);
}