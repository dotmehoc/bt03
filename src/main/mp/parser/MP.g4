/**
 * Student name: Nguyễn Quốc Dũng
 * Student ID: 1770471
 */
grammar MP;

@lexer::header{from lexerror import *}
options{ language=Python3; }

program     : decls+ EOF ;
decls       : varDecl
            | funcDecl
            | procDecl;

varDecl     : VAR (varList SEMI)+;
varList     : ID (COMMA ID)* retType;

retType     : COLON priType
            | COLON compoType;

priType     : INT
            | STRING
            | REAL
            | BOOLEAN ;

compoType   : ARRAY LS SUB? IntLit TWODOTS SUB? IntLit RS OF priType;
//iconst      : SUB? IntLit;

funcDecl    : FUNC ID LB paramList RB retType SEMI varDecl* body retWExp?;
paramList   : (varList (SEMI varList)*)?;

procDecl    : PROC ID LB paramList RB SEMI varDecl* body retNoExp?;

body        : BEGIN (stmt)*  END;

expr        : LB expr RB
            | <assoc=right> expr LS expr RS
            | <assoc=right> ( NOT | SUB) expr
            | expr (MUL | DIVIDE | INTDIV | MOD | AND) expr
            | expr (ADD | SUB | OR) expr
            | expr (EQ | NEQ | LT | LTEQ | GT | GTEQ) expr
            | expr (ANDTH | ORELSE) expr
            | dataTypeLit | funcCall | ID ;

ifStmt      : ifElse
            | ifNoElse;

ifElse      : IF expr THEN (ifElse | stmt) ELSE (ifStmt | stmt);
ifNoElse    : IF expr THEN (ifStmt | stmt);

stmt        : ifStmt
            | asignStmt
            | whileDo
            | withDo
            | procCall SEMI
            | forloop
            | BREAK SEMI
            | CONTINUE SEMI
            | retNoExp
            | retWExp
            | expr SEMI
            | body;

asignStmt   : <assoc=right> (lhs ASIGN)+ expr SEMI;

lhs         : ID
            | expr LS expr RS;

whileDo     : WHILE expr DO stmt;
forloop     : FOR ID ASIGN expr (TO | DOWNTO) expr DO stmt;
withDo      : WITH paramList SEMI DO stmt;
retNoExp    : RETURN SEMI;
retWExp     : RETURN expr SEMI;

dataTypeLit : IntLit
            | RealLit
            | BoolLit
            | StrLit;

funcCall    : ID LB (expr(COMMA expr)*)? RB;
procCall    : ID LB (expr(COMMA expr)*)? RB;

fragment A  : [aA];
fragment B  : [bB];
fragment C  : [cC];
fragment D  : [dD];
fragment E  : [eE];
fragment F  : [fF];
fragment G  : [gG];
fragment H  : [hH];
fragment I  : [iI];
fragment J  : [jJ];
fragment K  : [kK];
fragment L  : [lL];
fragment M  : [mM];
fragment N  : [nN];
fragment O  : [oO];
fragment P  : [pP];
fragment Q  : [qQ];
fragment R  : [rR];
fragment S  : [sS];
fragment T  : [tT];
fragment U  : [uU];
fragment V  : [vV];
fragment W  : [wW];
fragment X  : [xX];
fragment Y  : [yY];
fragment Z  : [zZ];

fragment DIGIT      : [0-9];
fragment CHARACTER  : [a-zA-Z];

BREAK       : B R E A K ;
CONTINUE    : C O N T I N U E;
FOR         : F O R;
TO          : T O;
DOWNTO      : D O W N T O;
DO          : D O;
IF          : I F;
THEN        : T H E N;
ELSE        : E L S E;
RETURN      : R E T U R N;
WHILE       : W H I L E;
WITH        : W I T H;
BEGIN       : B E G I N;
END         : E N D;
FUNC        : F U N C T I O N;
PROC        : P R O C E D U R E;
VAR         : V A R;
OF          : O F;
//MAIN        : M A I N;

/* Types declaration */
BOOLEAN     : B O O L E A N;
REAL        : R E A L;
INT         : I N T E G E R;
STRING      : S T R I N G;
ARRAY       : A R R A Y;

/* Operators */
ADD         : '+';
SUB         : '-';
MUL         : '*';
DIVIDE      : '/';
NOT         : N O T;
MOD         : M O D;
OR          : O R;
AND         : A N D;
ORELSE      : O R [ ]+ E L S E;
ANDTH       : A N D [ ]+ T H E N;
EQ          : '=';
NEQ         : '<>';
LT          : '<';
GT          : '>';
LTEQ        : '<=';
GTEQ        : '>=';
INTDIV      : D I V;
ASIGN       : ':=';

/* Separators */
LS          : '[';
RS          : ']';
LB          : '(';
RB          : ')';
SEMI        : ';';
COMMA       : ',';
COLON       : ':';
TWODOTS     : ' .. ';
LCMT        : '(*' ;
RCMT        : '*)';
LP          : '{' ;
RP          : '}' ;

/* Literals */
IntLit      : DIGIT+;
RealLit     : DIGIT+ '.' DIGIT*
            | DIGIT* '.' DIGIT+
            | DIGIT+ ('e'|'E') SUB? DIGIT+
            | DIGIT* '.' DIGIT+ ('e'|'E') SUB? DIGIT+ ;
BoolLit     : [Tt][Rr][Uu][Ee] | [Ff][Aa][Ll][Ss][Ee];
StrLit      : '"' (~[\b\f\r\n\t'"\\]  | '\\'[bfrnt"'\\])* '"'{self.text=self.text[1:-1]} ;

/* Comments */
BLOCKCMT1   : '(*' .*? '*)' -> skip;
BLOCKCMT2   : '{' .*? '}' -> skip ;
LINECMT     : '//' ~[\r\n]* -> skip;
WS          : [ \t\r\n\b\f]+ -> skip ;

/* Identifier */
ID          : ('_'|CHARACTER)(DIGIT|'_'|CHARACTER)*;

ILLEGAL_CHAR    :  '"' ~[\r\n"]* ['\t] ~[\r\n"]*'"' {
pos = tab = self.text.find('\t')
sq = self.text.find('\'')
if tab == -1:
    pos = sq
raise IllegalCharInString(self.text[1: pos +1])};

ILLEGAL_ESCAPE  : '"' ~[\r\n"]* '\\' ~[bfrnt"\\]* '"' {
slash = self.text.find('\\')
raise IllegalEscapeInString(self.text[1: slash+2])};

UNCLOSE_STRING  : '"' (~'"')* {raise UnclosedString(self.text[1:])};
ERROR_CHAR      : . {raise ErrorToken(self.text)};