'''
Student name: Nguyễn Quốc Dũng
Student ID: 1770470
'''
from AST import *
from Visitor import *
from Utils import Utils
from StaticError import *
import copy

class MType:
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype

class Symbol:
    def __init__(self,name,mtype,value = None):
        self.name = name
        self.mtype = mtype
        self.value = value
    
class StaticChecker(BaseVisitor,Utils):

    global_envi = [Symbol('getint',MType([],IntType())),
                   Symbol('putintln',MType([IntType()],VoidType())),
                   Symbol('putint', MType([IntType()], VoidType())),
                   Symbol('getfloat', MType([], FloatType())),
                   Symbol('putfloat', MType([FloatType()], VoidType())),
                   Symbol('putfloatln', MType([FloatType()], VoidType())),
                   Symbol('putbool', MType([BoolType()], VoidType())),
                   Symbol('putboolln', MType([BoolType()], VoidType())),
                   Symbol('putstring', MType([StringType()], VoidType())),
                   Symbol('putstringln', MType([StringType()], VoidType())),
                   Symbol('putln', MType([], VoidType()))]
    
    def __init__(self,ast):
        self.ast = ast

    def check(self):
        return self.visit(self.ast,StaticChecker.global_envi)

# ------------------------------------------------------------------ #
    def visitProgram(self,ast, env):
        new_env = env.copy()    #env[0]
        retTyp = None           #env[1]
        inLoop = False          #env[2]
        currFunc = {}           #env[3]
        chkMain = False         #env[4]
        chkEntry = False

        for i in ast.decl:
            new_env.append(chkRedecl(new_env, i, Variable() if type(i) is VarDecl 
                                                            else Procedure()
                                                                if type(i.returnType) is VoidType
                                                                else Function()))
            if type(i) is FuncDecl:
                chkEntry = i.name.name == 'main' if not chkEntry else chkEntry
                currFunc[i.name.name.lower()] = i.name.name == 'main'

        if not chkEntry:
            raise NoEntryPoint()

        for i in ast.decl:
            if not type(i) is VarDecl:
                self.visit(i, (new_env, retTyp, inLoop, currFunc, chkMain))

# ------------------------------------------------------------------ #
    def visitFuncDecl(self, ast, env):
        new_env = env[0].copy()
        retTyp = ast.returnType
        chkMain = env[4]
        chkEnd = 0
        if ast.name.name == 'main':
            chkMain = True

        locallst = []
        [self.visit(param, (locallst, Parameter(), new_env)) for param in ast.param]
        [self.visit(local, (locallst, Variable(), new_env)) for local in ast.local]
        
        envlst =  new_env + locallst
        
        chkEnd = self.visitCompStmt(ast.body, (envlst, retTyp, env[2], env[3], chkMain))
        if chkEnd == 1 and not type(retTyp) is VoidType:
            raise FunctionNotReturn(ast.name.name)

# ------------------------------------------------------------------ #
    def visitVarDecl(self, ast, env):
        locallst = env[0]
        para = env[1]
        rs = locallst.append(chkRedecl(locallst, ast, para))
        return rs

    ##------------------------------------------------------------------#
    def visitCompStmt(self, lst, env):
        [self.visit(stmt, env) for stmt in lst]

# ------------------------------------------------------------------ #
    def visitCallStmt(self, ast, env):
        rs = self.visitStmtBody(ast, (env[0], env[1], env[2], env[3], env[4], Procedure()))
        return rs

# ------------------------------------------------------------------ #
    def visitCallExpr(self, ast, env):
        rs = self.visitStmtBody(ast, (env[0], env[1], env[2], env[3], env[4], Function()))
        return rs

# ------------------------------------------------------------------ #
    def visitStmtBody(self, ast, env):
        new_env = env[0].copy()
        currFunc = env[3]
        chkMain = env[4]
        altertyp = env[5]

        paralst = [self.visit(x, (new_env, env[1], env[2], env[3], env[4])) for x in ast.param]
        chkFun = self.lookup(ast.method.name.lower(), new_env, lambda x: x.name.lower())

        if chkFun is None or not type(chkFun.mtype) is MType:
            raise Undeclared(altertyp, ast.method.name)

        if (not type(chkFun.mtype.rettype) is VoidType and type(altertyp) is Procedure) \
                or (type(chkFun.mtype.rettype) is VoidType and type(altertyp) is Function):
            raise Undeclared(altertyp, ast.method.name)

        if len(chkFun.mtype.partype) != len(paralst):
            raise TypeMismatchInStatement(ast)

        if not all([chkValidTyp(chkFun.mtype.partype[i], paralst[i]) for i in range(len(paralst))]):
            raise TypeMismatchInStatement(ast)

        if chkMain:
            currFunc[ast.method.name.lower()] = True

        return chkFun.mtype.rettype

# ------------------------------------------------------------------ #
    def visitIf(self, ast, env):
        new_env = env
        exp = self.visit(ast.expr, new_env)
        if type(exp) != BoolType:
            raise TypeMismatchInStatement(ast)
        else:
            [self.visit(x, new_env) for x in ast.thenStmt]
            if ast.elseStmt != None:
                [self.visit(e, new_env) for e in ast.elseStmt]

##------------------------------------------------------------------#
    def visitId(self, ast, env):
        new_env = env[0]
        chkid = self.lookup(ast.name.lower(), new_env, lambda x: x.name.lower())
        if chkid is None:
            raise Undeclared(Identifier(), ast.name)
        else:
            return chkid.mtype

# ------------------------------------------------------------------ #
    def visitAssign(self, ast, env):
        new_env = env[0].copy()
        
        if not type(ast.lhs) in [Id, ArrayCell]:
            raise TypeMismatchInStatement(ast)
        lhsTyp = self.visit(ast.lhs, (new_env, env[1], env[2], env[3], env[4]))
        rhsTyp = self.visit(ast.exp, (new_env, env[1], env[2], env[3], env[4]))
        if not chkAssign(lhsTyp, rhsTyp):
            raise TypeMismatchInStatement(ast)
        return 0

# ------------------------------------------------------------------ #
    def visitFor(self, ast, env):
        new_env = env[0].copy()
        rTyp = env[1]

        idn = self.visit(ast.id, (new_env, env[1], env[2], env[3], env[4]))
        if not type(idn) is IntType:     raise TypeMismatchInStatement(ast)

        exp1 = self.visit(ast.expr1, (new_env, env[1], env[2], env[3], env[4]))
        if not type(exp1) is IntType:   raise TypeMismatchInStatement(ast)

        exp2 = self.visit(ast.expr2, (new_env, env[1], env[2], env[3], env[4]))
        if not type(exp2) is IntType:   raise TypeMismatchInStatement(ast)

        chkEnd = 0
        for stmt in ast.loop:
            chkEnd = self.visit(stmt, (new_env, env[1], True, env[3], env[4]))
        chkEnd = self.visitCompStmt(self, ast.loop, env)
        if chkEnd == 1 and not type(rTyp) is VoidType:
            raise FunctionNotReturn(ast.name.name)
        return 0
# ------------------------------------------------------------------ #
    def visitWith(self, ast, env):
        new_env = env
        lstdec = []
        lstdec = lstdec + [self.visit(x, lstdec) for x in ast.decl]

        rsLst = lstdec + new_env
        [self.visit(x, rsLst) for x in ast.stmt]

# ------------------------------------------------------------------#
    def visitWhile(self, ast, env):
        expw = self.visit(ast.exp, env)
        if not type(expw) is BoolType():   raise TypeMismatchInStatement(ast)
        [self.visit(x, env[0]) for x in ast.sl]

# ------------------------------------------------------------------ #
    def visitBinaryOp(self, ast, env):
        new_env = env[0].copy()
        
        lhs = self.visit(ast.left, (new_env, env[1], env[2], env[3], env[4]))
        rhs = self.visit(ast.right, (new_env, env[1], env[2], env[3], env[4]))
        op = ast.op
        rtype = chkMatchTyp(lhs, rhs)

        if op in ['+', '-', '*']:
            if rtype is None:   raise TypeMismatchInExpression(ast)
            return rtype
            
        if op == '/':
            if rtype is None:   raise TypeMismatchInExpression(ast)
            return FloatType()
            
        if op in ['<', '>', '<=', '>=', '=', '<>']:
            if rtype is None:   raise TypeMismatchInExpression(ast)
            return BoolType()
            
        if op in ['and', 'or', 'andthen', 'orelse']:
            if (type(lhs), type(rhs)) == (BoolType, BoolType):  return BoolType()
            raise TypeMismatchInExpression(ast)
            
        if op in ['div', 'mod']:
            if (type(lhs), type(rhs)) == (IntType, IntType):    return IntType()
            raise TypeMismatchInExpression(ast)
            
        raise TypeMismatchInExpression(ast)
# ------------------------------------------------------------------ #
    def visitUnaryOp(self, ast, env):
        new_env = env[0].copy()
        exp = self.visit(ast.body, (new_env, env[1], env[2], env[3], env[4]))
        op = ast.op

        if op == '-':
            if type(exp) in [IntType, FloatType]:   return exp
            raise TypeMismatchInExpression(ast)
        if op == 'not':
            if type(exp) is BoolType:               return BoolType()
            raise TypeMismatchInExpression(ast)
        
        raise TypeMismatchInExpression(ast)

# ------------------------------------------------------------------ #
    def visitArrayCell(self, ast, env):
        new_env = env[0].copy()
        retTyp = env[1]
        inLoop = env[2]
        currFunc = env[3]
        chkMain = env[4]

        if not type(ast.arr) is Id and not type(ast.arr) is CallExpr:
            raise TypeMismatchInExpression(ast)
        
        chkArr = self.lookup(ast.arr.name.lower() if type(ast.arr) is Id
                                                  else ast.arr.method.name,
                             new_env, lambda x: x.name.lower())
        if chkArr is None:
            if type(ast.arr) is Id:
                raise Undeclared(Identifier(), ast.arr.name)
            else:
                raise Undeclared(Function(), ast.arr.method.name)
        
        idxlst = self.visit(ast.idx, (new_env, retTyp, inLoop, currFunc, chkMain))
        
        if not type(idxlst) is IntType:
            raise TypeMismatchInExpression(ast)
        
        if type(chkArr.mtype) is MType:
            if chkMain:
                currFunc[chkArr.name.lower()] = True
            return chkArr.mtype.rettype.eleType
        
        if not type(chkArr.mtype) is ArrayType:
            raise TypeMismatchInExpression(ast)
        
        return chkArr.mtype.eleType
# ------------------------------------------------------------------ #
    def visitBreak(self, ast, env):
        if not env[2]:
            raise BreakNotInLoop()
        return env
# ------------------------------------------------------------------ #
    def visitContinue(self, ast, env):
        if not env[2]:
            raise ContinueNotInLoop()
        return env

# ------------------------------------------------------------------ #
    def visitReturn(self, ast, env):
        new_env = env[0].copy()
        retTyp = env[1]
        exp = self.visit(ast.expr, (new_env, env[1], env[2], env[3], env[4]))

        if ast.expr is None:
            if type(retTyp) is VoidType:
                return 1
            raise TypeMismatchInStatement(ast)
        if chkValidTyp(retTyp, exp):
            return 1
        else:
            raise TypeMismatchInStatement(ast)

# ------------------------------------------------------------------ #
    def visitIntLiteral(self,ast, env):         return IntType()
    def visitFloatLiteral(self, ast, env):      return FloatType()
    def visitStringLiteral(self, ast, env):     return StringType()
    def visitBooleanLiteral(self, ast, env):    return BoolType()

    def visitIntType(self, ast, env):           return IntType()
    def visitFloatType(self, ast, env):         return FloatType()
    def visitStringType(self, ast, env):        return StringType()
    def visitBoolType(self, ast, env):          return BoolType()
    def visitVoidType(self, ast, env):          return VoidType()
    def visitArrayType(self, ast, env):         return ArrayType(ast.lower, ast.upper, ast.eleType)

# ------------------------------------------------------------------ #
# ------------------------------------------------------------------ #
def chkRedecl(lstDecl, decl, kind):
    if type(kind) in [Parameter, Variable]:
        if any(x.name == decl.variable.name.lower() for x in lstDecl):
            raise Redeclared(kind, decl.variable.name)
        return Symbol(decl.variable.name.lower(), decl.varType)

    if any(dec.name.lower() == decl.name.name.lower() for dec in lstDecl):
        raise Redeclared(kind, decl.name.name)
    return Symbol(decl.name.name.lower(), MType([x.varType for x in decl.param], decl.returnType))
# ------------------------------------------------------------------ #
def chkAssign(lhs, rhs):
    if type(lhs) is VoidType or type(rhs) is VoidType:      return False
    if type(lhs) is ArrayType or type(rhs) is ArrayType:    return False
    if type(lhs) is StringType or type(rhs) is StringType:  return False
    rs = [(BoolType, BoolType),
          (FloatType, FloatType),
          (FloatType, IntType),
          (IntType, IntType)]
    return (type(lhs), type(rhs)) in rs
# ------------------------------------------------------------------ #
def chkValidTyp(lhs, rhs):
    if type(lhs) is VoidType or type(rhs) is VoidType:
        return False
    if type(lhs) is ArrayType and type(rhs) is ArrayType:
        if lhs.lower != rhs.lower or lhs.upper != rhs.upper or type(lhs.eleType) != type(rhs.eleType):
            return False
        return True
    rs = [(StringType, StringType),
          (BoolType, BoolType),
          (FloatType, FloatType),
          (FloatType, IntType),
          (IntType, IntType)]
    return (type(lhs), type(rhs)) in rs

# ------------------------------------------------------------------ #
def chkMatchTyp(lhs, rhs):
    if (type(lhs), type(rhs)) == (IntType, IntType):        return IntType()
    if (type(lhs), type(rhs)) in [(IntType, FloatType),
                                  (FloatType, IntType),
                                  (FloatType, FloatType)]:  return FloatType()

# ------------------------------------------------------------------ #