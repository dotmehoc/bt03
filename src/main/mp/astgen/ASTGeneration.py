# Student name: Nguyễn Quốc Dũng
# Student ID: 1770471

from MPVisitor import MPVisitor
from MPParser import MPParser
from AST import *
from collections import OrderedDict
import ast
from itertools import chain
import numpy as np
from Utils import *

class ASTGeneration(MPVisitor):
    def visitRetType(self, ctx: MPParser.RetTypeContext):
        if ctx.priType() != None:       return self.visit(ctx.priType())
        elif ctx.compoType() != None:   return self.visit(ctx.compoType())
        else:                           return None

    def visitPriType(self, ctx: MPParser.PriTypeContext):
        if ctx.INT() != None:           return IntType()
        elif ctx.REAL() != None:        return FloatType()
        elif ctx.STRING() != None:      return StringType()
        else:                           return BoolType()

    def visitCompoType(self, ctx: MPParser.CompoTypeContext):
        low = self.visitExpr(ctx.getChild(2))
        up = self.visitExpr(ctx.getChild(4))
        elTyp = self.visit(ctx.priType())
        return ArrayType(low, up, elTyp)

    def visitProgram(self,ctx:MPParser.ProgramContext):
        lstdec = [self.visit(i) for i in ctx.decls()]
        flstdec = MyFlatLst.to_flatList(lstdec)     # khử các list trả về bị lồng nhau
        return Program(flstdec)

    def visitVarDecl(self, ctx: MPParser.VarDeclContext):
        retlst = [self.visit(v) for v in ctx.varList()]
        fretlst = MyFlatLst.to_flatList(retlst)
        return fretlst

    def visitVarList(self, ctx: MPParser.VarListContext):
        vlst = []
        vtyp = self.visit(ctx.retType())
        for vname in ctx.ID():
            vdec = VarDecl(Id(vname.getText()), vtyp)
            vlst.append(vdec)
        fvlst = MyFlatLst.to_flatList(vlst)
        return fvlst

    def visitParamList(self, ctx: MPParser.ParamListContext):
        paralst = []
        if ctx.varList() == None:
            return paralst
        else:
            paralst = [self.visit(p) for p in ctx.varList()]
            return MyFlatLst.to_flatList(paralst)

    def visitProcDecl(self, ctx: MPParser.ProcDeclContext):
        ID = Id(ctx.ID().getText())
        param = [self.visit(ctx.paramList())]
        fparam = MyFlatLst.to_flatList(param)
        local = [self.visit(x) for x in ctx.varDecl()]
        flocal = MyFlatLst.to_flatList(local)
        body = self.visit(ctx.body())
        return FuncDecl(ID, fparam, flocal, body)

    def visitFuncDecl(self, ctx: MPParser.FuncDeclContext):
        ID = Id(ctx.ID().getText())
        param = self.visit(ctx.paramList())
        fparam = MyFlatLst.to_flatList(param)
        retType = self.visit(ctx.retType())
        local = [self.visit(x) for x in ctx.varDecl()]
        flocal = MyFlatLst.to_flatList(local)
        body = self.visit(ctx.body())
        return FuncDecl(ID, fparam, flocal, body, retType)

    def visitStmt(self, ctx: MPParser.StmtContext):
        if ctx.BREAK() != None:         return [Break()]
        elif ctx.CONTINUE() != None:    return [Continue()]
        else:                           return [self.visit(ctx.getChild(0))]

    def visitAsignStmt(self, ctx: MPParser.AsignStmtContext):
        expstmt = self.visit(ctx.expr())
        lstLHS = [self.visit(lh) for lh in ctx.lhs()]
        result =  []
        rhs = [Assign(lstLHS[-1],expstmt)]
        pairs = [[lstLHS[i], lstLHS[i + 1]] for i in range(len(lstLHS) - 1)]
        for [curr_item, next_item] in pairs:
            result.append(Assign(curr_item, next_item))
        return MyFlatLst.to_flatList(reversed(result + rhs))

    def visitLhs(self, ctx: MPParser.LhsContext):
        if ctx.getChildCount() == 1:
            return Id(ctx.ID().getText())
        if ctx.getChildCount() == 4:
            exp1 = self.visit(ctx.getChild(0))
            exp2 = self.visit(ctx.getChild(2))
            return ArrayCell(exp1, exp2)

    def visitIfStmt(self, ctx: MPParser.IfStmtContext):
        return self.visit(ctx.getChild(0))

    def visitIfElse(self, ctx: MPParser.IfElseContext):
        ifexp = self.visit(ctx.expr())
        thenst = [self.visit(ctx.getChild(3))]
        fthenst = MyFlatLst.to_flatList(thenst)
        elstmt = [self.visit(ctx.getChild(5))]
        felstmt = MyFlatLst.to_flatList(elstmt)
        return If(ifexp, fthenst, felstmt)

    def visitIfNoElse(self, ctx: MPParser.IfNoElseContext):
        ifexp = self.visit(ctx.expr())
        thenst = [self.visit(ctx.getChild(3))]
        fthenst = MyFlatLst.to_flatList(thenst)
        return If(ifexp, fthenst)

    def visitWhileDo(self, ctx: MPParser.WhileDoContext):
        whexp = self.visit(ctx.expr())
        whstmt = self.visit(ctx.stmt())
        fwhstmt = MyFlatLst.to_flatList(whstmt)
        return While(whexp, fwhstmt)

    def visitForloop(self, ctx: MPParser.ForloopContext):
        uploop = True
        if ctx.TO():            uploop
        elif ctx.DOWNTO():      uploop = False
        foname = Id(str(ctx.ID()))
        foexp1 = self.visit(ctx.expr(0))
        foexp2 = self.visit(ctx.expr(1))
        fostmt = self.visit(ctx.stmt())
        ffostmt = MyFlatLst.to_flatList(fostmt)
        return For(foname, foexp1, foexp2, uploop, ffostmt)

    def visitWithDo(self, ctx: MPParser.WithDoContext):
        wipara = self.visit(ctx.paramList())
        wistmt = [self.visit(ctx.stmt())]
        fwistmt = MyFlatLst.to_flatList(wistmt)
        return With(wipara, fwistmt)

    def visitFuncCall(self, ctx: MPParser.FuncCallContext):
        ID = Id(ctx.ID().getText())
        para = [self.visit(p) for p in ctx.expr()]
        return CallExpr(ID,para)

    def visitProcCall(self, ctx: MPParser.ProcCallContext):
        ID = Id(ctx.ID().getText())
        para = [self.visit(p) for p in ctx.expr()]
        return CallStmt(ID, para)

    def visitBody(self, ctx: MPParser.BodyContext):
        bodylst = []
        if ctx.getChildCount() >= 3:
            bodylst = [self.visit(i) for i in ctx.stmt()]
            fbodylst = MyFlatLst.to_flatList(bodylst)
            return fbodylst
        else:
             return bodylst

    def visitExpr(self, ctx: MPParser.ExprContext):
        if ctx.getChildCount() == 1:
            if ctx.ID() != None:
                return Id(ctx.ID().getText())
            elif ctx.funcCall() != None:
                return self.visit(ctx.funcCall())
            elif ctx.dataTypeLit() != None:
                return self.visit(ctx.dataTypeLit())
            else: return None
        elif ctx.getChildCount() == 2:
            return UnaryOp(ctx.getChild(0).getText(), self.visit(ctx.getChild(1)))
        elif ctx.getChildCount() == 3 and isinstance(ctx.getChild(1), MPParser.ExprContext):
            return self.visit(ctx.getChild(1))
        elif ctx.getChildCount() == 3:
            if ctx.getChild(1).getText() == 'and then'.lower():
                ope = 'andthen'
            elif ctx.getChild(1).getText() == 'or else'.lower():
                ope = 'orelse'
            else:
                ope = ctx.getChild(1).getText()
            left = self.visit(ctx.getChild(0))
            right = self.visit(ctx.getChild(2))
            return BinaryOp(ope,left,right)
        elif ctx.getChildCount() == 4:
            id = self.visit(ctx.getChild(0))
            index = self.visit(ctx.getChild(2))
            return ArrayCell(id, index)

    def visitRetNoExp(self, ctx: MPParser.RetNoExpContext):
        return Return("None")

    def visitRetWExp(self, ctx: MPParser.RetWExpContext):
        return Return(self.visitExpr(ctx.expr()))

    def visitDataTypeLit(self, ctx: MPParser.DataTypeLitContext):
        if ctx.IntLit() != None:        return IntLiteral(ctx.IntLit().getText())
        elif ctx.RealLit() != None:     return FloatLiteral(ctx.RealLit().getText())
        elif ctx.StrLit() != None:      return StringLiteral(ctx.StrLit().getText())
        else:                           return BooleanLiteral(ctx.BoolLit())

##########################################
# Hàm dùng để flatting list (khử list lồng nhau)
class MyFlatLst:
    def to_flatList(lst, any=None):
        if any is None:
            any = []

        for i in lst:
            if isinstance(i, list):
                MyFlatLst.to_flatList(i, any)
            else:
                any.append(i)
        return any

