 function foo(): boolean;
	                begin
	                    return 2>1;
	                end
	                function foo1(a:integer): boolean;
	                begin
	                    return a=1;
	                end
	                function foo2(a:integer): boolean;
	                begin
	                    return a;
	                end
	                procedure main();
	                begin
	                    foo(); foo1(1); foo2(2);
	                    return;
	                end 