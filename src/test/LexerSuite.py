# Student name: Nguyễn Quốc Dũng
# Student ID: 1770471

import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
      
    # def test_lowercase_identifier(self):
    #     """test identifiers"""
    #     self.assertTrue(TestLexer.test("abc","abc,<EOF>",101))
    # def test_lower_upper_id(self):
    #     self.assertTrue(TestLexer.test("aCBbdc","aCBbdc,<EOF>",102))
    # def test_mixed_id(self):
    #     self.assertTrue(TestLexer.test("aAsVN3","aAsVN3,<EOF>",103))
    # def test_integer(self):
    #     """test integers"""
    #     self.assertTrue(TestLexer.test("123a123","123,a123,<EOF>",104))
    # def test_error_token(self):
    #     self.assertTrue(TestLexer.test("abc?def","abc,Error Token ?",105))
##########################################################################
# My testcases
    def test_01_invalidID(self):
        self.assertTrue(TestLexer.test("$_1abc23","Error Token $",106))
    def test_02_complexID(self):
        self.assertTrue(TestLexer.test("_abc_Abc123def456__ABC","_abc_Abc123def456__ABC,<EOF>""",107))

    def test_03_line_comment(self):
        self.assertTrue(TestLexer.test("//this is a comment out of line,it will be skipped","<EOF>",108))
    def test_04_block_comment(self):
        self.assertTrue(TestLexer.test("""(*this is a comment out of block,
                                      it will be skipped *) 123a""","123,a,<EOF>",109))
    def test_05_blockcomment2(self):
        self.assertTrue(TestLexer.test("""{this is a comment out of block,
                                       it will be skipped }345b""","345,b,<EOF>",110))
    def test_06_unclose_blockcomment(self):
        self.assertTrue(TestLexer.test("{ missing closing comment out of block,not skip","{,missing,closing,comment,out,of,block,,,not,skip,<EOF>",111))
    def test_07_unclose_blockcomment2(self):
        self.assertTrue(TestLexer.test("(* missing closing comment out of block,not skip ","(*,missing,closing,comment,out,of,block,,,not,skip,<EOF>",112))
    def test_08_no_meaning_in_comment(self):
        self.assertTrue(TestLexer.test("//these {} and (* ...*) will be ignored in comment line","<EOF>",113))

    def test_09_keywork(self):
        self.assertTrue(TestLexer.test("If elSe for break do while continue","If,elSe,for,break,do,while,continue,<EOF>",114))
    # def test_10_keywords_2(self):
        self.assertTrue(TestLexer.test("Function Procedure Var True Then False Of", "Function,Procedure,Var,True,Then,False,Of,<EOF>", 115))
    def test_10_keyword_type(self):
        self.assertTrue(TestLexer.test("boolean integer real string array","boolean,integer,real,string,array,<EOF>", 116))

    def test_11_operators(self):
        self.assertTrue(TestLexer.test("a + b","a,+,b,<EOF>", 117))
    def test_12_operators2(self):
        self.assertTrue(TestLexer.test("6 mod 4", "6,mod,4,<EOF>", 118))
    def test_13_divideInteger(self):
        self.assertTrue(TestLexer.test("100 div 10","100,div,10,<EOF>",119))
    def test_14_operators_general(self):
        self.assertTrue(TestLexer.test("- * / not or and or else and then div <> = > < >= <=", "-,*,/,not,or,and,or else,and then,div,<>,=,>,<,>=,<=,<EOF>", 120))
    def test_15_operators_specific(self):
        self.assertTrue(TestLexer.test("a or b = c", "a,or,b,=,c,<EOF>", 121))
    def test_16_operators_bracket(self):
        self.assertTrue(TestLexer.test("a*(b+c/(d+c))","a,*,(,b,+,c,/,(,d,+,c,),),<EOF>", 122))
    def test_17_realNumber(self):
        self.assertTrue(TestLexer.test("x := 1.50","x,:=,1.50,<EOF>",123))
    def test_18_valid_realNumber2(self):
        self.assertTrue(TestLexer.test("x:=1.2 y:=1. z:=0.1 pi:=3.1416","x,:=,1.2,y,:=,1.,z,:=,0.1,pi,:=,3.1416,<EOF>",124))
    def test_19_valid_realNumber3(self):
        self.assertTrue(TestLexer.test("x:=1e2 y:=1.2E-2 z:=0.1e2 r:=.3E4 t:=12E8 u:=0.33E-3 v:=128e-42","x,:=,1e2,y,:=,1.2E-2,z,:=,0.1e2,r,:=,.3E4,t,:=,12E8,u,:=,0.33E-3,v,:=,128e-42,<EOF>",125))
    def test_20_invalid_realNumber(self):
        self.assertTrue(TestLexer.test("x = e-12","x,=,e,-,12,<EOF>",126))
    def test_21_invalid_realNumber2(self):
        self.assertTrue(TestLexer.test("x = 143e","x,=,143,e,<EOF>",127))


    def test_22_stringlit(self):
        self.assertTrue(TestLexer.test("""s = "I am dummy coder" """, """s,=,I am dummy coder,<EOF>""",128))
    def test_23_Skip_characters(self):
        self.assertTrue(TestLexer.test("a\b\f\n\r", """a,<EOF>""",129))
    def test_24_Escape_string(self):
        self.assertTrue(TestLexer.test("""abc\nDe""", """abc,De,<EOF>""",130))
    def test_25_Escape_string2(self):
        self.assertTrue(TestLexer.test(""" Let\bs learn Python""", """Let,s,learn,Python,<EOF>""", 131))
    def test_26_ID_tokens(self):
        self.assertTrue(TestLexer.test("foo(2)[3+x] := a[b[2]] +3; ", """foo,(,2,),[,3,+,x,],:=,a,[,b,[,2,],],+,3,;,<EOF>""",132))
    def test_27_insensitive_case(self):
        self.assertTrue(TestLexer.test("IF if iF If THEN then THEn tHEN THen thEN Then theN ","IF,if,iF,If,THEN,then,THEn,tHEN,THen,thEN,Then,theN,<EOF>",133))
    def test_28_insensitive_case2(self):
        self.assertTrue(TestLexer.test("""VAR VAr Var var vaR vAR
                        RETURN RETURn RETUrn RETurn REturn Return
                        return returN retuRN retURN reTURN rETURN
                        REtuRN reTUrn rETURn rETUrn""","VAR,VAr,Var,var,vaR,vAR,RETURN,RETURn,RETUrn,RETurn,REturn,Return,return,returN,retuRN,retURN,reTURN,rETURN,REtuRN,reTUrn,rETURn,rETUrn,<EOF>",134))
    def test_29_Assignment_token(self):
        self.assertTrue(TestLexer.test(""" a := b[10] := foo()[3] := x := 1;""", """a,:=,b,[,10,],:=,foo,(,),[,3,],:=,x,:=,1,;,<EOF>""",135))

    def test_30_some_builtin_function(self):
        self.assertTrue(TestLexer.test(""" putFloat(7.55);
                                            putFloatLn(12E3);
                                            putBool(true);
                                            putBoolLn(false);""",
                                       """putFloat,(,7.55,),;,putFloatLn,(,12E3,),;,putBool,(,true,),;,putBoolLn,(,false,),;,<EOF>""", 136))

    def test_31_accept_escape1(self):
        self.assertTrue(TestLexer.test(""" "\\b " """, """\\b ,<EOF>""", 137))
    def test_32_accept_escape2(self):
        self.assertTrue(TestLexer.test(""" "\\f " """, """\\f ,<EOF>""", 138))
    def test_33_accept_escape3(self):
        self.assertTrue(TestLexer.test(""" "\\r " """, """\\r ,<EOF>""", 139))
    def test_34_accept_escape4(self):
        self.assertTrue(TestLexer.test(""" "\\n " """, """\\n ,<EOF>""", 140))
    def test_35_accept_escape5(self):
        self.assertTrue(TestLexer.test(""" "\\t " """, """\\t ,<EOF>""", 141))
    def test_36_accept_escape6(self):
        self.assertTrue(TestLexer.test(""" " \\' " """, """ \\' ,<EOF>""", 142))
    def test_37_accept_escape7(self):
        self.assertTrue(TestLexer.test(""" " \\" " """, """ \\" ,<EOF>""", 143))
    def test_38_accept_escape8(self):
        self.assertTrue(TestLexer.test(""" "\\"ab\\"\\"c\\"" """, """\\"ab\\"\\"c\\",<EOF>""", 144))
    def test_39_accept_other_escape8(self):
        self.assertTrue(TestLexer.test(""" "I \\'hate\\' Python" """, """I \\'hate\\' Python,<EOF>""", 145))

    def test_40_Illegal_Char(self):
        self.assertTrue(TestLexer.test(""" "12adc'abc" """, """Illegal Char In String: 12adc'""", 146))
    def test_41_Illegal_Char(self):
        self.assertTrue(TestLexer.test(""" "12adc\tabc" """, """Illegal Char In String: 12adc	""", 147))
    def test_42_error1(self):
        self.assertTrue(TestLexer.test(""" " " " """, """ ,Unclosed String:  """, 148))
    def test_43_error2(self):
        self.assertTrue(TestLexer.test(""" "\\i" """, """Illegal Escape In String: \\i""", 149))
    def test_44_error3(self):
        self.assertTrue(TestLexer.test(""" "text = python\\good" """, """Illegal Escape In String: text = python\\g""", 150))
    def test_45_error4(self):
        self.assertTrue(TestLexer.test(""" "Python is suck """, """Unclosed String: Python is suck """, 151))

    def test_46_Error_Token(self):
        self.assertTrue(TestLexer.test("abcd \\n ef", """abcd,Error Token \\""", 152))
    def test_47_Unclosed_string(self):
        self.assertTrue(TestLexer.test(""" "Python """, """Unclosed String: Python """, 153))
    def test_48_error_signed_integer(self):
        self.assertTrue(TestLexer.test(" !-25", "Error Token !", 154))
    def test_49_error_token_not_inString(self):
        self.assertTrue(TestLexer.test(""" Tuition is $400 US ""","Tuition,is,Error Token $", 155))
    def test_50_accept_error_token_in_string(self):
        self.assertTrue(TestLexer.test(""" "Tuition is $400 US" ""","""Tuition is $400 US,<EOF>""", 156))

    def test_51_error_Token_not_inString(self):
        self.assertTrue(TestLexer.test("""a:= My name's Piton""","""a,:=,My,name,Error Token '""", 157))
    def test_52_error_Char_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My name's Piton" ""","""Illegal Char In String: a:= My name'""", 158))

    def test_53_escape_sqn_backspace_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\b Piton" ""","""Unclosed String: a:= My names Piton""", 159))
    def test_54_escape_sqn_formfeed_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\f Piton" ""","""Unclosed String: a:= My names Piton""", 160))
    def test_55_escape_sqn_carriage_return_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\r Piton" ""","""Unclosed String: a:= My names
 Piton""", 161))
    def test_56_escape_sqn_newline_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\n Piton" ""","""Unclosed String: a:= My names

 Piton""", 162))
    def test_57_escape_sqn_hrz_tab_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\t Piton" ""","""Illegal Char In String: a:= My names	""", 163))
    def test_58_escape_single_quote_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\' Piton" ""","""Illegal Char In String: a:= My names'""", 164))
    def test_59_escape_double_quote_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\" Piton" ""","""a:= My names,Piton,Unclosed String:  """, 165))
    def test_60_escape_backslash_inString(self):
        self.assertTrue(TestLexer.test(""" "a:= My names\\ Piton" ""","""Unclosed String: a:= My names\ Piton""", 166))

    def test_61_for_checking_not_official(self):
        self.assertTrue(TestLexer.test(""" "text = abc\\de" """, """Illegal Escape In String: text = abc\d""", 167))
