import unittest
from TestUtils import TestChecker
from AST import *

class CheckerSuite(unittest.TestCase):
    def test_00_legal_declare(self):
        input = """var a: integer; 
                    procedure main(); begin  end"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input,expect,400))

    def test_01_CheckMain_entry(self):
        input = """var a : integer;
                    procedure foo(); begin end"""
        expect = "No entry point"
        self.assertTrue(TestChecker.test(input,expect,401))

    def test_02_redeclared_variables(self):
        input = """procedure main(); var a, b: integer; var a: real; begin end"""
        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input,expect,402))

    def test_03_redeclared_variables2(self):
        input = """var b, c , c: integer;
                    procedure main(); begin end"""
        expect = "Redeclared Variable: c"
        self.assertTrue(TestChecker.test(input,expect,403))

    def test_04_redeclared_parameter(self):
        input = """var aa: integer;
                    function goo( a1,a1: integer): integer; begin end
                    procedure main(); begin end
                    """
        expect = "Redeclared Parameter: a1"
        self.assertTrue(TestChecker.test(input,expect,404))

    def test_05_parameter_vs_variable(self):
        input = """var aa: integer;
                    function goo( a1,a2: integer): integer; var a1: real; begin end
                    procedure main(); begin end
                    """
        expect = "Redeclared Variable: a1"
        self.assertTrue(TestChecker.test(input,expect,405))

    def test_06_redeclared_function(self):
        input = """ var y: integer;
                    function goo(m,y: integer): integer; var o : real; begin end
                    procedure goo();  begin  end
                    procedure main(); begin  end
                    """
        expect = "Redeclared Procedure: goo"
        self.assertTrue(TestChecker.test(input,expect,406))

    def test_07_Redeclare_in_Func_body(self):
        input = """procedure main ();
                    var a1 : real;
                    begin
                        a1 := foo(1,2,3);
                    end
                    Function foo(a, b, c : integer): real;
                    var d,e,g,d : real;
                    begin   end """
        expect = "Redeclared Variable: d"
        self.assertTrue(TestChecker.test(input, expect, 407))

    def test_08_redeclared_builtin_procedure(self):
        input = """ procedure putIntLn(i:integer);
                    begin end
                    procedure main(); begin  end """
        expect = "Redeclared Procedure: putIntLn"
        self.assertTrue(TestChecker.test(input, expect, 408))

    def test_09_redeclared_builtin_function(self):
        input = """function getFloat(): real;
                    begin   end
                    procedure main();  begin   end """
        expect = "Redeclared Function: getFloat"
        self.assertTrue(TestChecker.test(input, expect, 409))

    def test_10_diff_numofparam_stmt(self):
        input = """procedure main(); begin  putIntLn(); end"""
        expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[])"
        self.assertTrue(TestChecker.test(input,expect,410))

    def test_11_wrong_para_type_in_procedure(self):
        input = """function foo(): integer;
                 begin
                     putIntLn(5.5); putInt(12);
                 end
                procedure main(); begin  end"""
        expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[FloatLiteral(5.5)])"
        self.assertTrue(TestChecker.test(input,expect,411))

    def test_12_check_break_invalid(self):
        input = """ procedure foo(); begin end
                    var x: integer;
                    procedure main();
                    begin
                        x := 5;
                        break;
                    end """
        expect = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 412))

    def test_13_check_continue(self):
        input = """ procedure foo(); begin end
                    var x: integer;
                    procedure main();
                    begin
                        x := 5;
                        continue;
                    end """
        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 413))

    def test_14_Mismatch_in_Expression(self):
        input = """ procedure main();
        			var x: integer;
        			begin
        				x := getint(12);
        			end """
        expect = "Type Mismatch In Statement: CallExpr(Id(getint),[IntLiteral(12)])"
        self.assertTrue(TestChecker.test(input, expect, 414))

    def test_15_undeclared_id(self):
        input = """ var x,y: integer;
                    procedure main();
                    begin
                        y := z;
                        return;
                    end """
        expect = "Undeclared Identifier: z"
        self.assertTrue(TestChecker.test(input, expect, 415))

    def test_16_invalid_assign(self):
        input = """ procedure main();
                var x: real;
                begin
                    x := true;
                end"""
        expect = "Type Mismatch In Statement: AssignStmt(Id(x),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input, expect, 416))

    def test_17_Mismatch_in_ForLoop(self):
        input = """ var a: integer; b: boolean;
                procedure main();
                begin
                    a := 2;
                    b := checkFor(1,3,true);
                end
                function checkFor(k, h: integer ; m: boolean): boolean;
                var b,a: integer; c: boolean ;
                begin
                    for c := 0 to 10 do
                         b := b + a;
                    return c;
                end"""
        expect = "Type Mismatch In Statement: For(Id(c)IntLiteral(0),IntLiteral(10),True,[AssignStmt(Id(b),BinaryOp(+,Id(b),Id(a)))])"
        self.assertTrue(TestChecker.test(input, expect, 417))

    def test_18_Mismatch_in_ForLoop2(self):
        input = """ function foo():boolean;
                var i: real;
                begin
                    for i := 5 to 10 do
                        i := i + 1;
                        break;
                end
                procedure main(); begin  end"""
        expect = "Type Mismatch In Statement: For(Id(i)IntLiteral(5),IntLiteral(10),True,[AssignStmt(Id(i),BinaryOp(+,Id(i),IntLiteral(1)))])"
        self.assertTrue(TestChecker.test(input, expect, 418))

    def test_19_wrong_type_assign(self):
        input = """ var a,b:integer;
                procedure main();
                var a: integer; r: real; b: boolean;
                begin
                    a:= r + b;
                end """
        expect = "Type Mismatch In Expression: BinaryOp(+,Id(r),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 419))



    # def test_66_just4test(self):
    #     input = """ """
    #     expect = ""
    #     self.assertTrue(TestChecker.test(input, expect, 666))
    #
    # def test_77_just4test(self):
    #     input = """ """
    #     expect = ""
    #     self.assertTrue(TestChecker.test(input,expect,777))
    #
    # def test_88_undeclared_function_use_ast(self):
    #     input = Program([FuncDecl(Id("main"),[],[],[
    #         CallStmt(Id("foo"),[])])])
    #     expect = "Undeclared Procedure: foo"
    #     self.assertTrue(TestChecker.test(input,expect,888))
    #
    # def test_99(self):
    #     input = """ """
    #     expected = ""
    #     self.assertTrue(TestChecker.test(input, expected, 999))
    #
    #
