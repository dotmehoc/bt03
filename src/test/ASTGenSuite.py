# Student name: Nguyễn Quốc Dũng
# Student ID: 1770471

import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    # def test_00_simple_procedure(self):
    #     input = """procedure main(); begin end """
    #     expect = """Program([FuncDecl(Id(main),[],VoidType(),[],[])])"""
    #     self.assertTrue(TestAST.test(input, expect, 300))

#     def test_01_simple_function(self):
#         input = """function abc(): integer; begin end"""
#         expect = """Program([FuncDecl(Id(abc),[],IntType,[],[])])"""
#         self.assertTrue(TestAST.test(input, expect, 301))
#
#     def test_02_simple_variables_declare(self):
#         input = """ var x,y,z : integer; f,e:real;"""
#         expect = """Program([VarDecl(Id(x),IntType),VarDecl(Id(y),IntType),"""  +\
#                  """VarDecl(Id(z),IntType),VarDecl(Id(f),FloatType),VarDecl(Id(e),FloatType)])"""
#         self.assertTrue(TestAST.test(input, expect, 302))
#
#     def test_03_simple_array_declare(self):
#         input = """ var d : array [1 .. 5] of integer; """
#         expect = """Program([VarDecl(Id(d),ArrayType(IntLiteral(1),IntLiteral(5),IntType))])"""
#         self.assertTrue(TestAST.test(input, expect, 303))
#
#     def test_04_var_array_declare(self):
#         input = """ var a: array [1+x .. y*2] of integer; """
#         expect = """Program([VarDecl(Id(a),"""                        +\
#                  """ArrayType(BinaryOp(+,IntLiteral(1),Id(x)),BinaryOp(*,Id(y),IntLiteral(2)),IntType))])"""
#         self.assertTrue(TestAST.test(input, expect, 304))
#
#     def test_05_function_with_parameters(self):
#         input = """function abc(x,y: real): integer; begin end"""
#         expect = """Program([FuncDecl(Id(abc),[VarDecl(Id(x),FloatType),VarDecl(Id(y),FloatType)],IntType,[],[])])"""
#         self.assertTrue(TestAST.test(input, expect, 305))
#
#     def test_06_call_builtin_function(self):
#         input = """function foo(): INTEGER;
#                 begin
#                     putInt(12);
#                     putIntLn(5);
#                 end"""
#         expect = """Program([FuncDecl(Id(foo),[],IntType,[],"""       +\
#                  """[CallStmt(Id(putInt),[IntLiteral(12)]),CallStmt(Id(putIntLn),[IntLiteral(5)])])])"""
#         self.assertTrue(TestAST.test(input,expect,306))
#
#     def test_07_call_without_parameter(self):
#         input = """procedure main();
#                 begin
#                     getInt();
#                 end
#                 function foo():INTEGER;
#                 begin
#                     getFloat();
#                 end"""
#         expect = """Program([FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(getInt),[])]),"""  +\
#                  """FuncDecl(Id(foo),[],IntType,[],[CallStmt(Id(getFloat),[])])])"""
#         self.assertTrue(TestAST.test(input, expect, 307))
#
#     def test_08_call_others(self):
#         input = """procedure main();
#                 begin
#                     putFloat(10.5);
#                     putFloatLn(20.5);
#                 end"""
#         expect = """Program([FuncDecl(Id(main),[],VoidType(),[],"""     +\
#                  """[CallStmt(Id(putFloat),[FloatLiteral(10.5)]),CallStmt(Id(putFloatLn),[FloatLiteral(20.5)])])])"""
#         self.assertTrue(TestAST.test(input, expect, 308))
#
#     def test_09_call_boolean_func(self):
#         input = """procedure main();
#                 begin
#                     putBool(True);
#                     putBoolLn(False);
#                 end"""
#         expect = """Program([FuncDecl(Id(main),[],VoidType(),[],"""     +\
#                  """[CallStmt(Id(putBool),[BooleanLiteral(True)]),CallStmt(Id(putBoolLn),[BooleanLiteral(False)])])])"""
#         self.assertTrue(TestAST.test(input, expect, 309))
#
#     def test_10_calling_function(self):
#         input = """procedure foo(a : integer); begin    end
#                 function goo( a : integer): integer; begin  end
#                 procedure main();
#                 begin
#                      if goo(2) then
#                         goo(foo(2));
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[VarDecl(Id(a),IntType)],VoidType(),[],[]),"""      +\
#                    """FuncDecl(Id(goo),[VarDecl(Id(a),IntType)],IntType,[],[]),"""                  +\
#                    """FuncDecl(Id(main),[],VoidType(),[],[If(CallExpr(Id(goo),[IntLiteral(2)]),"""  +\
#                    """[CallStmt(Id(goo),[CallExpr(Id(foo),[IntLiteral(2)])])],[])])])"""
#         self.assertTrue(TestAST.test(input, expected,310))
#
#     def test_11_with_array_index(self):
#         input = """procedure foo();
#                     begin
#                         x := goo(a, 2)[1];
#                     end
#                 """
#         expected = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""    +\
#                    """[AssignStmt(Id(x),ArrayCell(CallExpr(Id(goo),[Id(a),IntLiteral(2)]),IntLiteral(1)))])])"""
#         self.assertTrue(TestAST.test(input, expected, 311))
#
#     def test_12_variales_parameters(self):
#         input = """Function Square(Index, Result : Integer): integer;
#                 Var	Res : Integer;
#                 Begin
#                     Result := Index * Index;
#                 End
#                 procedure res();
#                 Begin
#                     Writeln("The square of 5 is: ");
#                     Square(5, Res);
#                     Writeln(Res);
#                 End"""
#         expected = """Program([FuncDecl(Id(Square),[VarDecl(Id(Index),IntType),"""                              +\
#                     """VarDecl(Id(Result),IntType)],IntType,"""                                                  +\
#                    """[VarDecl(Id(Res),IntType)],[AssignStmt(Id(Result),BinaryOp(*,Id(Index),Id(Index)))]),"""  +\
#                    """FuncDecl(Id(res),[],VoidType(),[],"""                                                     +\
#                    """[CallStmt(Id(Writeln),[StringLiteral(The square of 5 is: )]),"""                          +\
#                    """CallStmt(Id(Square),[IntLiteral(5),Id(Res)]),"""                                          +\
#                    """CallStmt(Id(Writeln),[Id(Res)])])])"""
#         self.assertTrue(TestAST.test(input, expected,312))
#
#     def test_13_single_assign(self):
#         input = """procedure main();
#                 begin
#                     BTL := 7;
#                     TongKet := 5.49;
#                     KQ := "Rot";
#                     XacNnhan := True;
#                 end"""
#         expect = """Program([FuncDecl(Id(main),[],VoidType(),[],"""                                     +\
#                  """[AssignStmt(Id(BTL),IntLiteral(7)),AssignStmt(Id(TongKet),FloatLiteral(5.49)),"""   +\
#                  """AssignStmt(Id(KQ),StringLiteral(Rot)),AssignStmt(Id(XacNnhan),BooleanLiteral(True))])])"""
#         self.assertTrue(TestAST.test(input, expect, 313))
#
#     def test_14_single_assign_array(self):
#         input = """function chitiet(): array[1 .. 4] of real;
#                 var BTL: array [1 .. 4] of real;
#                 Quizs: array [1 .. 10] of real;
#                 chitietKQ: array [1 .. 20] of string;
#                 XacNnhan: array [1 .. 2] of boolean;
#                 begin   end"""
#         expect = """Program([FuncDecl(Id(chitiet),[],ArrayType(IntLiteral(1),IntLiteral(4),FloatType),"""   +\
#                  """[VarDecl(Id(BTL),ArrayType(IntLiteral(1),IntLiteral(4),FloatType)),"""                  +\
#                  """VarDecl(Id(Quizs),ArrayType(IntLiteral(1),IntLiteral(10),FloatType)),"""                +\
#                  """VarDecl(Id(chitietKQ),ArrayType(IntLiteral(1),IntLiteral(20),StringType)),"""           +\
#                  """VarDecl(Id(XacNnhan),ArrayType(IntLiteral(1),IntLiteral(2),BoolType))],[])])"""
#         self.assertTrue(TestAST.test(input, expect, 314))
#
#     def test_15_multi_assignment(self):
#         input = """function foo(): real;
#                      begin
#                          a := b[c] := x := y := z+1 ;
#                      end"""
#         expected = """Program([FuncDecl(Id(foo),[],FloatType,[],"""            +\
#                    """[AssignStmt(Id(y),BinaryOp(+,Id(z),IntLiteral(1))),"""    +\
#                    """AssignStmt(Id(x),Id(y)),"""                               +\
#                    """AssignStmt(ArrayCell(Id(b),Id(c)),Id(x)),"""              +\
#                    """AssignStmt(Id(a),ArrayCell(Id(b),Id(c)))])])"""
#         self.assertTrue(TestAST.test(input, expected, 315))
#
#     def test_16_func_with_ifnoelse_statement(self):
#         input = """ Function boo() : real;
#                 begin
#                     If x = 20 then x := x + 1 ;
#                 end"""
#         expect = """Program([FuncDecl(Id(boo),[],FloatType,[],"""   +\
#                  """[If(BinaryOp(=,Id(x),IntLiteral(20)),"""        +\
#                  """[AssignStmt(Id(x),BinaryOp(+,Id(x),IntLiteral(1)))],[])])])"""
#         self.assertTrue(TestAST.test(input, expect, 316))
#
#     def test_17_IfElse_statement(self):
#         input = """ Function boo() : real;
#                 begin
#                     If x = 20 then x := x + 1; else x := 10;
#                 end"""
#         expect = """Program([FuncDecl(Id(boo),[],FloatType,[],"""               +\
#                  """[If(BinaryOp(=,Id(x),IntLiteral(20)),"""                    +\
#                  """[AssignStmt(Id(x),BinaryOp(+,Id(x),IntLiteral(1)))],"""     +\
#                  """[AssignStmt(Id(x),IntLiteral(10))])])])"""
#         self.assertTrue(TestAST.test(input, expect, 317))
#
#     def test_18_nested_IfNoElse(self):
#         input = """ procedure foo();
#                     begin
#                         if a=1 then
#                             if a=2 then
#                                 rs:=1;
#                     end"""
#         expect = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""                                  +\
#                  """[If(BinaryOp(=,Id(a),IntLiteral(1)),[If(BinaryOp(=,Id(a),IntLiteral(2)),"""     +\
#                  """[AssignStmt(Id(rs),IntLiteral(1))],[])],[])])])"""
#         self.assertTrue(TestAST.test(input, expect, 318))
#
#     def test_19_IfNoElse_nested_IfElse(self):
#         input = """ procedure foo();
#                 begin
#                     if a > 0 then
#                         if b > 0 then
#                             py := 1;
#                         else
#                             py := 2;
#                 end"""
#         expect = """Program([FuncDecl(Id(foo),[],VoidType(),"""                                         +\
#                  """[],[If(BinaryOp(>,Id(a),IntLiteral(0)),"""                                          +\
#                  """[If(BinaryOp(>,Id(b),IntLiteral(0)),[AssignStmt(Id(py),IntLiteral(1))],[])],"""     +\
#                  """[AssignStmt(Id(py),IntLiteral(2))])])])"""
#         self.assertTrue(TestAST.test(input, expect, 319))
#
#     def test_20_andthen_orelse(self):
#         input = """procedure main ();
#                 begin
#                     if a<>0 and c=1 and then b=1 then
#                         begin  print("hello world"); end
#                     else if b<0 or f= false or else d= 0.55 then
#                         begin print("welcome Python"); end
#                 end"""
#         expected = """Program([FuncDecl(Id(main),[],VoidType(),[],"""               +\
#                    """[If(BinaryOp(andthen,BinaryOp(=,BinaryOp(<>,Id(a),BinaryOp(and,IntLiteral(0),Id(c))),IntLiteral(1)),BinaryOp(=,Id(b),IntLiteral(1))),""" +\
#                    """[CallStmt(Id(print),[StringLiteral(hello world)])],"""        +\
#                    """[If(BinaryOp(orelse,BinaryOp(=,BinaryOp(<,Id(b),BinaryOp(or,IntLiteral(0),Id(f))),BooleanLiteral(false)),BinaryOp(=,Id(d),FloatLiteral(0.55))),""" +\
#                    """[CallStmt(Id(print),[StringLiteral(welcome Python)])],[])])])])"""
#         self.assertTrue(TestAST.test(input, expected,320))
#
#     def test_21_multi_operators(self):
#         input = """procedure main();
#                 BEGIN
#                     BEGIN
#                         number := 2;
#                         a := number;
#                         b := 10 * a + 10 * number / 4;
#                         c := a - - b;
#                     END
#                     x := 11;
#                 END"""
#         expected = """Program([FuncDecl(Id(main),[],VoidType(),[],"""                           +\
#                    """[AssignStmt(Id(number),IntLiteral(2)),"""                                 +\
#                    """AssignStmt(Id(a),Id(number)),"""                                          +\
#                    """AssignStmt(Id(b),BinaryOp(+,BinaryOp(*,IntLiteral(10),Id(a)),"""          +\
#                    """BinaryOp(/,BinaryOp(*,IntLiteral(10),Id(number)),IntLiteral(4)))),"""     +\
#                    """AssignStmt(Id(c),BinaryOp(-,Id(a),UnaryOp(-,Id(b)))),"""                  +\
#                    """AssignStmt(Id(x),IntLiteral(11))])])"""
#         self.assertTrue(TestAST.test(input, expected,321))
#
#     def test_22_Logical_Operators(self):
#         input = """ procedure test();
#                 begin
#                     If(admin = "boss") AND (password="pwd123") Then
#                         Writeln("Welcome! Login accepted");
#                 end"""
#         expected = """Program([FuncDecl(Id(test),[],VoidType(),[],"""                               +\
#                    """[If(BinaryOp(AND,BinaryOp(=,Id(admin),StringLiteral(boss)),"""                +\
#                    """BinaryOp(=,Id(password),StringLiteral(pwd123))),"""                           +\
#                    """[CallStmt(Id(Writeln),[StringLiteral(Welcome! Login accepted)])],[])])])"""
#         self.assertTrue(TestAST.test(input, expected,322))
#
#     def test_23_Other_Logical_Operators(self):
#         input = """ Var logic: Boolean; A, B: Integer;
#                 procedure logical();
#                 Begin
#                     A := 10; B := 20; logic := False;
#                     logic := (A = 10) OR (B = 10);
#                     Writeln(logic);
#                     logic := (A = 10) AND (B = 10);
#                     Writeln(logic);
#                 End"""
#         expected = """Program([VarDecl(Id(logic),BoolType),VarDecl(Id(A),IntType),VarDecl(Id(B),IntType),""" +\
#                    """FuncDecl(Id(logical),[],VoidType(),[],"""                                             +\
#                    """[AssignStmt(Id(A),IntLiteral(10)),"""                                                 +\
#                    """AssignStmt(Id(B),IntLiteral(20)),"""                                                  +\
#                    """AssignStmt(Id(logic),BooleanLiteral(False)),"""                                        +\
#                    """AssignStmt(Id(logic),BinaryOp(OR,BinaryOp(=,Id(A),IntLiteral(10)),"""                 +\
#                    """BinaryOp(=,Id(B),IntLiteral(10)))),"""                                                +\
#                    """CallStmt(Id(Writeln),[Id(logic)]),"""                                                 +\
#                    """AssignStmt(Id(logic),BinaryOp(AND,BinaryOp(=,Id(A),IntLiteral(10)),"""                +\
#                    """BinaryOp(=,Id(B),IntLiteral(10)))),CallStmt(Id(Writeln),[Id(logic)])])])"""
#         self.assertTrue(TestAST.test(input, expected,323))
#
#     def test_24_many_vars_declare(self):
#         input = """ function foo(): real;
#                 Var a,x: integer; b: array[1 .. 10] of integer;
#                 Begin
#                     If (a = 1) Then
#                         a := b[10] := foo( )[3] := x := 1 ;
#                 End """
#         expected = """Program([FuncDecl(Id(foo),[],FloatType,"""                                                    +\
#                    """[VarDecl(Id(a),IntType),VarDecl(Id(x),IntType),"""                                            +\
#                    """VarDecl(Id(b),ArrayType(IntLiteral(1),IntLiteral(10),IntType))],"""                           +\
#                    """[If(BinaryOp(=,Id(a),IntLiteral(1)),"""                                                       +\
#                    """[AssignStmt(Id(x),IntLiteral(1)),"""                                                          +\
#                    """AssignStmt(ArrayCell(CallExpr(Id(foo),[]),IntLiteral(3)),Id(x)),"""                           +\
#                    """AssignStmt(ArrayCell(Id(b),IntLiteral(10)),ArrayCell(CallExpr(Id(foo),[]),IntLiteral(3))),""" +\
#                    """AssignStmt(Id(a),ArrayCell(Id(b),IntLiteral(10)))],[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 324))
#
#     def test_25_for_loop_increasing(self):
#         input = """procedure foo();
#                     begin
#                         for i := 0 to 10 do
#                             i := i + 1;
#                     end """
#         expected = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""                +\
#                    """[For(Id(i)IntLiteral(0),IntLiteral(10),True,"""               +\
#                    """[AssignStmt(Id(i),BinaryOp(+,Id(i),IntLiteral(1)))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 325))
#
#     def test_26_for_loop_decreasing(self):
#         input = """procedure foo();
#                     begin
#                         for i := 10 downto 0 do
#                             i := i - 1;
#                     end """
#         expected = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""                +\
#                    """[For(Id(i)IntLiteral(10),IntLiteral(0),False,"""              +\
#                    """[AssignStmt(Id(i),BinaryOp(-,Id(i),IntLiteral(1)))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 326))
#
#     def test_27_forloop_downto(self):
#         input = """ Var	Counter : Integer;
#                 procedure goo();
#                 Begin
#                 For Counter := 10 Downto 1 do
#                     Begin
#                         gotoxy(11-Counter);
#                         Writeln("over");
#                     End
#                 End"""
#         expected = """Program([VarDecl(Id(Counter),IntType),"""                             +\
#                     """FuncDecl(Id(goo),[],VoidType(),[],"""                                +\
#                     """[For(Id(Counter)IntLiteral(10),IntLiteral(1),False,"""               +\
#                     """[CallStmt(Id(gotoxy),[BinaryOp(-,IntLiteral(11),Id(Counter))]),"""   +\
#                     """CallStmt(Id(Writeln),[StringLiteral(over)])])])])"""
#         self.assertTrue(TestAST.test(input, expected,327))
#
#     def test_28_whileloop(self):
#         input = """procedure foo();
#                 begin
#                     while (i > 0) do
#                     begin
#                         i := i + 1;
#                         calling(i);
#                     end
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""                +\
#                    """[While(BinaryOp(>,Id(i),IntLiteral(0)),"""                    +\
#                    """[AssignStmt(Id(i),BinaryOp(+,Id(i),IntLiteral(1))),CallStmt(Id(calling),[Id(i)])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 328))
#
#     def test_29_break_condition(self):
#         input = """function foo() : boolean;
#                 begin
#                     while x > 0 do
#                         break;
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[],BoolType,[],[While(BinaryOp(>,Id(x),IntLiteral(0)),[Break])])])"""
#         self.assertTrue(TestAST.test(input, expected, 329))
#
#     def test_30_break_continue(self):
#         input = """ var a: integer;
#                 function foo(): real;
#                 begin
#                     a := 10;
#                     while a < 20 do
#                     begin
#                         writeln("value of a: ", a);
#                         a := a + 1;
#                         if (a > 15) then
#                             break;
#                         else
#                             continue;
#                     end
#                 end """
#         expected = """Program([VarDecl(Id(a),IntType),FuncDecl(Id(foo),[],FloatType,"""     +\
#                    """[],[AssignStmt(Id(a),IntLiteral(10)),"""                              +\
#                    """While(BinaryOp(<,Id(a),IntLiteral(20)),"""                            +\
#                    """[CallStmt(Id(writeln),[StringLiteral(value of a: ),Id(a)]),"""        +\
#                    """AssignStmt(Id(a),BinaryOp(+,Id(a),IntLiteral(1))),"""                 +\
#                    """If(BinaryOp(>,Id(a),IntLiteral(15)),[Break],[Continue])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 330))
#
#     def test_31_with_do_statement(self):
#         input = """procedure foo();
#                 begin
#                     with
#                         i : integer ;
#                         main : integer ;
#                         f : integer ;
#                     do begin
#                         main := f := i := 100;
#                     end
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[],VoidType(),[],"""                                        +\
#                    """[With([VarDecl(Id(i),IntType),VarDecl(Id(main),IntType),VarDecl(Id(f),IntType)],"""   +\
#                    """[AssignStmt(Id(i),IntLiteral(100)),"""                                                +\
#                    """AssignStmt(Id(f),Id(i)),"""                                                           +\
#                    """AssignStmt(Id(main),Id(f))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 331))
#
#     def test_32_return_with_None(self):
#         input = """function foo(): integer;
#                 begin
#                     if a > b then return 2.3 ;
#                     else return 2 ;
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[],IntType,[],"""                               +\
#                    """[If(BinaryOp(>,Id(a),Id(b)),[Return(Some(FloatLiteral(2.3)))],"""         +\
#                    """[Return(Some(IntLiteral(2)))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 332))
#
#     def test_33_return_with_dataType(self):
#         input = """function foo(): real;
#                 begin
#                     if a > b then return 2.3 ;
#                     else return 2 ;
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[],FloatType,[],"""                 +\
#                    """[If(BinaryOp(>,Id(a),Id(b)),"""                               +\
#                    """[Return(Some(FloatLiteral(2.3)))],[Return(Some(IntLiteral(2)))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 333))
#
#     def test_34_return_with_other_para(self):
#         input = """ function foo(b: array[1 .. 2] of integer): array[2 .. 3] of real;
#                 var a: array[2 .. 3] of real;
#                 begin
#                     if x=y then return a ;
#                     else return a + b ;
#                 end"""
#         expected = """Program([FuncDecl(Id(foo),[VarDecl(Id(b),ArrayType(IntLiteral(1),IntLiteral(2),IntType))],""" +\
#                    """ArrayType(IntLiteral(2),IntLiteral(3),FloatType),"""                      +\
#                    """[VarDecl(Id(a),ArrayType(IntLiteral(2),IntLiteral(3),FloatType))],"""     +\
#                    """[If(BinaryOp(=,Id(x),Id(y)),"""                                           +\
#                    """[Return(Some(Id(a)))],[Return(Some(BinaryOp(+,Id(a),Id(b))))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 334))
#
#     def test_35_return_with_other_para(self):
#         input = """ procedure boo();
#                 Var Counter : Integer;
#                 Begin
#                     For Counter := 1 to 7 do
#                         Writeln("for loop");
#                     Readln();
#                 End"""
#         expected = """Program([FuncDecl(Id(boo),[],VoidType(),"""               +\
#                    """[VarDecl(Id(Counter),IntType)],"""                        +\
#                    """[For(Id(Counter)IntLiteral(1),IntLiteral(7),True,"""      +\
#                    """[CallStmt(Id(Writeln),[StringLiteral(for loop)])]),CallStmt(Id(Readln),[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 335))
#
#     def test_36_if_else_fibonacci(self):
#         input = """ function fib(n:integer): integer;
#                 var i:integer;
#                 begin
#                     if (n <= 2) then fib := 1;
#                     else fib := fib(n-1) + fib(n-2);
#                     for i := 1 to 16 do
#                         write(fib(i), ", ");
#                     writeln(fib(5));
#                 end"""
#         expected = """Program([FuncDecl(Id(fib),[VarDecl(Id(n),IntType)],IntType,"""                +\
#                    """[VarDecl(Id(i),IntType)],"""                                                  +\
#                    """[If(BinaryOp(<=,Id(n),IntLiteral(2)),[AssignStmt(Id(fib),IntLiteral(1))],"""  +\
#                    """[AssignStmt(Id(fib),BinaryOp(+,CallExpr(Id(fib),[BinaryOp(-,Id(n),IntLiteral(1))]),""" +\
#                    """CallExpr(Id(fib),[BinaryOp(-,Id(n),IntLiteral(2))])))]),"""                   +\
#                    """For(Id(i)IntLiteral(1),IntLiteral(16),True,"""                                +\
#                    """[CallStmt(Id(write),[CallExpr(Id(fib),[Id(i)]),StringLiteral(, )])]),"""      +\
#                    """CallStmt(Id(writeln),[CallExpr(Id(fib),[IntLiteral(5)])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 336))
# #
#     def test_37_if_else_factorial(self):
#         input = """ function fact(n: integer): integer;
#                 var n: integer;
#                 begin
#                     if n = 0 then fact := 1;
#                     else fact := n * fact(n - 1);
#                     for n := 0 to 16 do
#                         writeln(n, "! = ", fact(n));
#                 end"""
#         expected = """Program([FuncDecl(Id(fact),[VarDecl(Id(n),IntType)],IntType,"""       +\
#                    """[VarDecl(Id(n),IntType)],[If(BinaryOp(=,Id(n),IntLiteral(0)),"""      +\
#                    """[AssignStmt(Id(fact),IntLiteral(1))],"""                              +\
#                    """[AssignStmt(Id(fact),BinaryOp(*,Id(n),"""                             +\
#                    """CallExpr(Id(fact),[BinaryOp(-,Id(n),IntLiteral(1))])))]),"""          +\
#                    """For(Id(n)IntLiteral(0),IntLiteral(16),True,"""                        +\
#                    """[CallStmt(Id(writeln),[Id(n),StringLiteral(! = ),"""                  +\
#                    """CallExpr(Id(fact),[Id(n)])])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 337))
#
#     def test_38_Factorial_recursive(self):
#         input = """Function Factorial(n : Integer) : Integer;
#                 Var Result : Integer;
#                 Begin
#                     If n = 1 Then Factorial := 1 ;
#                     Else Factorial := n*Factorial(n-1);
#                 End"""
#         expected = """Program([FuncDecl(Id(Factorial),[VarDecl(Id(n),IntType)],IntType,"""          +\
#                    """[VarDecl(Id(Result),IntType)],"""                                             +\
#                    """[If(BinaryOp(=,Id(n),IntLiteral(1)),"""                                       +\
#                    """[AssignStmt(Id(Factorial),IntLiteral(1))],"""                                 +\
#                    """[AssignStmt(Id(Factorial),"""                                                 +\
#                    """BinaryOp(*,Id(n),CallExpr(Id(Factorial),[BinaryOp(-,Id(n),IntLiteral(1))])))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 338))
#
#     def test_39_nested_forloop(self):
#         input = """ Procedure BubbleSort(numbers: integer; size: integer);
#                 Var i, j, temp: integer;
#                 Begin
#                     For i := size - 1 DownTo 1 do
#                         For j := 2 to i do
#                             if(numbers[j - 1] > numbers[j]) Then
#                             Begin
#                                 temp := numbers[j - 1];
#                                 numbers[j - 1] := numbers[j];
#                                 numbers[j] := temp;
#                             End
#                 End """
#         expected = """Program([FuncDecl(Id(BubbleSort),[VarDecl(Id(numbers),IntType),VarDecl(Id(size),IntType)],"""     +\
#                    """VoidType(),[VarDecl(Id(i),IntType),VarDecl(Id(j),IntType),VarDecl(Id(temp),IntType)],"""          +\
#                    """[For(Id(i)BinaryOp(-,Id(size),IntLiteral(1)),IntLiteral(1),False,"""                              +\
#                    """[For(Id(j)IntLiteral(2),Id(i),True,"""                                                            +\
#                    """[If(BinaryOp(>,ArrayCell(Id(numbers),BinaryOp(-,Id(j),IntLiteral(1))),ArrayCell(Id(numbers),Id(j))),""" +\
#                    """[AssignStmt(Id(temp),ArrayCell(Id(numbers),BinaryOp(-,Id(j),IntLiteral(1)))),"""                  +\
#                    """AssignStmt(ArrayCell(Id(numbers),BinaryOp(-,Id(j),IntLiteral(1))),"""                             +\
#                    """ArrayCell(Id(numbers),Id(j))),AssignStmt(ArrayCell(Id(numbers),Id(j)),Id(temp))],[])])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 339))
#
#     def test_40_operators(self):
#         input = """ procedure chuvi();
#                 var radius, dia, circ: real;
#                 begin
#                     writeln("Input radius : ");
#                     readln(radius);
#                     dia := 2 * radius;
#                     circ :=  3.14 * dia;
#                     writeln("Chu vi : ",circ);
#                 end """
#         expected = """Program([FuncDecl(Id(chuvi),[],VoidType(),"""                             +\
#                    """[VarDecl(Id(radius),FloatType),VarDecl(Id(dia),FloatType),VarDecl(Id(circ),FloatType)],""" +\
#                    """[CallStmt(Id(writeln),[StringLiteral(Input radius : )]),"""               +\
#                    """CallStmt(Id(readln),[Id(radius)]),"""                                     +\
#                    """AssignStmt(Id(dia),BinaryOp(*,IntLiteral(2),Id(radius))),"""              +\
#                    """AssignStmt(Id(circ),BinaryOp(*,FloatLiteral(3.14),Id(dia))),"""           +\
#                    """CallStmt(Id(writeln),[StringLiteral(Chu vi : ),Id(circ)])])])"""
#         self.assertTrue(TestAST.test(input, expected, 340))
#
#     def test_41_another_operators(self):
#         input = """ procedure dientich();
#                 var radius, dia, area: real;
#                 begin
#                     writeln("Input radius : ");
#                     readln(radius);
#                     exponent := radius * radius;
#                     area := exponent * 3.14;
#                     writeln("Dien tich : ",area);
#                 end """
#         expected = """Program([FuncDecl(Id(dientich),[],VoidType(),"""                                              +\
#                    """[VarDecl(Id(radius),FloatType),VarDecl(Id(dia),FloatType),VarDecl(Id(area),FloatType)],"""    +\
#                    """[CallStmt(Id(writeln),[StringLiteral(Input radius : )]),"""                                   +\
#                    """CallStmt(Id(readln),[Id(radius)]),"""                                                         +\
#                    """AssignStmt(Id(exponent),BinaryOp(*,Id(radius),Id(radius))),"""                                +\
#                    """AssignStmt(Id(area),BinaryOp(*,Id(exponent),FloatLiteral(3.14))),"""                          +\
#                    """CallStmt(Id(writeln),[StringLiteral(Dien tich : ),Id(area)])])])"""
#         self.assertTrue(TestAST.test(input, expected, 341))
#
#     def test_42_more_operators(self):
#         input = """function divisible(): integer;
#                 var N:integer;
#                 begin
#                     writeln("Input N= "); readln(N);
#                     If (N mod 2 = 0) and (N mod 4 = 0) and (N mod 5 = 0) then
#                         write("N is divisible by 2, 4, 5");
#                     If (N mod 2 <> 0) and (N mod 4 <> 0) and (N mod 5 <> 0) then
#                         write("N is not divisible by 2, 4, 5");
#                 end"""
#         expected = """Program([FuncDecl(Id(divisible),[],IntType,"""+\
#                    """[VarDecl(Id(N),IntType)],"""+\
#                    """[CallStmt(Id(writeln),[StringLiteral(Input N= )]),"""+\
#                    """CallStmt(Id(readln),[Id(N)]),"""+\
#                    """If(BinaryOp(and,BinaryOp(and,BinaryOp(=,BinaryOp(mod,Id(N),IntLiteral(2)),IntLiteral(0)),"""  +\
#                    """BinaryOp(=,BinaryOp(mod,Id(N),IntLiteral(4)),IntLiteral(0))),"""                              +\
#                    """BinaryOp(=,BinaryOp(mod,Id(N),IntLiteral(5)),IntLiteral(0))),"""                              +\
#                    """[CallStmt(Id(write),[StringLiteral(N is divisible by 2, 4, 5)])],[]),"""                      +\
#                    """If(BinaryOp(and,BinaryOp(and,BinaryOp(<>,BinaryOp(mod,Id(N),IntLiteral(2)),IntLiteral(0)),""" +\
#                    """BinaryOp(<>,BinaryOp(mod,Id(N),IntLiteral(4)),IntLiteral(0))),"""                             +\
#                    """BinaryOp(<>,BinaryOp(mod,Id(N),IntLiteral(5)),IntLiteral(0))),"""                             +\
#                    """[CallStmt(Id(write),[StringLiteral(N is not divisible by 2, 4, 5)])],[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 342))
#
#
#     def test_43_function(self):
#         input = """ function findMin(x, y, z, m: integer): integer;
#                 begin
#                     if x < y then m := x;
#                     else m := y;
#                     if z < m then m := z;
#                 end """
#         expected = """Program([FuncDecl(Id(findMin),[VarDecl(Id(x),IntType),"""                             +\
#                    """VarDecl(Id(y),IntType),VarDecl(Id(z),IntType),VarDecl(Id(m),IntType)],IntType,[],"""  +\
#                    """[If(BinaryOp(<,Id(x),Id(y)),[AssignStmt(Id(m),Id(x))],[AssignStmt(Id(m),Id(y))]),"""  +\
#                    """If(BinaryOp(<,Id(z),Id(m)),[AssignStmt(Id(m),Id(z))],[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 343))
#
#     def test_44_another_array(self):
#         input = """ procedure main();
#                 var n: array[1 .. 10] of integer;
#                 i, j: integer;
#                 begin
#                     for i := 1 to 10 do
#                         n[i] := i + 100;
#                         for j := 1 to 10 do
#                             writeln("Element[", j, "] = ", n[j]);
#                 end """
#         expected = """Program([FuncDecl(Id(main),[],VoidType(),"""                                  +\
#                    """[VarDecl(Id(n),ArrayType(IntLiteral(1),IntLiteral(10),IntType)),"""           +\
#                    """VarDecl(Id(i),IntType),VarDecl(Id(j),IntType)],"""                            +\
#                    """[For(Id(i)IntLiteral(1),IntLiteral(10),True,"""                               +\
#                    """[AssignStmt(ArrayCell(Id(n),Id(i)),BinaryOp(+,Id(i),IntLiteral(100)))]),"""   +\
#                    """For(Id(j)IntLiteral(1),IntLiteral(10),True,"""                                +\
#                    """[CallStmt(Id(writeln),[StringLiteral(Element[),Id(j),StringLiteral(] = ),ArrayCell(Id(n),Id(j))])])])])"""
#         self.assertTrue(TestAST.test(input, expected, 344))
#
#     def test_45_ignore_comments(self):
#         input = """procedure main();
#                 begin
#                     (* this is block comment *)
#                     { this is also block comment}
#                     // this is line comment
#                     writeln("not print comments out");
#                 end """
#         expected = """Program([FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(writeln),[StringLiteral(not print comments out)])])])"""
#
#         self.assertTrue(TestAST.test(input, expected, 345))
#
#     def test_46_array_in_Call(self):
#         input = """var x: integer; i:array [(1+3) .. 5] of integer;
#                 procedure main();
#                 var y: integer; i:array [(1+7) .. (5+y)] of integer;
#                     begin
#                         foo(2)[3+x] := a[b[2]] + 3;
#                     end"""
#         expected = """Program([VarDecl(Id(x),IntType),"""                                                               +\
#                    """VarDecl(Id(i),ArrayType(BinaryOp(+,IntLiteral(1),IntLiteral(3)),IntLiteral(5),IntType)),"""       +\
#                    """FuncDecl(Id(main),[],VoidType(),"""                                                               +\
#                    """[VarDecl(Id(y),IntType),"""                                                                       +\
#                    """VarDecl(Id(i),ArrayType(BinaryOp(+,IntLiteral(1),IntLiteral(7)),BinaryOp(+,IntLiteral(5),Id(y)),IntType))],"""+\
#                    """[AssignStmt(ArrayCell(CallExpr(Id(foo),[IntLiteral(2)]),BinaryOp(+,IntLiteral(3),Id(x))),"""      +\
#                    """BinaryOp(+,ArrayCell(Id(a),ArrayCell(Id(b),IntLiteral(2))),IntLiteral(3)))])])"""
#         self.assertTrue(TestAST.test(input, expected, 346))
#
#     def test_47_stack_Initial(self):
#         input = """Var myStack : Array[1 .. 100] of integer;
#                 topPointer : Integer;
#                 Procedure InitStack();
#                 Begin
#                 	topPointer := 0;
#                 End"""
#         expected = """Program([VarDecl(Id(myStack),ArrayType(IntLiteral(1),IntLiteral(100),IntType)),"""    +\
#                    """VarDecl(Id(topPointer),IntType),"""                                                   +\
#                    """FuncDecl(Id(InitStack),[],VoidType(),[],"""                                           +\
#                    """[AssignStmt(Id(topPointer),IntLiteral(0))])])"""
#         self.assertTrue(TestAST.test(input, expected, 347))
#
#     def test_48_check_Full_Empty_stack(self):
#         input = """Function IsEmpty() : Boolean;
#                 Begin
#                 	IsEmpty := False;
#                 	If (topPointer = 0) Then IsEmpty := true;
#                 End
#                 Function IsFull() : Boolean;
#                 Begin
#                 	IsFull := False;
#                 	If ((topPointer + 1) = 100) Then IsFull := True;
#                 End"""
#         expected = """Program([FuncDecl(Id(IsEmpty),[],BoolType,[],"""                                                  +\
#                    """[AssignStmt(Id(IsEmpty),BooleanLiteral(False)),"""                                                 +\
#                    """If(BinaryOp(=,Id(topPointer),IntLiteral(0)),[AssignStmt(Id(IsEmpty),BooleanLiteral(true))],"""    +\
#                    """[])]),FuncDecl(Id(IsFull),[],BoolType,[],"""                                                      +\
#                    """[AssignStmt(Id(IsFull),BooleanLiteral(False)),"""                                                  +\
#                    """If(BinaryOp(=,BinaryOp(+,Id(topPointer),IntLiteral(1)),IntLiteral(100)),"""                       +\
#                    """[AssignStmt(Id(IsFull),BooleanLiteral(True))],[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 348))
#
#     def test_49_Pop_Push_stack(self):
#         input = """Function Pop() : integer;
#                 Begin
#                 	Pop := nil;
#                 	If not IsEmpty Then
#                 	    Begin
#                 	        Pop := myStack[topPointer];
#                 	        topPointer := topPointer - 1;
#                 	    End
#                 End
#                 Procedure Push(item : integer);
#                 Begin
#                     If not IsFull Then
#                         Begin
#                             myStack[topPointer+1] := item;
#                             topPointer := topPointer + 1;
#                         End
#                 End"""
#         expected = """Program([FuncDecl(Id(Pop),[],IntType,[],"""                                           +\
#                    """[AssignStmt(Id(Pop),Id(nil)),"""                                                      +\
#                    """If(UnaryOp(not,Id(IsEmpty)),"""                                                       +\
#                    """[AssignStmt(Id(Pop),ArrayCell(Id(myStack),Id(topPointer))),"""                        +\
#                    """AssignStmt(Id(topPointer),BinaryOp(-,Id(topPointer),IntLiteral(1)))],[])]),"""        +\
#                    """FuncDecl(Id(Push),[VarDecl(Id(item),IntType)],VoidType(),[],"""                       +\
#                    """[If(UnaryOp(not,Id(IsFull)),"""                                                       +\
#                    """[AssignStmt(ArrayCell(Id(myStack),BinaryOp(+,Id(topPointer),IntLiteral(1))),Id(item)),"""+\
#                    """AssignStmt(Id(topPointer),BinaryOp(+,Id(topPointer),IntLiteral(1)))],[])])])"""
#         self.assertTrue(TestAST.test(input, expected, 349))
#
#     def test_50_getSize_stack(self):
#         input = """Function GetSize() : Integer;
#                 Begin
#                 	GetSize := topPointer;
#                 End"""
#         expected = """Program([FuncDecl(Id(GetSize),[],IntType,[],[AssignStmt(Id(GetSize),Id(topPointer))])])"""
#         self.assertTrue(TestAST.test(input, expected, 350))
#
#     def test_51_insertion_Sort(self):
#         input = """Procedure InsertionSort(numbers : Array [1 .. 100] of Integer; size : Integer);
#                 Var	i, j, index : Integer;
#                 Begin
#                     For i := 2 to size - 1 do
#                         Begin
#                             index := numbers[i];
#                             j := i;
#                             While ((j > 1) AND (numbers[j-1] > index)) do
#                             Begin
#                                 numbers[j] := numbers[j-1];
#                                 j := j - 1;
#                             End
#                             numbers[j] := index;
#                         End
#                 End"""
#         expected = """Program([FuncDecl(Id(InsertionSort),"""                                           +\
#                    """[VarDecl(Id(numbers),ArrayType(IntLiteral(1),IntLiteral(100),IntType)),"""        +\
#                    """VarDecl(Id(size),IntType)],VoidType(),"""                                         +\
#                    """[VarDecl(Id(i),IntType),VarDecl(Id(j),IntType),VarDecl(Id(index),IntType)],"""    +\
#                    """[For(Id(i)IntLiteral(2),BinaryOp(-,Id(size),IntLiteral(1)),True,"""               +\
#                    """[AssignStmt(Id(index),ArrayCell(Id(numbers),Id(i))),"""                           +\
#                    """AssignStmt(Id(j),Id(i)),"""                                                       +\
#                    """While(BinaryOp(AND,BinaryOp(>,Id(j),IntLiteral(1)),"""                            +\
#                    """BinaryOp(>,ArrayCell(Id(numbers),BinaryOp(-,Id(j),IntLiteral(1))),Id(index))),""" +\
#                    """[AssignStmt(ArrayCell(Id(numbers),Id(j)),ArrayCell(Id(numbers),BinaryOp(-,Id(j),IntLiteral(1)))),"""+\
#                    """AssignStmt(Id(j),BinaryOp(-,Id(j),IntLiteral(1)))]),"""                           +\
#                    """AssignStmt(ArrayCell(Id(numbers),Id(j)),Id(index))])])])"""
#         self.assertTrue(TestAST.test(input, expected, 351))
#
#     def test_52_more_test(self):
#         input = """ procedure main();
#                 BEGIN
#                     BEGIN
#                         number := 2;
#                         a := number;
#                         b := 10 * a + 10 * number / 4;
#                         c := a - - b;
#                     END
#                     x := 11;
#                 END"""
#         expected = """Program([FuncDecl(Id(main),[],VoidType(),[],"""                   +\
#                    """[AssignStmt(Id(number),IntLiteral(2)),"""                         +\
#                    """AssignStmt(Id(a),Id(number)),"""                                  +\
#                    """AssignStmt(Id(b),BinaryOp(+,BinaryOp(*,IntLiteral(10),Id(a)),BinaryOp(/,BinaryOp(*,IntLiteral(10),Id(number)),IntLiteral(4)))),"""+\
#                    """AssignStmt(Id(c),BinaryOp(-,Id(a),UnaryOp(-,Id(b)))),"""          +\
#                    """AssignStmt(Id(x),IntLiteral(11))])])"""
#         self.assertTrue(TestAST.test(input, expected, 352))
#
#     def test_53(self):
#         input = """var i: integer ;
#                 function f(): integer ;
#                 begin
#                     return 200;
#                 end
#                 procedure main();
#                 var main: integer;
#                 begin
#                     main := f();
#                     putIntLn(main);
#                     with i: integer ; main: integer ; f: integer ; do
#                         begin
#                             main := f := i := 100;
#                             putIntLn(i);
#                             putIntLn(main);
#                             putIntLn(f);
#                         end
#                     putIntLn(main);
#                 end
#                 var g:real;"""
#         expected = str(Program([VarDecl(Id('i'),IntType()),
#                                 FuncDecl(Id('f'),[],[],[Return((IntLiteral(200)))],IntType()),
#                                 FuncDecl(Id('main'),[],[VarDecl(Id('main'),IntType())],
#                                          [Assign(Id('main'),CallExpr(Id('f'),[])),
#                                           CallStmt(Id('putIntLn'),[Id('main')]),
#                                           With([VarDecl(Id('i'),IntType()),
#                                                 VarDecl(Id('main'),IntType()),
#                                                 VarDecl(Id('f'),IntType())],
#                                                [Assign(Id('i'),IntLiteral(100)),
#                                                 Assign(Id('f'),Id('i')),
#                                                 Assign(Id('main'),Id('f')),CallStmt(Id('putIntLn'),[Id('i')]),
#                                                 CallStmt(Id('putIntLn'),[Id('main')]),
#                                                 CallStmt(Id('putIntLn'),[Id('f')])]),
#                                           CallStmt(Id('putIntLn'),[Id('main')])],VoidType()),VarDecl(Id('g'),FloatType())]))
#         self.assertTrue(TestAST.test(input, expected, 353))
#
#     def test_54(self):
#         input = """ var i: integer;
#                 procedure main();
#                 var x, y, z: integer;
#                 begin
#                     x := y := z := hello();
#                 end
#                 function hello(): integer;
#                 begin
#                     i := i + 1;
#                     return i;
#                 end"""
#         expected = """Program([VarDecl(Id(i),IntType),FuncDecl(Id(main),[],VoidType(),"""               +\
#                    """[VarDecl(Id(x),IntType),VarDecl(Id(y),IntType),VarDecl(Id(z),IntType)],"""        +\
#                    """[AssignStmt(Id(z),CallExpr(Id(hello),[])),"""                                     +\
#                    """AssignStmt(Id(y),Id(z)),"""                                                       +\
#                    """AssignStmt(Id(x),Id(y))]),"""                                                     +\
#                    """FuncDecl(Id(hello),[],IntType,[],"""                                              +\
#                    """[AssignStmt(Id(i),BinaryOp(+,Id(i),IntLiteral(1))),Return(Some(Id(i)))])])"""
#         self.assertTrue(TestAST.test(input, expected, 354))

    def test_999_(self):
        input = """var x,y : integer; procedure main(); begin end;"""
        expected = """"""
        self.assertTrue(TestAST.test(input, expected, 555))
