import unittest
from TestUtils import TestChecker
from AST import *

class CheckerSuite(unittest.TestCase):
    def test_undeclared_function(self):
        """Simple program: int main() {} """
        input = """procedure main(); begin foo(); end"""
        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,400))

    def test_diff_numofparam_stmt(self):
        """More complex program"""
        input = """procedure main (); begin
            putIntLn();
        end"""
        expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[])"
        self.assertTrue(TestChecker.test(input,expect,401))

    def test_undeclared_function_use_ast(self):
        """Simple program: int main() {} """
        input = Program([FuncDecl(Id("main"),[],[],[
            CallStmt(Id("foo"),[])])])
        expect = "Undeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input,expect,402))

    def test_diff_numofparam_expr_use_ast(self):
        """More complex program"""
        input = Program([
                FuncDecl(Id("main"),[],[],[
                    CallStmt(Id("putIntLn"),[])])])

        expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[])"
        self.assertTrue(TestChecker.test(input,expect,403))

    def test_404(self):
        input = """
        procedure main();
            var a: integer;
            var a: boolean;
            begin
            end
        """
        expected = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input, expected, 404))

    def test_405(self):
        input = """
        procedure main();
            var a: integer;
            begin
                a := false;
            end
        """
        expected = "Type Mismatch In Statement: AssignStmt(Id(a),BooleanLiteral(false))"
        self.assertTrue(TestChecker.test(input, expected, 405))

    def test_406(self):
        input = """
        procedure main();
            var a: integer;
            begin
                a := 1;
                if (a + 5) then
                    a := 5;
            end
        """
        expected = "Type Mismatch In Statement: If(BinaryOp(+,Id(a),IntLiteral(5)),[AssignStmt(Id(a),IntLiteral(5))],[])"
        self.assertTrue(TestChecker.test(input, expected, 406))

    def test_407(self):
        input = """
        procedure main();
            var a: integer;
            begin
                a := 1;
                break;
            end
        """
        expected = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input, expected, 407))

    def test_408(self):
        input = """
        procedure main();
            var a: integer;
            begin
                a := 1;
                continue;
            end
        """
        expected = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expected, 408))

    def test_409(self):
        input = """

        function foo():integer;
            begin
                return 2;
            end

        procedure main();
            var a: boolean;
            begin
                a := foo();
            end
        """
        expected = "Type Mismatch In Statement: AssignStmt(Id(a),CallExpr(Id(foo),[]))"
        self.assertTrue(TestChecker.test(input, expected, 409))

    def test_410(self):
        input = """

        function foo():integer;
            begin
                return 2;
            end

        procedure main();
            var a: integer;
            begin
                return;
                a := foo();
            end
        """
        expected = "Unreachable statement: AssignStmt(Id(a),CallExpr(Id(foo),[]))"
        self.assertTrue(TestChecker.test(input, expected, 410))

    def test_411(self):
        input = """

        function foo():integer;
            begin
                return 2;
            end
        """
        expected = "No entry point"
        self.assertTrue(TestChecker.test(input, expected, 411))

    def test_412(self):
        input = """

        function foo():integer;
            begin
                return false;
            end
        """
        expected = "Type Mismatch In Statement: Return(Some(BooleanLiteral(False)))"
        self.assertTrue(TestChecker.test(input, expected, 412))

    def test_413(self):
        input = """

        function foo():integer;
            var i: integer;
            begin
                if (1 > 2) then
                    return false;
                i := 0;
            end
        """
        expected = "Type Mismatch In Statement: Return(Some(BooleanLiteral(False)))"
        self.assertTrue(TestChecker.test(input, expected, 413))

    def test_414(self):
        input = """

        function foo():boolean;
            begin
                for i := 0 to 10 do
                    begin
                        i := i + 1;
                        break;
                    end
            end
        """
        expected = "Undeclared Variable: i"
        self.assertTrue(TestChecker.test(input, expected, 414))

    def test_415(self):
        input = """

        function foo():boolean;
            var i: boolean;
            begin
                for i := 0 to 10 do
                    begin
                        i := i + 1;
                        break;
                    end
            end
        """
        expected = "Type Mismatch In Expression: AssignStmt(Id(i),IntLiteral(0))"
        self.assertTrue(TestChecker.test(input, expected, 415))

    def test_416(self):
        input = """

        function foo():boolean;
            var i: integer;
            begin
                for i := 0 to 10 do
                    with
                        f : integer ;
                    do begin
                        break;
                    end
            end
        """
        expected = "No entry point"
        self.assertTrue(TestChecker.test(input, expected, 416))

    def test_417(self):
        input = """

        function foo():boolean;
            var i: integer;
            begin
                for i := 0 to 10 do
                    begin
                        while (i > 0) do
                            begin
                                i := i + 1;
                                break;
                            end
                        break;
                    end
                break;
            end
        """
        expected = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input, expected, 417))

    def test_418(self):
        input = """

        function foo():integer;
            begin
                return 1;
            end

        procedure bar();
            begin
            end

        procedure main();
            begin
                bar();
            end
        """
        expected = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input, expected, 418))

    def test_redeclared_builtin_procedure(self):
        """Redeclared error"""
        input = """
                    procedure putInt(n:integer);
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Procedure: putInt"
        self.assertTrue(TestChecker.test(input, expect, 404))

    def test_redeclared_builtin_function(self):
        """Redeclared error"""
        input = """
                    function getInt():integer;
                    begin
                        return 1;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Function: getInt"
        self.assertTrue(TestChecker.test(input, expect, 405))

    def test_redeclared_procedure(self):
        """Redeclared error"""
        input = """
                    function foo():integer;
                    begin
                        return 1;
                    end
                    procedure foo();
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input, expect, 406))

    def test_redeclared_fuction(self):
        """Redeclared error"""
        input = """
                    procedure foo();
                    begin
                        return;
                    end
                    function foo():integer;
                    begin
                        return 1;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 407))

    def test_redeclared_in_local_parameters(self):
        """Redeclared error"""
        input = """
                    procedure foo(x,y:integer;z,x:real);
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Parameter: x"
        self.assertTrue(TestChecker.test(input, expect, 408))

    def test_redeclared_in_local_declararion(self):
        """Redeclared error"""
        input = """
                    procedure foo(x,y:integer);
                    var x:real;
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Variable: x"
        self.assertTrue(TestChecker.test(input, expect, 409))

    def test_another_redeclared_in_local_declararion(self):
        """Redeclared error"""
        input = """
                    procedure foo(x,y:integer);
                    var a:real;a:array[1 .. 5] of real;
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input, expect, 410))

    def test_another_redeclared_in_global_envi_declararion_variable_and_procedure(self):
        """Redeclared error"""
        input = """
                    var a,b:integer;
                    var foo:real;
                    procedure foo(x,y:integer);
                    begin
                        return;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Procedure: foo"
        self.assertTrue(TestChecker.test(input, expect, 411))

    def test_another_redeclared_in_global_envi_declararion_variable_and_function(self):
        """Redeclared error"""
        input = """
                    var a,b:integer;
                    var foo:real;
                    function foo(x,y:integer):boolean;
                    begin
                        return true;
                    end
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 412))

    def test_another_redeclared_in_global_envi_declararion_function_and_variable(self):
        """Redeclared error"""
        input = """
                    var a,b:integer;
                    function foo(x,y:integer):boolean;
                    begin
                        return true;
                    end
                    var foo:real;
                    procedure main();
                    begin
                        return;
                    end
                """
        expect = "Redeclared Variable: foo"
        self.assertTrue(TestChecker.test(input, expect, 413))

    def test_undeclared_variable_in_assignment(self):
        """Undeclared error"""
        input = """
                    var a,b:integer;
                    procedure main();
                    begin
                        a := c;
                        return;
                    end
                """
        expect = "Undeclared Identifier: c"
        self.assertTrue(TestChecker.test(input, expect, 414))

    def test_undeclared_variable_in_array_cell(self):
        """Undeclared error"""
        input = """
                    var a,b:integer;
                    procedure main();
                    begin
                        arr[1] := a;
                        return;
                    end
                """
        expect = "Undeclared Identifier: arr"
        self.assertTrue(TestChecker.test(input, expect, 415))

    def test_undeclared_variable_in_call_statement(self):
        """Undeclared error"""
        input = """
                    var a,b:integer;
                    procedure main();
                    begin
                        putInt(number);
                        return;
                    end
                """
        expect = "Undeclared Identifier: number"
        self.assertTrue(TestChecker.test(input, expect, 416))

    def test_undeclared_procedure_in_call_statement(self):
        """Undeclared error"""
        input = """
                    var a,b:integer;
                    procedure main();
                    begin
                        putInts(a);
                        return;
                    end
                """
        expect = "Undeclared Procedure: putInts"
        self.assertTrue(TestChecker.test(input, expect, 417))

    def test_undeclared_function_in_call_expression(self):
        """Undeclared error"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                    begin
                        number := getInts();
                        return;
                    end
                """
        expect = "Undeclared Function: getInts"
        self.assertTrue(TestChecker.test(input, expect, 418))

    def test_type_mismatch_in_array_cell_expression_array_the_subcripting_E1_is_not_array_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                    begin
                        number := (a + b)[1];
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: ArrayCell(BinaryOp(+,Id(a),Id(b)),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 421))

    def test_another_type_mismatch_in_array_cell_expression_the_subcripting_E2_is_not_Int_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                    begin
                        number := arr[1.5];
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: ArrayCell(Id(arr),FloatLiteral(1.5))"
        self.assertTrue(TestChecker.test(input, expect, 422))

    def test_another_type_mismatch_in_array_cell_expression_the_subcripting_E2_is_Float_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                    begin
                        number := arr[r];
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: ArrayCell(Id(arr),Id(r))"
        self.assertTrue(TestChecker.test(input, expect, 423))

    def test_another_type_mismatch_in_array_cell_expression_the_subcripting_E1_is_not_array_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    function foo(a,b:integer): integer;
                    var arr: array [1 .. 5] of integer;
                        rs:integer;
                    begin
                        return rs;
                    end
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                    begin
                        arr[0] := foo(a,b)[0];
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: ArrayCell(CallExpr(Id(foo),[Id(a),Id(b)]),IntLiteral(0))"
        self.assertTrue(TestChecker.test(input, expect, 424))

    def test_another_type_mismatch_in_array_cell_expression_the_subcripting_E2_is_call_expression_which_not_Int_type(
            self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    function foo(a,b:integer): array [1 .. 5] of integer;
                    var arr: array [1 .. 5] of integer;
                        rs:integer;
                    begin
                        return arr;
                    end
                    function goo(): boolean;
                    begin
                        return true;
                    end
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                    begin
                        arr[0] := foo(a,b)[goo()];
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: ArrayCell(CallExpr(Id(foo),[Id(a),Id(b)]),CallExpr(Id(goo),[]))"
        self.assertTrue(TestChecker.test(input, expect, 425))

    def test_type_mismatch_in_unary_expression_with_the_expression_is_boolean_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        number:= -bool;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: UnaryOp(-,Id(bool))"
        self.assertTrue(TestChecker.test(input, expect, 426))

    def test_type_mismatch_in_unary_expression_with_call_expression_inside(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        number:= -foo("foo");
                        return;
                    end
                    function foo(s:string): string;
                    begin
                        return s;
                    end
                """
        expect = "Type Mismatch In Expression: UnaryOp(-,CallExpr(Id(foo),[StringLiteral(foo)]))"
        self.assertTrue(TestChecker.test(input, expect, 427))

    def test_type_mismatch_in_NOT_unary_expression_with_call_expression_inside(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        bool:= not foo("foo");
                        return;
                    end
                    function foo(s:string): string;
                    begin
                        return s;
                    end
                """
        expect = "Type Mismatch In Expression: UnaryOp(not,CallExpr(Id(foo),[StringLiteral(foo)]))"
        self.assertTrue(TestChecker.test(input, expect, 428))

    def test_type_mismatch_in_NOT_unary_expression_with_the_expression_is_not_boolean_type(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        bool:= not(foo(a) + b);
                        return;
                    end
                    function foo(a:integer): integer;
                    begin
                        return a;
                    end
                """
        expect = "Type Mismatch In Expression: UnaryOp(not,BinaryOp(+,CallExpr(Id(foo),[Id(a)]),Id(b)))"
        self.assertTrue(TestChecker.test(input, expect, 429))

    def test_type_mismatch_in_binary_expression_with_string_plus_integer(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a:= "s" + b;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(+,StringLiteral(s),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 430))

    def test_type_mismatch_in_binary_expression_with_integer_subtract_boolean(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a:= b - bool;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(-,Id(b),Id(bool))"
        self.assertTrue(TestChecker.test(input, expect, 431))

    def test_type_mismatch_in_binary_expression_with_boolean_multiply_integer(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a:= bool * a;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(*,Id(bool),Id(a))"
        self.assertTrue(TestChecker.test(input, expect, 432))

    def test_type_mismatch_in_binary_expression_with_string_divide_boolean(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a := "s" / bool;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(/,StringLiteral(s),Id(bool))"
        self.assertTrue(TestChecker.test(input, expect, 433))

    def test_type_mismatch_in_binary_expression_with_integer_div_real(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a := b div r;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(div,Id(b),Id(r))"
        self.assertTrue(TestChecker.test(input, expect, 434))

    def test_type_mismatch_in_binary_expression_with_real_mod_integer(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;
                        bool: boolean;
                    begin
                        a := 5.3 mod 2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(mod,FloatLiteral(5.3),IntLiteral(2))"
        self.assertTrue(TestChecker.test(input, expect, 435))

    def test_type_mismatch_in_binary_expression_with_string_greater_than_string(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := s1 > s2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(>,Id(s1),Id(s2))"
        self.assertTrue(TestChecker.test(input, expect, 436))

    def test_type_mismatch_in_binary_expression_with_boolean_less_than_boolean(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := bool1 < bool2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(<,Id(bool1),Id(bool2))"
        self.assertTrue(TestChecker.test(input, expect, 437))

    def test_type_mismatch_in_binary_expression_with_string_less_than_or_equal_boolean(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := s1 <= bool2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(<=,Id(s1),Id(bool2))"
        self.assertTrue(TestChecker.test(input, expect, 438))

    def test_type_mismatch_in_binary_expression_with_boolean_greater_than_or_equal_string(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := bool2 >= s1;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(>=,Id(bool2),Id(s1))"
        self.assertTrue(TestChecker.test(input, expect, 439))

    def test_type_mismatch_in_binary_expression_with_array_equal_to_another_array(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr1,arr2: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := arr1 = arr2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(=,Id(arr1),Id(arr2))"
        self.assertTrue(TestChecker.test(input, expect, 440))

    def test_type_mismatch_in_binary_expression_with_string_differ_to_a_float_number(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr1,arr2: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := "2.1e17" <> 2.05e-19;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(<>,StringLiteral(2.1e17),FloatLiteral(2.05e-19))"
        self.assertTrue(TestChecker.test(input, expect, 441))

    def test_type_mismatch_in_binary_expression_with_string_and_a_string(self):
        """Type Mismatch In Expression"""
        input = """
                    var a,b:integer;
                    procedure main();
                    var number : integer;
                        arr1,arr2: array [1 .. 5] of integer;
                        r: real;s1,s2:string;
                        bool1,bool2: boolean;
                    begin
                        bool1 := s1 and s2;
                        return;
                    end
                """
        expect = "Type Mismatch In Expression: BinaryOp(and,Id(s1),Id(s2))"
        self.assertTrue(TestChecker.test(input, expect, 442))

    def test_type_mismatch_in_binary_expression_with_integer_or_a_float_number(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 5] of integer;
                           r: real;s1,s2:string;
                           bool1,bool2: boolean;
                       begin
                           bool1 := a and r;
                           return;
                       end
                   """
        expect = "Type Mismatch In Expression: BinaryOp(and,Id(a),Id(r))"
        self.assertTrue(TestChecker.test(input, expect, 443))

    def test_type_mismatch_in_binary_expression_with_float_and_then_an_integer_number(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 5] of integer;
                           r: real;s1,s2:string;
                           bool1,bool2: boolean;
                       begin
                           bool1 := r and then b;
                           return;
                       end
                   """
        expect = "Type Mismatch In Expression: BinaryOp(andthen,Id(r),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 444))

    def test_type_mismatch_in_binary_expression_with_array_or_else_another_array(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 5] of integer;
                           r: real;s1,s2:string;
                           bool1,bool2: boolean;
                       begin
                           bool1 := arr1 or else arr2;
                           return;
                       end
                   """
        expect = "Type Mismatch In Expression: BinaryOp(orelse,Id(arr1),Id(arr2))"
        self.assertTrue(TestChecker.test(input, expect, 445))

    def test_type_mismatch_in_call_expression_with_parameter_in_wrong_type(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 5] of integer;
                           r: real;s1,s2:string;
                           bool1,bool2: boolean;
                       begin
                            r := foo(2.5);
                           return;
                       end
                       function foo(n:integer):real;
                       begin
                            return n;
                       end
                   """
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[FloatLiteral(2.5)])"
        self.assertTrue(TestChecker.test(input, expect, 446))

    def test_another_type_mismatch_in_call_expression_with_parameter_is_an_array(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 2] of integer;
                           r: real;s1,s2:string;
                           bool1,bool2: boolean;
                        var arr3: array [5 .. 6] of real;
                            arr4: array [9 .. 10] of string;
                       begin
                            arr4[0] := foo(arr1,arr2)[10];
                           return;
                       end
                       function foo(x:array[1 .. 2] of integer; y: array [5 .. 6] of real):array [9 .. 10] of string;
                       var arr: array[9 .. 10] of string;
                       begin
                            return arr;
                       end
                   """
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[Id(arr1),Id(arr2)])"
        self.assertTrue(TestChecker.test(input, expect, 449))

    def test_another_type_mismatch_in_call_expression_with_illegal_parameter(self):
        """Type Mismatch In Expression"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 2] of integer;
                           r1,r2: real;s1,s2:string;
                           bool1,bool2: boolean;
                        var arr3: array [5 .. 6] of real;
                            arr4: array [9 .. 10] of string;
                       begin
                           r1:=foo(r2);
                           return;
                       end
                       function foo(x: integer):real;
                       begin
                            return x;
                       end
                   """
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[Id(r2)])"
        self.assertTrue(TestChecker.test(input, expect, 450))

    def test_type_mismatch_in_if_statement_with_condition_expression_is_integer(self):
        """Type Mismatch In Statement"""
        input = """
                       var a,b:integer;
                       procedure main();
                       var number : integer;
                           arr1,arr2: array [1 .. 2] of integer;
                           r1,r2: real;s1,s2:string;
                           bool1,bool2: boolean;
                        var arr3: array [5 .. 6] of real;
                            arr4: array [9 .. 10] of string;
                       begin
                            if b then
                                r1:=foo(a);
                            return;
                       end
                       function foo(x: integer):real;
                       begin
                            return x;
                       end
                   """
        expect = "Type Mismatch In Statement: If(Id(b),[AssignStmt(Id(r1),CallExpr(Id(foo),[Id(a)]))],[])"
        self.assertTrue(TestChecker.test(input, expect, 451))