# Student name: Nguyễn Quốc Dũng
# Student ID: 1770471

import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    # def test_simple_program(self):
    #     """Simple program: int main() {} """
    #     input = """int main() {}"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,201))
    #
    # def test_more_complex_program(self):
    #     """More complex program"""
    #     input = """int main () {
    #         putIntLn(4);
    #     }"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,202))
    #
    # def test_wrong_miss_close(self):
    #     """Miss ) int main( {}"""
    #     input = """int main( {}"""
    #     expect = "Error on line 1 col 10: {"
    #     self.assertTrue(TestParser.test(input,expect,203))
######################################################
# My testcases
#     def test_01_var_decl_simple(self):
#         input = """vAr a , b , c : integer ; """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 204))
#
    def test_02_multiple_var_decl(self):
        input = """var a , b , c : integer; e,f: real;"""
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 205))
#
    def test_03_var_decl_combo(self):
        input = """var  a,b,c: integer;
                        e,f: real;
                        d : array [ 1 .. 5 ] of integer;"""
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 206))
#
#     def test_04_multi_var_decls_array(self):
#         input = """var a,b,c: integer;
#                     e,f: real;
#                     d,x,y : array [ 1 .. 5 ] of integer;"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 207))
#
#     def test_05_function_decl_simple(self):
#         input = """function foo(): integer;
#                 begin end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 208))
#
#     def test_06_function_decl_withpara(self):
#         input = """function foo(a,b: integer): integer;
#                  begin  end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 209))
#
#     def test_07_function_decl_2(self):
#         input = """function foo(a,b: integer; c:real): array [1 .. 2] of integer;
#                 begin end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 210))
#
#     def test_08_simple_procedure_declare(self):
#         input = """procedure foo();
#                 begin end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input,expect,211))
#
#     def test_09_procedure_declare_wpara(self):
#         input = """procedure foo(a,b:integer);
#                 begin end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input,expect,212))
#
#     def test_10_procedure_declare_wpara2(self):
#         input = """procedure foo(a,b:integer; c : real);
#                 begin end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input,expect,213))
#
#     def test_11_error_vardecl_not_terminate(self):
#         input = """var _Abc: string"""
#         expect = "Error on line 1 col 16: <EOF>"
#         self.assertTrue(TestParser.test(input,expect,214))
#
#     def test_12_error_array_illegal(self):
#         input = """var x : array [ ] of integer; """
#         expect = "Error on line 1 col 16: ]"
#         self.assertTrue(TestParser.test(input,expect,215))
#
#     def test_13_error_type_in_array(self):
#         input = """var x : array [1 .. 5] of array; """
#         expect = "Error on line 1 col 26: array"
#         self.assertTrue(TestParser.test(input,expect,216))
#
#     def test_14_error_type_in_var(self):
#         input = """var x,y,z : array; """
#         expect = "Error on line 1 col 17: ;"
#         self.assertTrue(TestParser.test(input,expect,217))
#
#     def test_15_error_dim_array(self):
#         input = """var i : array [ 1 .. 2 , 3 .. 4 ] of integer ; """
#         expect = "Error on line 1 col 23: ,"
#         self.assertTrue(TestParser.test(input,expect,218))
#
#     def test_16_simple_funct(self):
#         input = """FuNctIon foo() : integer;
#                 var x, y : real;
#                 begin end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 219))
# #
#     def test_17_simple_if_statement(self):
#         input = """ Function goo() : integer;
#                 begin
#                     If x = 20 then x := x + 1;
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 220))
#
#     def test_18_invalid_assign_in_if(self):
#         input = """ Function boo() : real;
#                 begin
#                     If x := 20 then x := x + 1;
#                 end"""
#         expect = "Error on line 3 col 25: :="
#         self.assertTrue(TestParser.test(input, expect, 221))
#
#     def test_19_simple_if_no_else(self):
#         input = """Var YN: string;
#                 Function goo() : integer;
#                 Begin
#                     Writeln("Y(YES) or N(NO)?");
#                     YN := Readkey;
#                     If YN = y Then Writeln("Halt"); (* Halt - exit *)
#                     If YN = n Then Writeln("Why not? Exiting...");
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 222))
#
#     def test_20_nested_if_statement(self):
#         input = """ Function goo() : real;
#                 begin
#                     If x = 20 then
#                     begin
#                         if x < 10 then x := x + 1;
#                     end
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 223))
#
#     def test_21_simple_dangling(self):
#         input = """procedure main();
#                 Begin
#                     IF x = 0 THEN
#                         IF y = 0 THEN
#                             z := 1;
#                     ELSE
#                         w := 2;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 224))
#
#     def test_22_dangling_else(self):
#         input = """ var count : integer ;
#                 function foo(result : integer) : integer;
#                 begin
#                     if count = 0 then
#                         if count = 1 then
#                             if count = 2 then
#                                 result := 0;
#                         else
#                             result := 1;
#                     else
#                         result := 2;
#                 end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 225))
# # #
#     def test_23_if_else_fibonacci(self):
#         input = """ function fib(n:integer): integer;
#                 var i:integer;
#                 begin
#                     if (n <= 2) then fib := 1;
#                     else fib := fib(n-1) + fib(n-2);
#                     for i := 1 to 16 do
#                         write(fib(i), ", ");
#                     writeln("...");
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 226))
# #
#     def test_24_if_else_factorial(self):
#         input = """ function fact(n: integer): integer;
#                 var n: integer;
#                 begin
#                     if n = 0 then fact := 1;
#                     else fact := n * fact(n - 1);
#                     for n := 0 to 16 do
#                     writeln(n, "! = ", fact(n));
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 227))
#
#     def test_25_simple_forloop(self):
#         input = """Var Counter : integer;
#                 function simple(): boolean;
#                 Begin
#                     For Counter := 1 to 7 do
#                         Writeln("for loop");
#                     Readln();
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 228))
#
#     def test_26_array_variables(self):
#         input = """  Var myVar: integer;  myArray: array[1 .. 5] of integer;
#                 procedure abc();
#                 Begin
#                     myArray[2]:= 25;
#                     myVar := myArray[2];
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 229))
# #
#     def test_27_array_variables_multi(self):
#         input = """ procedure test();
#                 Var i : integer;
#                 myIntArray : array[1 .. 20] of integer;
#         	    myBoolArray : array[1 .. 20] of boolean;
#         	    myRealArray : array[1 .. 20] of real;
#         	    myStringArray : array[1 .. 20] of string;
#         	    Begin
#         	    	For i := 1 to Length(myIntArray) do
#         	    		Begin
#         	    		    myIntArray[i] := 1;
#         	    		    myRealArray[i] := 0.5;
#         	    		    myStringArray[i] := "Python";
#         	    		    myBoolArray[i] := true;
#         	    		End
#         	    End """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 230))
#
#     def test_28_nested_forloop(self):
#         input = """ Procedure BubbleSort(numbers: array of integer; size: integer);
#                 Var i, j, temp: integer;
#                 Begin
#                     For i := size - 1 DownTo 1 do
#                         For j := 2 to i do
#                             if(numbers[j - 1] > numbers[j]) Then
#                             Begin
#                                 temp := numbers[j - 1];
#                                 numbers[j - 1] := numbers[j];
#                                 numbers[j] := temp;
#                             End
#                 End """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 231))
#
#     def test_29_function_call(self):
#         input = """ procedure foo(a: array[1 .. 2] of real);
#                 begin end
#                 procedure goo(x: array[1 .. 2] of real );
#                 var y: array[2 .. 3] of real;
#                     z: array[1 .. 2] of integer;
#                 begin
#                     foo(x); // CORRECT
#                     foo(y); // WRONG
#                     foo(z); // WRONG
#                 end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 232))
#
#     def test_30_boolean_variables(self):
#         input = """ function foo(): real;
#                 Var quit: boolean; a: string;
#                 Begin
#                     Writeln("Type exit to quit:");
#                     Readln(a);
#                     If (a = "exit") Then
#                         quit := True;
#                 End """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 233))
#
#     def test_31_assignment(self):
#         input = """ function foo(): real;
#                 Var a,x: integer; b: array[1 .. 10] of integer;
#                 Begin
#                     If (a = 1) Then
#                         a := b[10] := foo( )[3] := x := 1 ;
#                 End """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 234))
# #
#     def test_32_break_continue(self):
#         input = """ var a: integer;
#                 function foo(): real;
#                 begin
#                     a := 10;
#                     while a < 20 do
#                     begin
#                         writeln("value of a: ", a);
#                         a := a + 1;
#                         if (a > 15) then
#                             break;
#                         else
#                             continue;
#                     end
#                 end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 235))
#
#     def test_33_simple_with_do(self):
#         input = """Var d : real;
#                 function foo(): real;
#                 Begin
#                     with a, b: integer ; c: array [1 .. 2] of real;
#                     do d := c[a] + b ;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 236))
# #
#     def test_34_main_procedure(self):
#         input = """procedure main();
#                 Begin
#                     if (x = 1) then
#                         return true;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 237))
#
#     def test_35_var_decl_simple(self):
#         input = """function foo(): real;  begin x:=5; x:=y:=z:=1+2; end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 238))
#
#     def test_36_return_state(self):
#         input = """function foo(): real;
#                 begin
#                     if a > b then return 2.3 ;
#                     else return 2 ;
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input,expect, 239))
#
#     def test_37_others_return_state(self):
#         input = """ function foo(b: array[1 .. 2] of integer): array[2 .. 3] of real;
#                 var a: array[2 .. 3] of real;
#                 begin
#                     if x=y then return a ; // Correct
#                     else return b ; // Wrong
#                 end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 240))
#
#     def test_38_with_do_statement(self):
#         input = """function foo(): real;
#                 Begin
#                     with a, b: integer ; c: array[ 1 .. 2 ] of real;
#                     do d := c[a] + b ;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 241))
#
#     def test_39_while_do_loop(self):
#         input = """ Var Ch : String;
#                 function foo(): real;
#                 Begin
#                 	Writeln( "Press q to exit");
#                 	Ch := Readkey;
#                 	While Ch <> "q" do
#                 		Begin
#                 		    Writeln( "Please press q to exit");
#                 		    Ch := Readkey;
#                 		End
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 242))
#
#     def test_40_with_none_do_statement(self):
#         input = """function foo(): real;
#                 Begin
#                     with a, b: integer ; c: array[ 1 .. 2 ] of real;
#                 End"""
#         expect = "Error on line 4 col 16: End"
#         self.assertTrue(TestParser.test(input, expect, 243))
#
#     def test_41_while_none_do_statement(self):
#         input = """function goo(): real;
#                 Begin
#                     while a <> b
#                         Writeln("Please check something");
#                 End"""
#         expect = "Error on line 4 col 24: Writeln"
#         self.assertTrue(TestParser.test(input, expect, 244))
#
#     def test_42_for_error_assign(self):
#         input = """ procedure goo();
#                 Var Counter : Integer;
#                 Begin
#                     For Counter = 1 to 7 do
#                         Writeln("for loop");
#                         Readln;
#                 End"""
#         expect = "Error on line 4 col 32: ="
#         self.assertTrue(TestParser.test(input, expect, 245))
#
#     def test_43_for_error_none_do(self):
#         input = """ procedure foo();
#                 Var Counter : Integer;
#                 Begin
#                     For Counter := 1 to 7
#                         Writeln("for loop");
#                         Readln;
#                 End"""
#         expect = "Error on line 5 col 24: Writeln"
#         self.assertTrue(TestParser.test(input, expect, 246))
#
#     def test_44_Begin_none_End(self):
#         input = """ procedure goo();
#                 Var Counter : Integer;
#                 Begin
#                     For Counter := 1 to 7 do
#                         Writeln("for loop");
#                         Readln;"""
#         expect = "Error on line 6 col 30: ;"
#         self.assertTrue(TestParser.test(input,  expect, 247))
#
#     def test_45_End_none_Begin(self):
#         input = """ Var Counter : Integer;
#                     For Counter := 1 to 7 do
#                         Writeln("for loop");
#                         Readln;
#                     End"""
#         expect = "Error on line 2 col 20: For"
#         self.assertTrue(TestParser.test(input, expect, 248))
#
#     def test_46_Assign_statement(self):
#         input = """ function abc(): integer;
#                 Begin
#                     a := b[10] := foo( )[3] := x := 1;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 249))
#
#     def test_47_some_builtin_proc(self):
#         input = """ procedure putFloat(f: real);
#                     Begin End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 250))
#
#     def test_48_some_builtin_func(self):
#         input = """ function getFloat(): real;
#                         Begin End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 251))
#
#     def test_49_Prime_program(self):
#         input = """function isPrime(n: integer): boolean;
#                 var prime: boolean;  i,j: integer;
#                 begin
#                     prime := true;
#                     if n<2 then primer := false;
#                     if n>3 then if (n mod 2 = 0) then primer := false;
#                     if n>5 then
#                         begin k := round(n/6);
#                             if (n<>(6*k-1) and n<>(6*k+1)) then primer := false;
#                             else
#                                 begin i := round(sqrt(n))+1; j := 3;
#                                     while prime and (j<i) do
#                                         begin prime := (n mod j)<>0; j := j+2; end
#                                 end
#                         end
#                         isPrime := prime;
#                 end
#                 procedure main();
#                 begin
#                     for n:=0 to 1234567898765 do
#                         if isPrime(n) then writlen(n, "is Prime");
#                 end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 252))
#
#     def test_50_n_LHS(self):
#         input = """ procedure main();
#                     begin
#                         a := b := c := 2+5*4;
#                         a := b[10] := foo()[3] := x := 1;
#                     end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 253))
#
#
#     def test_51_RHS(self):
#             input = """ procedure main();
#                         begin
#                             arr[1]:=10*2;
#                             arr[2]:=arr[1+1]-100;
#                             arr[3]:=foo(2)[2+1]-100;
#                             foo(2)[3+x] := a[b[2]] +3;
#                             a[5] := 1 - a[1] + a[2] * 2-4+5*6;
#                         end"""
#             expect = "successful"
#             self.assertTrue(TestParser.test(input, expect, 254))
#
#     def test_52_wrong_LHS(self):
#         input = """procedure main();
#                     begin
#                         5-2*10/2:=4+5*6;
#                     end"""
#         expect = "Error on line 3 col 32: :="
#         self.assertTrue(TestParser.test(input, expect, 255))
#
#     def test_53_invalid_assign(self):
#         input = """procedure main();
#                     begin
#                         3-2*10:=a;
#                     end"""
#         expect = "Error on line 3 col 30: :="
#         self.assertTrue(TestParser.test(input, expect, 256))
#
#     def test_54_Rela_Logical_Boolean(self):
#         input = """ procedure check();
#                 Var bool : Boolean; A, B : Integer;
#                 Begin
#                     A := 10;
#                     B := 20;
#                     bool := False;
#                     bool := (A = 10) OR (B = 10);
#                     Writeln(bool); { outputs TRUE }
#                     bool := (A = 10) AND (B = 10);
#                     Writeln(bool); { outputs FALSE }
#                     If not(B = 20 or A = 10) Then bool :=false;
#                 End"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 257))
#
#     def test_55_And_Then_Or_Else_multispace(self):
#         input = """ procedure main ();
#                     begin
#                         if a<>0 and c=1 and       then b=1 then
#                             begin  print("hello world"); end
#                         else if b<0 or f= false or        else d= .55 then
#                             begin print("welcome Python"); end
#                     end """
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 258))
#
#     def test_56_break_continue_free(self):
#         input = """procedure main();
#                     begin break;  continue; end"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 259))
#
#     def test_57_just_test(self):
#         input = """ procedure main();
#                 BEGIN
#                     BEGIN
#                         number := 2;
#                         a := number;
#                         b := 10 * a + 10 * number / 4;
#                         c := a - - b;
#                     END
#                     x := 11;
#                 END"""
#         expect = "successful"
#         self.assertTrue(TestParser.test(input, expect, 260))
#
#     def test_58_callStmt(self):
#             input = """ procedure foo(a : integer);
#                     begin  end
#                     function goo( a : integer): integer;
#                     begin end
#                     procedure main();
#                     begin
#                          a:=x+y*z/foo(2);
#                          goo(4*x);
#                     end"""
#             expect = "successful"
#             self.assertTrue(TestParser.test(input, expect, 261))
#
#     def test_59_callboth(self):
#             input = """ procedure foo(a : integer);
#                     begin  end
#                     function goo( a : integer): integer;
#                     begin end
#                     procedure main();
#                     begin
#                          if goo(2) then
#                          goo(foo(2));
#                     end"""
#             expect = "successful"
#             self.assertTrue(TestParser.test(input, expect, 262))


