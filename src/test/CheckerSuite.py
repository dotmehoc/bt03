import unittest
from TestUtils import TestChecker
from AST import *

class CheckerSuite(unittest.TestCase):
	def test_01(self):
		input = """ var b,c,a,a: integer;
	                procedure main();
	                begin
	                    return;
	                end """
		expect = "Redeclared Variable: a"
		self.assertTrue(TestChecker.test(input, expect, 401))

	def test_02(self):
		input = """ var a,b,c,d,b: integer;
	                procedure main();
	                begin
	                    return;
	                end """
		expect = "Redeclared Variable: b"
		self.assertTrue(TestChecker.test(input, expect, 402))

	def test_03(self):
		input = """var a,b: integer; d:real;
	                procedure m(); var n:integer;
	                begin	end
	                procedure n(); var c:integer;
	                begin  end
	                function c(a,b:integer):real;
	                begin
	                    return 1.0;
	                end
	                procedure d(); begin end
	                procedure main();
	                begin
	                    m(); n(); c(1,2); d();
	                    return;
	                end """
		expect = "Redeclared Procedure: d"
		self.assertTrue(TestChecker.test(input, expect, 403))

	def test_04(self):
		input = """ var c: integer; d:real; e:boolean;
	                function foo(b,a:integer; m:array[1 .. 3] of integer): integer;
	                begin
	                    with a: array [1 .. 5] of integer; do
	                        return 0;
	                end
	                function foo1(b,c:integer;m: Array   [1 .. 3] of integer): integer;
	                var c: arrAY [1 .. 5] of integer;
	                begin
	                    return 0;
	                end
	                procedure main();
	                var a: aRRay[1 .. 3] of integer;
	                begin
	                    a[3] := foo(1, foo1(1,2,a), a);
	                    return;
	                end """
		expect = "Redeclared Variable: c"
		self.assertTrue(TestChecker.test(input, expect, 404))

	def test_05(self):
		input = """ var a: integer;
	                procedure funcA(); var b: integer;
	                begin
	                    b := a := 7;
	                end
	                function sum(b: integer): integer; var d: integer;
	                begin
	                    d:= 7;
	                    return a+b+d;
	                end
	                procedure main (); var m: array [1 .. 10] of integer;
	                begin
	                    m[1] := sum(3);
	                    funcA();
	                    a := 1 + n[1];
	                    return;
	                end """
		expect = "Undeclared Identifier: n"
		self.assertTrue(TestChecker.test(input, expect, 405))

	def test_06(self):
		input = """ var a: integer; d:real; e:boolean;
	                function foo(a,b:integer; m:array[1 .. 3] of integer): integer;
	                begin
	                    if a > b then
	                        a := 1 + b;
	                    else
	                        a:=b + 2;
	                    return a;
	                end
	                function foo1(a:integer): integer; var b,c,d: integer;
	                begin
	                    b:=2; c:=3;
	                    if a>b then
	                        d:=a+c;
	                    else
	                        d:=foo2(1);
	                    return d;
	                end
	                var b:integer;
	                function foo2(a:integer): integer;
	                begin
	                    while a>5 do
	                        a:=a+1;
	                    return a;
	                end
	                procedure main();
	                begin
	                    a := foo(1, foo1(1), foo2(2));
	                    funy(4);
	                    return;
	                end """
		expect = "Type Mismatch In Expression: CallExpr(Id(foo),[IntLiteral(1),CallExpr(Id(foo1),[IntLiteral(1)]),CallExpr(Id(foo2),[IntLiteral(2)])])"
		self.assertTrue(TestChecker.test(input, expect, 406))

	def test_07(self):
		input = Program([FuncDecl(Id('foo'),[],[],[Return(IntLiteral(1))],IntType()),
						 FuncDecl(Id('foo1'),[],[],[Return(FloatLiteral(1.0))],IntType()),
						 FuncDecl(Id('main'),[],[],
								  [CallStmt(Id('foo'),[]),CallStmt(Id('foo1'),[]), Return(None)],VoidType())])
		expect = "Type Mismatch In Statement: Return(Some(FloatLiteral(1.0)))"
		self.assertTrue(TestChecker.test(input, expect, 407))

	def test_08(self):
		input = """ function foo(): integer;
	                begin
	                    return 1;
	                end
	                function foo1(): integer;
	                begin
	                    return 1.0;
	                end
	                procedure main();
	                begin
	                    foo(); foo1();
	                    return;
	                end """
		expect = "Type Mismatch In Statement: Return(Some(FloatLiteral(1.0)))"
		self.assertTrue(TestChecker.test(input, expect, 408))

	def test_09(self):
		input = """ function foo(): boolean;
	                begin
	                    return 2>1;
	                end
	                function foo1(a:integer): boolean;
	                begin
	                    return a=1;
	                end
	                function foo2(a:integer): boolean;
	                begin
	                    return a;
	                end
	                procedure main();
	                begin
	                    foo(); foo1(1); foo2(2);
	                    return;
	                end """
		expect = "Type Mismatch In Statement: Return(Some(Id(a)))"
		self.assertTrue(TestChecker.test(input, expect, 409))

	def test_10(self):
		input = """ var a,b,c:integer; d:boolean;e:real;m:array [1 .. 100] of integer;
	                function foo(): integer;
	                begin
	                    return 0;
	                end
	                procedure main();
	                begin
	                    b:=foo(); d:=true; a:=m[0]+1; c:=m[d]+1;
	                    return;
	                end """
		expect = "Type Mismatch In Expression: ArrayCell(Id(m),Id(d))"
		self.assertTrue(TestChecker.test(input, expect, 410))

	def test_11(self):
		input = """ var a,b,c:integer;
	                procedure main();
	                var d:boolean;
	                begin
	                    d:= a > b and true;
	                    d:= a > b and 1.0;
	                    return;
	                end """
		expect = "Type Mismatch In Expression: BinaryOp(and,Id(b),BooleanLiteral(true))"
		self.assertTrue(TestChecker.test(input, expect, 411))

	def test_12(self):
		input = """ var a:integer; m:array[1 .. 100] of real;
	                procedure foo();
	                begin
	                    a:= -(-(a - -1));
	                end
	                procedure main (); var a,i,j: integer;
	                begin
	                    foo();
	                    a:= -(m[1] + -1 > 1.2);
	                    return;
	                end """
		expect = "Type Mismatch In Expression: UnaryOp(-,BinaryOp(>,BinaryOp(+,ArrayCell(Id(m),IntLiteral(1)),UnaryOp(-,IntLiteral(1))),FloatLiteral(1.2)))"
		self.assertTrue(TestChecker.test(input, expect, 412))

	def test_13(self):
		input = """ var a:integer; b:real; m:array[1 .. 10] of integer;
	                procedure main ();
	                var a,i,j: integer;
	                begin
	                    b:= m[1] + -1;
	                    b:= b * 1.0 + 1;
	                    b:= not(m[1] = 1);
	                    return;
	                end """
		expect = "Type Mismatch In Statement: AssignStmt(Id(b),UnaryOp(not,BinaryOp(=,ArrayCell(Id(m),IntLiteral(1)),IntLiteral(1))))"
		self.assertTrue(TestChecker.test(input, expect, 413))

	def test_14(self):
		input = """ var a,b,c:integer; m:array[1 .. 10] of integer;
	                procedure foo();
	                begin
	                    while a<100 do
	                    begin
	                        a:=a+1;
	                        break;
	                    end
	                    return;
	                end
	                procedure main (); var a,i,j: integer;
	                begin
	                    while a<100 do
	                    begin
	                        a:= a+1;
	                        break;
	                    end
	                    foo();
	                    if a=100 then
	                        break;
	                    return;
	                end """
		expect = "Break Not In Loop"
		self.assertTrue(TestChecker.test(input, expect, 414))

	def test_15(self):
		input = """ function foo():integer; var a:integer;
	                begin
	                    a:=4;
	                    if a=10 then
	                        return 1;
	                    return 10;
	                end
	                procedure main (); var a: integer;
	                begin
	                    a:=0;
	                    if foo() > 1 then
	                        if foo2() <= 100 then
	                            return;
	                        else
	                            return;
	                    else
	                        begin
	                            a:=2;
	                            return;
	                        end
	                end """
		expect = "Undeclared Function: foo2"
		self.assertTrue(TestChecker.test(input, expect, 415))


# def test_00_Redeclared_Variable(self):
    #     input = Program([VarDecl(Id('b'),IntType()),
    #             VarDecl(Id('c'),IntType()),
    #             VarDecl(Id('a'),IntType()),
    #             VarDecl(Id('a'),IntType()),
    #             FuncDecl(Id('main'),[],[],[Return(None)],VoidType())])
    #     expect = "Redeclared Variable: a"
    #     self.assertTrue(TestChecker.test(input,expect,400))
	#
    # def test_01_Redeclared_Variable(self):
    #     input = Program([VarDecl(Id('a'),IntType()),
    #                      VarDecl(Id('b'),IntType()),
    #                      VarDecl(Id('c'),IntType()),
    #                      VarDecl(Id('d'),IntType()),
    #                      VarDecl(Id('b'), IntType()),
    #                      FuncDecl(Id('main'),[],[],[Return(None)],VoidType())])
    #     expect = "Redeclared Variable: b"
    #     self.assertTrue(TestChecker.test(input,expect,401))
	#
    # def test_02_Redeclared_Procedure(self):
    #     input = Program([VarDecl(Id('c'),IntType()),
	# 					VarDecl(Id('d'),FloatType()),
	# 					VarDecl(Id('e'),BoolType()),
	# 					FuncDecl(Id('foo'),[VarDecl(Id('b'),IntType()),
	# 									VarDecl(Id('a'),IntType()),
	# 									VarDecl(Id('m'),ArrayType(1,3,IntType()))],[],
	# 									[With([VarDecl(Id('a'),ArrayType(1,5,IntType()))],[Return(IntLiteral(0))])],IntType()),
	# 					FuncDecl(Id('foo1'),[VarDecl(Id('b'),IntType()),
	# 									VarDecl(Id('c'),IntType()),
	# 									VarDecl(Id('m'),ArrayType(1,3,IntType()))],
	# 									[VarDecl(Id('c'),ArrayType(1,5,IntType()))],
	# 									[Return(IntLiteral(0))],IntType()),
	# 					FuncDecl(Id('main'),[],[VarDecl(Id('a'),ArrayType(1,3,IntType()))],
	# 							[Assign(ArrayCell(Id('a'),IntLiteral(3)),
	# 										CallExpr(Id('foo'),
	# 										[IntLiteral(1),CallExpr(Id('foo1'),[IntLiteral(1),IntLiteral(2),Id('a')]),Id('a')])),
	# 							Return(None)],VoidType())])
    #     expect = "Redeclared Procedure: "
    #     self.assertTrue(TestChecker.test(input,expect,402))

# Test case 0:
# VarDecl(Id(b),IntType)
# VarDecl(Id(c),IntType)
# VarDecl(Id(a),IntType)
# VarDecl(Id(a),IntType)
# FuncDecl(Id(main),[],VoidType(),[],[Return(None)])
#
# Test case 1:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),IntType)
# VarDecl(Id(c),IntType)
# VarDecl(Id(d),IntType)
# VarDecl(Id(b),IntType)
# FuncDecl(Id(main),[],VoidType(),[],[Return(None)])
#
# Test case 2_Redeclared Procedure:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),IntType)
# VarDecl(Id(d),FloatType)
# FuncDecl(Id(m),[],VoidType(),[VarDecl(Id(n),IntType)],[])
# FuncDecl(Id(n),[],VoidType(),[VarDecl(Id(c),IntType)],[])
# FuncDecl(Id(c),[VarDecl(Id(a),IntType),VarDecl(Id(b),IntType)],FloatType,[],[Return(Some(FloatLiteral(1.0)))])
# FuncDecl(Id(d),[],VoidType(),[],[])
# FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(m),[]),CallStmt(Id(n),[]),CallStmt(Id(c),[IntLiteral(1),IntLiteral(2)]),CallStmt(Id(d),[]),Return(None)])
#
# Test case 3_Redeclared Variable:
# VarDecl(Id(c),IntType)
# VarDecl(Id(d),FloatType)
# VarDecl(Id(e),BoolType)
# FuncDecl(Id(foo),[VarDecl(Id(b),IntType),VarDecl(Id(a),IntType),VarDecl(Id(m),ArrayType(1,3,IntType))],IntType,[],[With([VarDecl(Id(a),ArrayType(1,5,IntType))],[]),Return(Some(IntLiteral(0)))])
# FuncDecl(Id(foo1),[VarDecl(Id(b),IntType),VarDecl(Id(c),IntType),VarDecl(Id(m),ArrayType(1,3,IntType))],IntType,[VarDecl(Id(c),ArrayType(1,5,IntType))],[Return(Some(IntLiteral(0)))])
# FuncDecl(Id(main),[],VoidType(),[VarDecl(Id(a),ArrayType(1,3,IntType))],[AssignStmt(ArrayCell(Id(a),IntLiteral(3)),CallExpr(Id(foo),[IntLiteral(1),CallExpr(Id(foo1),[IntLiteral(1),IntLiteral(2),Id(a)]),Id(a)])),Return(None)])
#
# Test case 4_Undeclared Identifier:
# VarDecl(Id(a),IntType)
# FuncDecl(Id(funcA),[],VoidType(),[VarDecl(Id(b),IntType)],[AssignStmt(Id(a),IntLiteral(7)),AssignStmt(Id(b),Id(a))])
# FuncDecl(Id(sum),[VarDecl(Id(b),IntType)],IntType,[VarDecl(Id(d),IntType)],[AssignStmt(Id(d),IntLiteral(7)),Return(Some(BinaryOp(+,BinaryOp(+,Id(a),Id(b)),Id(d))))])
# FuncDecl(Id(main),[],VoidType(),[VarDecl(Id(m),ArrayType(1,10,IntType))],[AssignStmt(ArrayCell(Id(m),IntLiteral(1)),CallExpr(Id(sum),[IntLiteral(3)])),CallStmt(Id(funcA),[]),AssignStmt(Id(a),BinaryOp(+,IntLiteral(1),ArrayCell(Id(n),IntLiteral(1)))),Return(None)])
#
# Test case 5_Undeclared Procedure:
# VarDecl(Id(a),IntType)
# FuncDecl(Id(foo),[VarDecl(Id(a),IntType),VarDecl(Id(b),IntType)],IntType,[],[If(BinaryOp(>,Id(a),Id(b)),[AssignStmt(Id(a),BinaryOp(+,IntLiteral(1),Id(b)))],[AssignStmt(Id(a),BinaryOp(+,Id(b),IntLiteral(2)))]),Return(Some(Id(a)))])
# FuncDecl(Id(foo1),[VarDecl(Id(a),IntType)],IntType,[VarDecl(Id(b),IntType),VarDecl(Id(c),IntType),VarDecl(Id(d),IntType)],[AssignStmt(Id(b),IntLiteral(2)),AssignStmt(Id(c),IntLiteral(3)),If(BinaryOp(>,Id(a),Id(b)),[AssignStmt(Id(d),BinaryOp(+,Id(a),Id(c)))],[AssignStmt(Id(d),BinaryOp(+,Id(b),CallExpr(Id(foo2),[IntLiteral(1)])))]),Return(Some(Id(d)))])
# VarDecl(Id(b),IntType)
# FuncDecl(Id(foo2),[VarDecl(Id(a),IntType)],IntType,[],[While(BinaryOp(>,Id(a),IntLiteral(5)),[AssignStmt(Id(a),BinaryOp(+,Id(a),IntLiteral(1)))]),Return(Some(Id(a)))])
# FuncDecl(Id(main),[],VoidType(),[],[AssignStmt(Id(a),CallExpr(Id(foo),[CallExpr(Id(foo1),[IntLiteral(1)]),CallExpr(Id(foo2),[IntLiteral(2)])])),CallStmt(Id(funy),[IntLiteral(4)]),Return(None)])
#
# Test case 6_Undeclared Procedure:
# FuncDecl(Id(main),[],VoidType(),[VarDecl(Id(a),IntType),VarDecl(Id(i),IntType),VarDecl(Id(j),IntType)],[For(Id(i)IntLiteral(1),IntLiteral(10),True,[AssignStmt(Id(j),IntLiteral(2))]),For(Id(a)IntLiteral(0),BinaryOp(=,IntLiteral(5),IntLiteral(3)),True,[AssignStmt(Id(j),IntLiteral(1))]),Return(None)])
#
# Test case 7_Undeclared Procedure:
# FuncDecl(Id(foo),[],IntType,[],[Return(Some(IntLiteral(1)))])
# FuncDecl(Id(foo1),[],IntType,[],[Return(Some(FloatLiteral(1.0)))])
# FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(foo),[]),CallStmt(Id(foo1),[]),Return(None)])
#
# Test case 8_Undeclared Procedure:
# FuncDecl(Id(foo),[],BoolType,[],[Return(Some(BinaryOp(>,IntLiteral(2),IntLiteral(1))))])
# FuncDecl(Id(foo1),[VarDecl(Id(a),IntType)],BoolType,[],[Return(Some(BinaryOp(=,Id(a),IntLiteral(1))))])
# FuncDecl(Id(foo2),[VarDecl(Id(a),IntType)],BoolType,[],[Return(Some(Id(a)))])
# FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(foo),[]),CallStmt(Id(foo1),[IntLiteral(1)]),CallStmt(Id(foo2),[IntLiteral(2)]),Return(None)])
#
# Test case 9_Type Mismatch In Expression:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),IntType)
# VarDecl(Id(c),IntType)
# VarDecl(Id(d),BoolType)
# VarDecl(Id(e),FloatType)
# VarDecl(Id(m),ArrayType(1,100,IntType))
# FuncDecl(Id(foo),[],IntType,[],[Return(Some(IntLiteral(0)))])
# FuncDecl(Id(main),[],VoidType(),[],[AssignStmt(Id(b),CallExpr(Id(foo),[])),AssignStmt(Id(d),BooleanLiteral(True)),AssignStmt(Id(a),BinaryOp(+,ArrayCell(Id(m),IntLiteral(0)),IntLiteral(1))),AssignStmt(Id(c),BinaryOp(+,ArrayCell(Id(m),Id(d)),IntLiteral(1))),Return(None)])
#
# Test case 10_Type Mismatch In Expression:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),IntType)
# VarDecl(Id(c),IntType)
# FuncDecl(Id(main),[],VoidType(),[VarDecl(Id(d),BoolType)],[AssignStmt(Id(d),BinaryOp(and,BinaryOp(>,Id(a),Id(b)),BooleanLiteral(True))),AssignStmt(Id(d),BinaryOp(and,BinaryOp(>,Id(a),Id(b)),FloatLiteral(1.0))),Return(None)])
#
# Test case 11_Type Mismatch In Expression:
# VarDecl(Id(a),IntType)
# VarDecl(Id(m),ArrayType(1,100,FloatType))
# FuncDecl(Id(foo),[],VoidType(),[],[AssignStmt(Id(a),UnaryOp(-,UnaryOp(-,BinaryOp(-,Id(a),UnaryOp(-,IntLiteral(1))))))])
# FuncDecl(Id(main),[],VoidType(),[],[CallStmt(Id(foo),[]),AssignStmt(Id(a),UnaryOp(-,BinaryOp(>,BinaryOp(+,ArrayCell(Id(m),IntLiteral(1)),UnaryOp(-,IntLiteral(1))),FloatLiteral(1.2)))),Return(None)])
#
# Test case 12_Undeclared Procedure:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),FloatType)
# VarDecl(Id(m),ArrayType(1,10,IntType))
# FuncDecl(Id(main),[],VoidType(),[],[AssignStmt(Id(b),BinaryOp(+,ArrayCell(Id(m),IntLiteral(1)),UnaryOp(-,IntLiteral(1)))),AssignStmt(Id(b),BinaryOp(*,Id(b),BinaryOp(+,FloatLiteral(1.0),IntLiteral(1)))),AssignStmt(Id(b),UnaryOp(not,BinaryOp(=,ArrayCell(Id(m),IntLiteral(1)),IntLiteral(1)))),Return(None)])
#
# Test case 13_Break Not In Loop:
# VarDecl(Id(a),IntType)
# VarDecl(Id(b),IntType)
# VarDecl(Id(c),IntType)
# FuncDecl(Id(foo),[],VoidType(),[],[While(BinaryOp(<,Id(a),IntLiteral(100)),[AssignStmt(Id(a),BinaryOp(+,Id(a),IntLiteral(1))),Break]),Return(None)])
# FuncDecl(Id(main),[],VoidType(),[],[While(BinaryOp(<,Id(a),IntLiteral(100)),[AssignStmt(Id(a),BinaryOp(+,Id(a),IntLiteral(1))),Break]),CallStmt(Id(foo),[]),If(BinaryOp(=,Id(a),IntLiteral(100)),[Break],[]),Return(None)])
#
# Test case 14_Undeclared Function:
# FuncDecl(Id(foo),[],IntType,[VarDecl(Id(a),IntType)],[AssignStmt(Id(a),IntLiteral(4)),If(BinaryOp(=,Id(a),IntLiteral(10)),[Return(Some(IntLiteral(1)))],[]),Return(Some(IntLiteral(10)))])
# FuncDecl(Id(main),[],VoidType(),[VarDecl(Id(a),IntType)],[AssignStmt(Id(a),IntLiteral(0)),If(BinaryOp(>,CallExpr(Id(foo),[]),IntLiteral(1)),[If(BinaryOp(<=,CallExpr(Id(foo2),[]),IntLiteral(100)),[Return(None)],[Return(None)])],[AssignStmt(Id(a),IntLiteral(2)),Return(None)])])

# 1,2, 4: Redeclared Variable
# 3: Redeclared Procedure
# 5: Undeclared Identifier
# 6: Undeclared Procedure
# 7,8,9,13: Type Mismatch In Statement
# 10,11,12: Type Mismatch In Expression
# 14: Break Not In Loop
# 15: Undeclared Function
