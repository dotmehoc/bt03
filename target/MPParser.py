# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3D")
        buf.write("\u0138\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\3\2\6\28\n\2\r\2\16\29\3\2\3")
        buf.write("\2\3\3\3\3\3\3\5\3A\n\3\3\4\3\4\3\4\3\4\6\4G\n\4\r\4\16")
        buf.write("\4H\3\5\3\5\3\5\7\5N\n\5\f\5\16\5Q\13\5\3\5\3\5\3\6\3")
        buf.write("\6\3\6\3\6\5\6Y\n\6\3\7\3\7\3\b\3\b\3\b\5\b`\n\b\3\b\3")
        buf.write("\b\3\b\5\be\n\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\7\tt\n\t\f\t\16\tw\13\t\3\t\3\t\5\t{\n")
        buf.write("\t\3\n\3\n\3\n\7\n\u0080\n\n\f\n\16\n\u0083\13\n\5\n\u0085")
        buf.write("\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u008e\n\13")
        buf.write("\f\13\16\13\u0091\13\13\3\13\3\13\5\13\u0095\n\13\3\f")
        buf.write("\3\f\7\f\u0099\n\f\f\f\16\f\u009c\13\f\3\f\3\f\3\r\3\r")
        buf.write("\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00aa\n\r\3\r\3\r")
        buf.write("\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3")
        buf.write("\r\3\r\7\r\u00bd\n\r\f\r\16\r\u00c0\13\r\3\16\3\16\5\16")
        buf.write("\u00c4\n\16\3\17\3\17\3\17\3\17\3\17\5\17\u00cb\n\17\3")
        buf.write("\17\3\17\3\17\5\17\u00d0\n\17\3\20\3\20\3\20\3\20\3\20")
        buf.write("\5\20\u00d7\n\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3")
        buf.write("\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\5\21\u00eb\n\21\3\22\3\22\3\22\6\22\u00f0\n\22\r\22\16")
        buf.write("\22\u00f1\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\5\23\u00fd\n\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3")
        buf.write("\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\31\3\31")
        buf.write("\3\32\3\32\3\32\3\32\3\32\7\32\u0121\n\32\f\32\16\32\u0124")
        buf.write("\13\32\5\32\u0126\n\32\3\32\3\32\3\33\3\33\3\33\3\33\3")
        buf.write("\33\7\33\u012f\n\33\f\33\16\33\u0132\13\33\5\33\u0134")
        buf.write("\n\33\3\33\3\33\3\33\2\3\30\34\2\4\6\b\n\f\16\20\22\24")
        buf.write("\26\30\32\34\36 \"$&(*,.\60\62\64\2\n\3\2\25\30\4\2\33")
        buf.write("\33\36\36\6\2\34\35\37\37!!**\4\2\32\33  \3\2$)\3\2\"")
        buf.write("#\3\2\6\7\3\28;\2\u014a\2\67\3\2\2\2\4@\3\2\2\2\6B\3\2")
        buf.write("\2\2\bJ\3\2\2\2\nX\3\2\2\2\fZ\3\2\2\2\16\\\3\2\2\2\20")
        buf.write("k\3\2\2\2\22\u0084\3\2\2\2\24\u0086\3\2\2\2\26\u0096\3")
        buf.write("\2\2\2\30\u00a9\3\2\2\2\32\u00c3\3\2\2\2\34\u00c5\3\2")
        buf.write("\2\2\36\u00d1\3\2\2\2 \u00ea\3\2\2\2\"\u00ef\3\2\2\2$")
        buf.write("\u00fc\3\2\2\2&\u00fe\3\2\2\2(\u0103\3\2\2\2*\u010c\3")
        buf.write("\2\2\2,\u0112\3\2\2\2.\u0115\3\2\2\2\60\u0119\3\2\2\2")
        buf.write("\62\u011b\3\2\2\2\64\u0129\3\2\2\2\668\5\4\3\2\67\66\3")
        buf.write("\2\2\289\3\2\2\29\67\3\2\2\29:\3\2\2\2:;\3\2\2\2;<\7\2")
        buf.write("\2\3<\3\3\2\2\2=A\5\6\4\2>A\5\20\t\2?A\5\24\13\2@=\3\2")
        buf.write("\2\2@>\3\2\2\2@?\3\2\2\2A\5\3\2\2\2BF\7\23\2\2CD\5\b\5")
        buf.write("\2DE\7\60\2\2EG\3\2\2\2FC\3\2\2\2GH\3\2\2\2HF\3\2\2\2")
        buf.write("HI\3\2\2\2I\7\3\2\2\2JO\7@\2\2KL\7\61\2\2LN\7@\2\2MK\3")
        buf.write("\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2")
        buf.write("\2RS\5\n\6\2S\t\3\2\2\2TU\7\62\2\2UY\5\f\7\2VW\7\62\2")
        buf.write("\2WY\5\16\b\2XT\3\2\2\2XV\3\2\2\2Y\13\3\2\2\2Z[\t\2\2")
        buf.write("\2[\r\3\2\2\2\\]\7\31\2\2]_\7,\2\2^`\7\33\2\2_^\3\2\2")
        buf.write("\2_`\3\2\2\2`a\3\2\2\2ab\78\2\2bd\7\63\2\2ce\7\33\2\2")
        buf.write("dc\3\2\2\2de\3\2\2\2ef\3\2\2\2fg\78\2\2gh\7-\2\2hi\7\24")
        buf.write("\2\2ij\5\f\7\2j\17\3\2\2\2kl\7\21\2\2lm\7@\2\2mn\7.\2")
        buf.write("\2no\5\22\n\2op\7/\2\2pq\5\n\6\2qu\7\60\2\2rt\5\6\4\2")
        buf.write("sr\3\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2vx\3\2\2\2wu\3")
        buf.write("\2\2\2xz\5\26\f\2y{\5.\30\2zy\3\2\2\2z{\3\2\2\2{\21\3")
        buf.write("\2\2\2|\u0081\5\b\5\2}~\7\60\2\2~\u0080\5\b\5\2\177}\3")
        buf.write("\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082")
        buf.write("\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0084")
        buf.write("|\3\2\2\2\u0084\u0085\3\2\2\2\u0085\23\3\2\2\2\u0086\u0087")
        buf.write("\7\22\2\2\u0087\u0088\7@\2\2\u0088\u0089\7.\2\2\u0089")
        buf.write("\u008a\5\22\n\2\u008a\u008b\7/\2\2\u008b\u008f\7\60\2")
        buf.write("\2\u008c\u008e\5\6\4\2\u008d\u008c\3\2\2\2\u008e\u0091")
        buf.write("\3\2\2\2\u008f\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090")
        buf.write("\u0092\3\2\2\2\u0091\u008f\3\2\2\2\u0092\u0094\5\26\f")
        buf.write("\2\u0093\u0095\5,\27\2\u0094\u0093\3\2\2\2\u0094\u0095")
        buf.write("\3\2\2\2\u0095\25\3\2\2\2\u0096\u009a\7\17\2\2\u0097\u0099")
        buf.write("\5 \21\2\u0098\u0097\3\2\2\2\u0099\u009c\3\2\2\2\u009a")
        buf.write("\u0098\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009d\3\2\2\2")
        buf.write("\u009c\u009a\3\2\2\2\u009d\u009e\7\20\2\2\u009e\27\3\2")
        buf.write("\2\2\u009f\u00a0\b\r\1\2\u00a0\u00a1\7.\2\2\u00a1\u00a2")
        buf.write("\5\30\r\2\u00a2\u00a3\7/\2\2\u00a3\u00aa\3\2\2\2\u00a4")
        buf.write("\u00a5\t\3\2\2\u00a5\u00aa\5\30\r\n\u00a6\u00aa\5\60\31")
        buf.write("\2\u00a7\u00aa\5\62\32\2\u00a8\u00aa\7@\2\2\u00a9\u009f")
        buf.write("\3\2\2\2\u00a9\u00a4\3\2\2\2\u00a9\u00a6\3\2\2\2\u00a9")
        buf.write("\u00a7\3\2\2\2\u00a9\u00a8\3\2\2\2\u00aa\u00be\3\2\2\2")
        buf.write("\u00ab\u00ac\f\t\2\2\u00ac\u00ad\t\4\2\2\u00ad\u00bd\5")
        buf.write("\30\r\n\u00ae\u00af\f\b\2\2\u00af\u00b0\t\5\2\2\u00b0")
        buf.write("\u00bd\5\30\r\t\u00b1\u00b2\f\7\2\2\u00b2\u00b3\t\6\2")
        buf.write("\2\u00b3\u00bd\5\30\r\b\u00b4\u00b5\f\6\2\2\u00b5\u00b6")
        buf.write("\t\7\2\2\u00b6\u00bd\5\30\r\7\u00b7\u00b8\f\13\2\2\u00b8")
        buf.write("\u00b9\7,\2\2\u00b9\u00ba\5\30\r\2\u00ba\u00bb\7-\2\2")
        buf.write("\u00bb\u00bd\3\2\2\2\u00bc\u00ab\3\2\2\2\u00bc\u00ae\3")
        buf.write("\2\2\2\u00bc\u00b1\3\2\2\2\u00bc\u00b4\3\2\2\2\u00bc\u00b7")
        buf.write("\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bc\3\2\2\2\u00be")
        buf.write("\u00bf\3\2\2\2\u00bf\31\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1")
        buf.write("\u00c4\5\34\17\2\u00c2\u00c4\5\36\20\2\u00c3\u00c1\3\2")
        buf.write("\2\2\u00c3\u00c2\3\2\2\2\u00c4\33\3\2\2\2\u00c5\u00c6")
        buf.write("\7\t\2\2\u00c6\u00c7\5\30\r\2\u00c7\u00ca\7\n\2\2\u00c8")
        buf.write("\u00cb\5\34\17\2\u00c9\u00cb\5 \21\2\u00ca\u00c8\3\2\2")
        buf.write("\2\u00ca\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cf")
        buf.write("\7\13\2\2\u00cd\u00d0\5\32\16\2\u00ce\u00d0\5 \21\2\u00cf")
        buf.write("\u00cd\3\2\2\2\u00cf\u00ce\3\2\2\2\u00d0\35\3\2\2\2\u00d1")
        buf.write("\u00d2\7\t\2\2\u00d2\u00d3\5\30\r\2\u00d3\u00d6\7\n\2")
        buf.write("\2\u00d4\u00d7\5\32\16\2\u00d5\u00d7\5 \21\2\u00d6\u00d4")
        buf.write("\3\2\2\2\u00d6\u00d5\3\2\2\2\u00d7\37\3\2\2\2\u00d8\u00eb")
        buf.write("\5\32\16\2\u00d9\u00eb\5\"\22\2\u00da\u00eb\5&\24\2\u00db")
        buf.write("\u00eb\5*\26\2\u00dc\u00dd\5\64\33\2\u00dd\u00de\7\60")
        buf.write("\2\2\u00de\u00eb\3\2\2\2\u00df\u00eb\5(\25\2\u00e0\u00e1")
        buf.write("\7\3\2\2\u00e1\u00eb\7\60\2\2\u00e2\u00e3\7\4\2\2\u00e3")
        buf.write("\u00eb\7\60\2\2\u00e4\u00eb\5,\27\2\u00e5\u00eb\5.\30")
        buf.write("\2\u00e6\u00e7\5\30\r\2\u00e7\u00e8\7\60\2\2\u00e8\u00eb")
        buf.write("\3\2\2\2\u00e9\u00eb\5\26\f\2\u00ea\u00d8\3\2\2\2\u00ea")
        buf.write("\u00d9\3\2\2\2\u00ea\u00da\3\2\2\2\u00ea\u00db\3\2\2\2")
        buf.write("\u00ea\u00dc\3\2\2\2\u00ea\u00df\3\2\2\2\u00ea\u00e0\3")
        buf.write("\2\2\2\u00ea\u00e2\3\2\2\2\u00ea\u00e4\3\2\2\2\u00ea\u00e5")
        buf.write("\3\2\2\2\u00ea\u00e6\3\2\2\2\u00ea\u00e9\3\2\2\2\u00eb")
        buf.write("!\3\2\2\2\u00ec\u00ed\5$\23\2\u00ed\u00ee\7+\2\2\u00ee")
        buf.write("\u00f0\3\2\2\2\u00ef\u00ec\3\2\2\2\u00f0\u00f1\3\2\2\2")
        buf.write("\u00f1\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f3\3")
        buf.write("\2\2\2\u00f3\u00f4\5\30\r\2\u00f4\u00f5\7\60\2\2\u00f5")
        buf.write("#\3\2\2\2\u00f6\u00fd\7@\2\2\u00f7\u00f8\5\30\r\2\u00f8")
        buf.write("\u00f9\7,\2\2\u00f9\u00fa\5\30\r\2\u00fa\u00fb\7-\2\2")
        buf.write("\u00fb\u00fd\3\2\2\2\u00fc\u00f6\3\2\2\2\u00fc\u00f7\3")
        buf.write("\2\2\2\u00fd%\3\2\2\2\u00fe\u00ff\7\r\2\2\u00ff\u0100")
        buf.write("\5\30\r\2\u0100\u0101\7\b\2\2\u0101\u0102\5 \21\2\u0102")
        buf.write("\'\3\2\2\2\u0103\u0104\7\5\2\2\u0104\u0105\7@\2\2\u0105")
        buf.write("\u0106\7+\2\2\u0106\u0107\5\30\r\2\u0107\u0108\t\b\2\2")
        buf.write("\u0108\u0109\5\30\r\2\u0109\u010a\7\b\2\2\u010a\u010b")
        buf.write("\5 \21\2\u010b)\3\2\2\2\u010c\u010d\7\16\2\2\u010d\u010e")
        buf.write("\5\22\n\2\u010e\u010f\7\60\2\2\u010f\u0110\7\b\2\2\u0110")
        buf.write("\u0111\5 \21\2\u0111+\3\2\2\2\u0112\u0113\7\f\2\2\u0113")
        buf.write("\u0114\7\60\2\2\u0114-\3\2\2\2\u0115\u0116\7\f\2\2\u0116")
        buf.write("\u0117\5\30\r\2\u0117\u0118\7\60\2\2\u0118/\3\2\2\2\u0119")
        buf.write("\u011a\t\t\2\2\u011a\61\3\2\2\2\u011b\u011c\7@\2\2\u011c")
        buf.write("\u0125\7.\2\2\u011d\u0122\5\30\r\2\u011e\u011f\7\61\2")
        buf.write("\2\u011f\u0121\5\30\r\2\u0120\u011e\3\2\2\2\u0121\u0124")
        buf.write("\3\2\2\2\u0122\u0120\3\2\2\2\u0122\u0123\3\2\2\2\u0123")
        buf.write("\u0126\3\2\2\2\u0124\u0122\3\2\2\2\u0125\u011d\3\2\2\2")
        buf.write("\u0125\u0126\3\2\2\2\u0126\u0127\3\2\2\2\u0127\u0128\7")
        buf.write("/\2\2\u0128\63\3\2\2\2\u0129\u012a\7@\2\2\u012a\u0133")
        buf.write("\7.\2\2\u012b\u0130\5\30\r\2\u012c\u012d\7\61\2\2\u012d")
        buf.write("\u012f\5\30\r\2\u012e\u012c\3\2\2\2\u012f\u0132\3\2\2")
        buf.write("\2\u0130\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u0134")
        buf.write("\3\2\2\2\u0132\u0130\3\2\2\2\u0133\u012b\3\2\2\2\u0133")
        buf.write("\u0134\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0136\7/\2\2")
        buf.write("\u0136\65\3\2\2\2\369@HOX_duz\u0081\u0084\u008f\u0094")
        buf.write("\u009a\u00a9\u00bc\u00be\u00c3\u00ca\u00cf\u00d6\u00ea")
        buf.write("\u00f1\u00fc\u0122\u0125\u0130\u0133")
        return buf.getvalue()


class MPParser ( Parser ):

    grammarFileName = "MP.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'+'", "'-'", "'*'", "'/'", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'='", "'<>'", "'<'", "'>'", "'<='", "'>='", "<INVALID>", 
                     "':='", "'['", "']'", "'('", "')'", "';'", "','", "':'", 
                     "' .. '", "'(*'", "'*)'", "'{'", "'}'" ]

    symbolicNames = [ "<INVALID>", "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", 
                      "DO", "IF", "THEN", "ELSE", "RETURN", "WHILE", "WITH", 
                      "BEGIN", "END", "FUNC", "PROC", "VAR", "OF", "BOOLEAN", 
                      "REAL", "INT", "STRING", "ARRAY", "ADD", "SUB", "MUL", 
                      "DIVIDE", "NOT", "MOD", "OR", "AND", "ORELSE", "ANDTH", 
                      "EQ", "NEQ", "LT", "GT", "LTEQ", "GTEQ", "INTDIV", 
                      "ASIGN", "LS", "RS", "LB", "RB", "SEMI", "COMMA", 
                      "COLON", "TWODOTS", "LCMT", "RCMT", "LP", "RP", "IntLit", 
                      "RealLit", "BoolLit", "StrLit", "BLOCKCMT1", "BLOCKCMT2", 
                      "LINECMT", "WS", "ID", "ILLEGAL_CHAR", "ILLEGAL_ESCAPE", 
                      "UNCLOSE_STRING", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_decls = 1
    RULE_varDecl = 2
    RULE_varList = 3
    RULE_retType = 4
    RULE_priType = 5
    RULE_compoType = 6
    RULE_funcDecl = 7
    RULE_paramList = 8
    RULE_procDecl = 9
    RULE_body = 10
    RULE_expr = 11
    RULE_ifStmt = 12
    RULE_ifElse = 13
    RULE_ifNoElse = 14
    RULE_stmt = 15
    RULE_asignStmt = 16
    RULE_lhs = 17
    RULE_whileDo = 18
    RULE_forloop = 19
    RULE_withDo = 20
    RULE_retNoExp = 21
    RULE_retWExp = 22
    RULE_dataTypeLit = 23
    RULE_funcCall = 24
    RULE_procCall = 25

    ruleNames =  [ "program", "decls", "varDecl", "varList", "retType", 
                   "priType", "compoType", "funcDecl", "paramList", "procDecl", 
                   "body", "expr", "ifStmt", "ifElse", "ifNoElse", "stmt", 
                   "asignStmt", "lhs", "whileDo", "forloop", "withDo", "retNoExp", 
                   "retWExp", "dataTypeLit", "funcCall", "procCall" ]

    EOF = Token.EOF
    BREAK=1
    CONTINUE=2
    FOR=3
    TO=4
    DOWNTO=5
    DO=6
    IF=7
    THEN=8
    ELSE=9
    RETURN=10
    WHILE=11
    WITH=12
    BEGIN=13
    END=14
    FUNC=15
    PROC=16
    VAR=17
    OF=18
    BOOLEAN=19
    REAL=20
    INT=21
    STRING=22
    ARRAY=23
    ADD=24
    SUB=25
    MUL=26
    DIVIDE=27
    NOT=28
    MOD=29
    OR=30
    AND=31
    ORELSE=32
    ANDTH=33
    EQ=34
    NEQ=35
    LT=36
    GT=37
    LTEQ=38
    GTEQ=39
    INTDIV=40
    ASIGN=41
    LS=42
    RS=43
    LB=44
    RB=45
    SEMI=46
    COMMA=47
    COLON=48
    TWODOTS=49
    LCMT=50
    RCMT=51
    LP=52
    RP=53
    IntLit=54
    RealLit=55
    BoolLit=56
    StrLit=57
    BLOCKCMT1=58
    BLOCKCMT2=59
    LINECMT=60
    WS=61
    ID=62
    ILLEGAL_CHAR=63
    ILLEGAL_ESCAPE=64
    UNCLOSE_STRING=65
    ERROR_CHAR=66

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MPParser.EOF, 0)

        def decls(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.DeclsContext)
            else:
                return self.getTypedRuleContext(MPParser.DeclsContext,i)


        def getRuleIndex(self):
            return MPParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MPParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 53 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 52
                self.decls()
                self.state = 55 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.FUNC) | (1 << MPParser.PROC) | (1 << MPParser.VAR))) != 0)):
                    break

            self.state = 57
            self.match(MPParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DeclsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varDecl(self):
            return self.getTypedRuleContext(MPParser.VarDeclContext,0)


        def funcDecl(self):
            return self.getTypedRuleContext(MPParser.FuncDeclContext,0)


        def procDecl(self):
            return self.getTypedRuleContext(MPParser.ProcDeclContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_decls

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecls" ):
                return visitor.visitDecls(self)
            else:
                return visitor.visitChildren(self)




    def decls(self):

        localctx = MPParser.DeclsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_decls)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MPParser.VAR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 59
                self.varDecl()
                pass
            elif token in [MPParser.FUNC]:
                self.enterOuterAlt(localctx, 2)
                self.state = 60
                self.funcDecl()
                pass
            elif token in [MPParser.PROC]:
                self.enterOuterAlt(localctx, 3)
                self.state = 61
                self.procDecl()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(MPParser.VAR, 0)

        def varList(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.VarListContext)
            else:
                return self.getTypedRuleContext(MPParser.VarListContext,i)


        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.SEMI)
            else:
                return self.getToken(MPParser.SEMI, i)

        def getRuleIndex(self):
            return MPParser.RULE_varDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarDecl" ):
                return visitor.visitVarDecl(self)
            else:
                return visitor.visitChildren(self)




    def varDecl(self):

        localctx = MPParser.VarDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_varDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.match(MPParser.VAR)
            self.state = 68 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 65
                self.varList()
                self.state = 66
                self.match(MPParser.SEMI)
                self.state = 70 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==MPParser.ID):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.ID)
            else:
                return self.getToken(MPParser.ID, i)

        def retType(self):
            return self.getTypedRuleContext(MPParser.RetTypeContext,0)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.COMMA)
            else:
                return self.getToken(MPParser.COMMA, i)

        def getRuleIndex(self):
            return MPParser.RULE_varList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarList" ):
                return visitor.visitVarList(self)
            else:
                return visitor.visitChildren(self)




    def varList(self):

        localctx = MPParser.VarListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_varList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self.match(MPParser.ID)
            self.state = 77
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MPParser.COMMA:
                self.state = 73
                self.match(MPParser.COMMA)
                self.state = 74
                self.match(MPParser.ID)
                self.state = 79
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 80
            self.retType()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RetTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COLON(self):
            return self.getToken(MPParser.COLON, 0)

        def priType(self):
            return self.getTypedRuleContext(MPParser.PriTypeContext,0)


        def compoType(self):
            return self.getTypedRuleContext(MPParser.CompoTypeContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_retType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRetType" ):
                return visitor.visitRetType(self)
            else:
                return visitor.visitChildren(self)




    def retType(self):

        localctx = MPParser.RetTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_retType)
        try:
            self.state = 86
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 82
                self.match(MPParser.COLON)
                self.state = 83
                self.priType()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 84
                self.match(MPParser.COLON)
                self.state = 85
                self.compoType()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PriTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(MPParser.INT, 0)

        def STRING(self):
            return self.getToken(MPParser.STRING, 0)

        def REAL(self):
            return self.getToken(MPParser.REAL, 0)

        def BOOLEAN(self):
            return self.getToken(MPParser.BOOLEAN, 0)

        def getRuleIndex(self):
            return MPParser.RULE_priType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPriType" ):
                return visitor.visitPriType(self)
            else:
                return visitor.visitChildren(self)




    def priType(self):

        localctx = MPParser.PriTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_priType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 88
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.BOOLEAN) | (1 << MPParser.REAL) | (1 << MPParser.INT) | (1 << MPParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CompoTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ARRAY(self):
            return self.getToken(MPParser.ARRAY, 0)

        def LS(self):
            return self.getToken(MPParser.LS, 0)

        def IntLit(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.IntLit)
            else:
                return self.getToken(MPParser.IntLit, i)

        def TWODOTS(self):
            return self.getToken(MPParser.TWODOTS, 0)

        def RS(self):
            return self.getToken(MPParser.RS, 0)

        def OF(self):
            return self.getToken(MPParser.OF, 0)

        def priType(self):
            return self.getTypedRuleContext(MPParser.PriTypeContext,0)


        def SUB(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.SUB)
            else:
                return self.getToken(MPParser.SUB, i)

        def getRuleIndex(self):
            return MPParser.RULE_compoType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCompoType" ):
                return visitor.visitCompoType(self)
            else:
                return visitor.visitChildren(self)




    def compoType(self):

        localctx = MPParser.CompoTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_compoType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.match(MPParser.ARRAY)
            self.state = 91
            self.match(MPParser.LS)
            self.state = 93
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.SUB:
                self.state = 92
                self.match(MPParser.SUB)


            self.state = 95
            self.match(MPParser.IntLit)
            self.state = 96
            self.match(MPParser.TWODOTS)
            self.state = 98
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.SUB:
                self.state = 97
                self.match(MPParser.SUB)


            self.state = 100
            self.match(MPParser.IntLit)
            self.state = 101
            self.match(MPParser.RS)
            self.state = 102
            self.match(MPParser.OF)
            self.state = 103
            self.priType()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUNC(self):
            return self.getToken(MPParser.FUNC, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def paramList(self):
            return self.getTypedRuleContext(MPParser.ParamListContext,0)


        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def retType(self):
            return self.getTypedRuleContext(MPParser.RetTypeContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def body(self):
            return self.getTypedRuleContext(MPParser.BodyContext,0)


        def varDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.VarDeclContext)
            else:
                return self.getTypedRuleContext(MPParser.VarDeclContext,i)


        def retWExp(self):
            return self.getTypedRuleContext(MPParser.RetWExpContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_funcDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncDecl" ):
                return visitor.visitFuncDecl(self)
            else:
                return visitor.visitChildren(self)




    def funcDecl(self):

        localctx = MPParser.FuncDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_funcDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.match(MPParser.FUNC)
            self.state = 106
            self.match(MPParser.ID)
            self.state = 107
            self.match(MPParser.LB)
            self.state = 108
            self.paramList()
            self.state = 109
            self.match(MPParser.RB)
            self.state = 110
            self.retType()
            self.state = 111
            self.match(MPParser.SEMI)
            self.state = 115
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MPParser.VAR:
                self.state = 112
                self.varDecl()
                self.state = 117
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 118
            self.body()
            self.state = 120
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.RETURN:
                self.state = 119
                self.retWExp()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ParamListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varList(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.VarListContext)
            else:
                return self.getTypedRuleContext(MPParser.VarListContext,i)


        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.SEMI)
            else:
                return self.getToken(MPParser.SEMI, i)

        def getRuleIndex(self):
            return MPParser.RULE_paramList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParamList" ):
                return visitor.visitParamList(self)
            else:
                return visitor.visitChildren(self)




    def paramList(self):

        localctx = MPParser.ParamListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_paramList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.ID:
                self.state = 122
                self.varList()
                self.state = 127
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 123
                        self.match(MPParser.SEMI)
                        self.state = 124
                        self.varList() 
                    self.state = 129
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,9,self._ctx)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ProcDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PROC(self):
            return self.getToken(MPParser.PROC, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def paramList(self):
            return self.getTypedRuleContext(MPParser.ParamListContext,0)


        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def body(self):
            return self.getTypedRuleContext(MPParser.BodyContext,0)


        def varDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.VarDeclContext)
            else:
                return self.getTypedRuleContext(MPParser.VarDeclContext,i)


        def retNoExp(self):
            return self.getTypedRuleContext(MPParser.RetNoExpContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_procDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProcDecl" ):
                return visitor.visitProcDecl(self)
            else:
                return visitor.visitChildren(self)




    def procDecl(self):

        localctx = MPParser.ProcDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_procDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(MPParser.PROC)
            self.state = 133
            self.match(MPParser.ID)
            self.state = 134
            self.match(MPParser.LB)
            self.state = 135
            self.paramList()
            self.state = 136
            self.match(MPParser.RB)
            self.state = 137
            self.match(MPParser.SEMI)
            self.state = 141
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MPParser.VAR:
                self.state = 138
                self.varDecl()
                self.state = 143
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 144
            self.body()
            self.state = 146
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.RETURN:
                self.state = 145
                self.retNoExp()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BEGIN(self):
            return self.getToken(MPParser.BEGIN, 0)

        def END(self):
            return self.getToken(MPParser.END, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.StmtContext)
            else:
                return self.getTypedRuleContext(MPParser.StmtContext,i)


        def getRuleIndex(self):
            return MPParser.RULE_body

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBody" ):
                return visitor.visitBody(self)
            else:
                return visitor.visitChildren(self)




    def body(self):

        localctx = MPParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self.match(MPParser.BEGIN)
            self.state = 152
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.BREAK) | (1 << MPParser.CONTINUE) | (1 << MPParser.FOR) | (1 << MPParser.IF) | (1 << MPParser.RETURN) | (1 << MPParser.WHILE) | (1 << MPParser.WITH) | (1 << MPParser.BEGIN) | (1 << MPParser.SUB) | (1 << MPParser.NOT) | (1 << MPParser.LB) | (1 << MPParser.IntLit) | (1 << MPParser.RealLit) | (1 << MPParser.BoolLit) | (1 << MPParser.StrLit) | (1 << MPParser.ID))) != 0):
                self.state = 149
                self.stmt()
                self.state = 154
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 155
            self.match(MPParser.END)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.ExprContext)
            else:
                return self.getTypedRuleContext(MPParser.ExprContext,i)


        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def NOT(self):
            return self.getToken(MPParser.NOT, 0)

        def SUB(self):
            return self.getToken(MPParser.SUB, 0)

        def dataTypeLit(self):
            return self.getTypedRuleContext(MPParser.DataTypeLitContext,0)


        def funcCall(self):
            return self.getTypedRuleContext(MPParser.FuncCallContext,0)


        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def MUL(self):
            return self.getToken(MPParser.MUL, 0)

        def DIVIDE(self):
            return self.getToken(MPParser.DIVIDE, 0)

        def INTDIV(self):
            return self.getToken(MPParser.INTDIV, 0)

        def MOD(self):
            return self.getToken(MPParser.MOD, 0)

        def AND(self):
            return self.getToken(MPParser.AND, 0)

        def ADD(self):
            return self.getToken(MPParser.ADD, 0)

        def OR(self):
            return self.getToken(MPParser.OR, 0)

        def EQ(self):
            return self.getToken(MPParser.EQ, 0)

        def NEQ(self):
            return self.getToken(MPParser.NEQ, 0)

        def LT(self):
            return self.getToken(MPParser.LT, 0)

        def LTEQ(self):
            return self.getToken(MPParser.LTEQ, 0)

        def GT(self):
            return self.getToken(MPParser.GT, 0)

        def GTEQ(self):
            return self.getToken(MPParser.GTEQ, 0)

        def ANDTH(self):
            return self.getToken(MPParser.ANDTH, 0)

        def ORELSE(self):
            return self.getToken(MPParser.ORELSE, 0)

        def LS(self):
            return self.getToken(MPParser.LS, 0)

        def RS(self):
            return self.getToken(MPParser.RS, 0)

        def getRuleIndex(self):
            return MPParser.RULE_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MPParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 22
        self.enterRecursionRule(localctx, 22, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 167
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                self.state = 158
                self.match(MPParser.LB)
                self.state = 159
                self.expr(0)
                self.state = 160
                self.match(MPParser.RB)
                pass

            elif la_ == 2:
                self.state = 162
                _la = self._input.LA(1)
                if not(_la==MPParser.SUB or _la==MPParser.NOT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 163
                self.expr(8)
                pass

            elif la_ == 3:
                self.state = 164
                self.dataTypeLit()
                pass

            elif la_ == 4:
                self.state = 165
                self.funcCall()
                pass

            elif la_ == 5:
                self.state = 166
                self.match(MPParser.ID)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 188
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 186
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                    if la_ == 1:
                        localctx = MPParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 169
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 170
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.MUL) | (1 << MPParser.DIVIDE) | (1 << MPParser.MOD) | (1 << MPParser.AND) | (1 << MPParser.INTDIV))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 171
                        self.expr(8)
                        pass

                    elif la_ == 2:
                        localctx = MPParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 172
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 173
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.ADD) | (1 << MPParser.SUB) | (1 << MPParser.OR))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 174
                        self.expr(7)
                        pass

                    elif la_ == 3:
                        localctx = MPParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 175
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 176
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.EQ) | (1 << MPParser.NEQ) | (1 << MPParser.LT) | (1 << MPParser.GT) | (1 << MPParser.LTEQ) | (1 << MPParser.GTEQ))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 177
                        self.expr(6)
                        pass

                    elif la_ == 4:
                        localctx = MPParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 178
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 179
                        _la = self._input.LA(1)
                        if not(_la==MPParser.ORELSE or _la==MPParser.ANDTH):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 180
                        self.expr(5)
                        pass

                    elif la_ == 5:
                        localctx = MPParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 181
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 182
                        self.match(MPParser.LS)
                        self.state = 183
                        self.expr(0)
                        self.state = 184
                        self.match(MPParser.RS)
                        pass

             
                self.state = 190
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class IfStmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifElse(self):
            return self.getTypedRuleContext(MPParser.IfElseContext,0)


        def ifNoElse(self):
            return self.getTypedRuleContext(MPParser.IfNoElseContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_ifStmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfStmt" ):
                return visitor.visitIfStmt(self)
            else:
                return visitor.visitChildren(self)




    def ifStmt(self):

        localctx = MPParser.IfStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_ifStmt)
        try:
            self.state = 193
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 191
                self.ifElse()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 192
                self.ifNoElse()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfElseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MPParser.IF, 0)

        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def THEN(self):
            return self.getToken(MPParser.THEN, 0)

        def ELSE(self):
            return self.getToken(MPParser.ELSE, 0)

        def ifElse(self):
            return self.getTypedRuleContext(MPParser.IfElseContext,0)


        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.StmtContext)
            else:
                return self.getTypedRuleContext(MPParser.StmtContext,i)


        def ifStmt(self):
            return self.getTypedRuleContext(MPParser.IfStmtContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_ifElse

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfElse" ):
                return visitor.visitIfElse(self)
            else:
                return visitor.visitChildren(self)




    def ifElse(self):

        localctx = MPParser.IfElseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifElse)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 195
            self.match(MPParser.IF)
            self.state = 196
            self.expr(0)
            self.state = 197
            self.match(MPParser.THEN)
            self.state = 200
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 198
                self.ifElse()
                pass

            elif la_ == 2:
                self.state = 199
                self.stmt()
                pass


            self.state = 202
            self.match(MPParser.ELSE)
            self.state = 205
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.state = 203
                self.ifStmt()
                pass

            elif la_ == 2:
                self.state = 204
                self.stmt()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfNoElseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MPParser.IF, 0)

        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def THEN(self):
            return self.getToken(MPParser.THEN, 0)

        def ifStmt(self):
            return self.getTypedRuleContext(MPParser.IfStmtContext,0)


        def stmt(self):
            return self.getTypedRuleContext(MPParser.StmtContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_ifNoElse

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfNoElse" ):
                return visitor.visitIfNoElse(self)
            else:
                return visitor.visitChildren(self)




    def ifNoElse(self):

        localctx = MPParser.IfNoElseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_ifNoElse)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.match(MPParser.IF)
            self.state = 208
            self.expr(0)
            self.state = 209
            self.match(MPParser.THEN)
            self.state = 212
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.state = 210
                self.ifStmt()
                pass

            elif la_ == 2:
                self.state = 211
                self.stmt()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifStmt(self):
            return self.getTypedRuleContext(MPParser.IfStmtContext,0)


        def asignStmt(self):
            return self.getTypedRuleContext(MPParser.AsignStmtContext,0)


        def whileDo(self):
            return self.getTypedRuleContext(MPParser.WhileDoContext,0)


        def withDo(self):
            return self.getTypedRuleContext(MPParser.WithDoContext,0)


        def procCall(self):
            return self.getTypedRuleContext(MPParser.ProcCallContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def forloop(self):
            return self.getTypedRuleContext(MPParser.ForloopContext,0)


        def BREAK(self):
            return self.getToken(MPParser.BREAK, 0)

        def CONTINUE(self):
            return self.getToken(MPParser.CONTINUE, 0)

        def retNoExp(self):
            return self.getTypedRuleContext(MPParser.RetNoExpContext,0)


        def retWExp(self):
            return self.getTypedRuleContext(MPParser.RetWExpContext,0)


        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def body(self):
            return self.getTypedRuleContext(MPParser.BodyContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = MPParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_stmt)
        try:
            self.state = 232
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 214
                self.ifStmt()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 215
                self.asignStmt()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 216
                self.whileDo()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 217
                self.withDo()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 218
                self.procCall()
                self.state = 219
                self.match(MPParser.SEMI)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 221
                self.forloop()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 222
                self.match(MPParser.BREAK)
                self.state = 223
                self.match(MPParser.SEMI)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 224
                self.match(MPParser.CONTINUE)
                self.state = 225
                self.match(MPParser.SEMI)
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 226
                self.retNoExp()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 227
                self.retWExp()
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 228
                self.expr(0)
                self.state = 229
                self.match(MPParser.SEMI)
                pass

            elif la_ == 12:
                self.enterOuterAlt(localctx, 12)
                self.state = 231
                self.body()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AsignStmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def lhs(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.LhsContext)
            else:
                return self.getTypedRuleContext(MPParser.LhsContext,i)


        def ASIGN(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.ASIGN)
            else:
                return self.getToken(MPParser.ASIGN, i)

        def getRuleIndex(self):
            return MPParser.RULE_asignStmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAsignStmt" ):
                return visitor.visitAsignStmt(self)
            else:
                return visitor.visitChildren(self)




    def asignStmt(self):

        localctx = MPParser.AsignStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_asignStmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 237 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 234
                    self.lhs()
                    self.state = 235
                    self.match(MPParser.ASIGN)

                else:
                    raise NoViableAltException(self)
                self.state = 239 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

            self.state = 241
            self.expr(0)
            self.state = 242
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LhsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.ExprContext)
            else:
                return self.getTypedRuleContext(MPParser.ExprContext,i)


        def LS(self):
            return self.getToken(MPParser.LS, 0)

        def RS(self):
            return self.getToken(MPParser.RS, 0)

        def getRuleIndex(self):
            return MPParser.RULE_lhs

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLhs" ):
                return visitor.visitLhs(self)
            else:
                return visitor.visitChildren(self)




    def lhs(self):

        localctx = MPParser.LhsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_lhs)
        try:
            self.state = 250
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 244
                self.match(MPParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 245
                self.expr(0)
                self.state = 246
                self.match(MPParser.LS)
                self.state = 247
                self.expr(0)
                self.state = 248
                self.match(MPParser.RS)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WhileDoContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHILE(self):
            return self.getToken(MPParser.WHILE, 0)

        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def DO(self):
            return self.getToken(MPParser.DO, 0)

        def stmt(self):
            return self.getTypedRuleContext(MPParser.StmtContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_whileDo

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhileDo" ):
                return visitor.visitWhileDo(self)
            else:
                return visitor.visitChildren(self)




    def whileDo(self):

        localctx = MPParser.WhileDoContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_whileDo)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 252
            self.match(MPParser.WHILE)
            self.state = 253
            self.expr(0)
            self.state = 254
            self.match(MPParser.DO)
            self.state = 255
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ForloopContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(MPParser.FOR, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def ASIGN(self):
            return self.getToken(MPParser.ASIGN, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.ExprContext)
            else:
                return self.getTypedRuleContext(MPParser.ExprContext,i)


        def DO(self):
            return self.getToken(MPParser.DO, 0)

        def stmt(self):
            return self.getTypedRuleContext(MPParser.StmtContext,0)


        def TO(self):
            return self.getToken(MPParser.TO, 0)

        def DOWNTO(self):
            return self.getToken(MPParser.DOWNTO, 0)

        def getRuleIndex(self):
            return MPParser.RULE_forloop

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitForloop" ):
                return visitor.visitForloop(self)
            else:
                return visitor.visitChildren(self)




    def forloop(self):

        localctx = MPParser.ForloopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_forloop)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 257
            self.match(MPParser.FOR)
            self.state = 258
            self.match(MPParser.ID)
            self.state = 259
            self.match(MPParser.ASIGN)
            self.state = 260
            self.expr(0)
            self.state = 261
            _la = self._input.LA(1)
            if not(_la==MPParser.TO or _la==MPParser.DOWNTO):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 262
            self.expr(0)
            self.state = 263
            self.match(MPParser.DO)
            self.state = 264
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WithDoContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WITH(self):
            return self.getToken(MPParser.WITH, 0)

        def paramList(self):
            return self.getTypedRuleContext(MPParser.ParamListContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def DO(self):
            return self.getToken(MPParser.DO, 0)

        def stmt(self):
            return self.getTypedRuleContext(MPParser.StmtContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_withDo

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWithDo" ):
                return visitor.visitWithDo(self)
            else:
                return visitor.visitChildren(self)




    def withDo(self):

        localctx = MPParser.WithDoContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_withDo)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 266
            self.match(MPParser.WITH)
            self.state = 267
            self.paramList()
            self.state = 268
            self.match(MPParser.SEMI)
            self.state = 269
            self.match(MPParser.DO)
            self.state = 270
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RetNoExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MPParser.RETURN, 0)

        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def getRuleIndex(self):
            return MPParser.RULE_retNoExp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRetNoExp" ):
                return visitor.visitRetNoExp(self)
            else:
                return visitor.visitChildren(self)




    def retNoExp(self):

        localctx = MPParser.RetNoExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_retNoExp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 272
            self.match(MPParser.RETURN)
            self.state = 273
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RetWExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MPParser.RETURN, 0)

        def expr(self):
            return self.getTypedRuleContext(MPParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def getRuleIndex(self):
            return MPParser.RULE_retWExp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRetWExp" ):
                return visitor.visitRetWExp(self)
            else:
                return visitor.visitChildren(self)




    def retWExp(self):

        localctx = MPParser.RetWExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_retWExp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 275
            self.match(MPParser.RETURN)
            self.state = 276
            self.expr(0)
            self.state = 277
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DataTypeLitContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IntLit(self):
            return self.getToken(MPParser.IntLit, 0)

        def RealLit(self):
            return self.getToken(MPParser.RealLit, 0)

        def BoolLit(self):
            return self.getToken(MPParser.BoolLit, 0)

        def StrLit(self):
            return self.getToken(MPParser.StrLit, 0)

        def getRuleIndex(self):
            return MPParser.RULE_dataTypeLit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDataTypeLit" ):
                return visitor.visitDataTypeLit(self)
            else:
                return visitor.visitChildren(self)




    def dataTypeLit(self):

        localctx = MPParser.DataTypeLitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_dataTypeLit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 279
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.IntLit) | (1 << MPParser.RealLit) | (1 << MPParser.BoolLit) | (1 << MPParser.StrLit))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.ExprContext)
            else:
                return self.getTypedRuleContext(MPParser.ExprContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.COMMA)
            else:
                return self.getToken(MPParser.COMMA, i)

        def getRuleIndex(self):
            return MPParser.RULE_funcCall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncCall" ):
                return visitor.visitFuncCall(self)
            else:
                return visitor.visitChildren(self)




    def funcCall(self):

        localctx = MPParser.FuncCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_funcCall)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 281
            self.match(MPParser.ID)
            self.state = 282
            self.match(MPParser.LB)
            self.state = 291
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.SUB) | (1 << MPParser.NOT) | (1 << MPParser.LB) | (1 << MPParser.IntLit) | (1 << MPParser.RealLit) | (1 << MPParser.BoolLit) | (1 << MPParser.StrLit) | (1 << MPParser.ID))) != 0):
                self.state = 283
                self.expr(0)
                self.state = 288
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MPParser.COMMA:
                    self.state = 284
                    self.match(MPParser.COMMA)
                    self.state = 285
                    self.expr(0)
                    self.state = 290
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 293
            self.match(MPParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ProcCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.ExprContext)
            else:
                return self.getTypedRuleContext(MPParser.ExprContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MPParser.COMMA)
            else:
                return self.getToken(MPParser.COMMA, i)

        def getRuleIndex(self):
            return MPParser.RULE_procCall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProcCall" ):
                return visitor.visitProcCall(self)
            else:
                return visitor.visitChildren(self)




    def procCall(self):

        localctx = MPParser.ProcCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_procCall)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 295
            self.match(MPParser.ID)
            self.state = 296
            self.match(MPParser.LB)
            self.state = 305
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MPParser.SUB) | (1 << MPParser.NOT) | (1 << MPParser.LB) | (1 << MPParser.IntLit) | (1 << MPParser.RealLit) | (1 << MPParser.BoolLit) | (1 << MPParser.StrLit) | (1 << MPParser.ID))) != 0):
                self.state = 297
                self.expr(0)
                self.state = 302
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MPParser.COMMA:
                    self.state = 298
                    self.match(MPParser.COMMA)
                    self.state = 299
                    self.expr(0)
                    self.state = 304
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 307
            self.match(MPParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[11] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 9)
         




