# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

from lexerror import *

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2D")
        buf.write("\u029d\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4")
        buf.write("^\t^\4_\t_\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7")
        buf.write("\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r")
        buf.write("\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23")
        buf.write("\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30")
        buf.write("\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\3 \3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3\"\3")
        buf.write("\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3%\3%\3%\3%\3%\3&\3&\3&\3")
        buf.write("&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3)")
        buf.write("\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3+\3+\3+\3+\3,\3,\3,\3")
        buf.write(",\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3.\3.\3")
        buf.write(".\3.\3/\3/\3/\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60")
        buf.write("\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3\62\3\62\3\62")
        buf.write("\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64")
        buf.write("\3\64\3\64\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38")
        buf.write("\38\39\39\39\39\3:\3:\3:\3:\3;\3;\3;\3<\3<\3<\3<\3=\3")
        buf.write("=\3=\6=\u0197\n=\r=\16=\u0198\3=\3=\3=\3=\3=\3>\3>\3>")
        buf.write("\3>\6>\u01a4\n>\r>\16>\u01a5\3>\3>\3>\3>\3>\3?\3?\3@\3")
        buf.write("@\3@\3A\3A\3B\3B\3C\3C\3C\3D\3D\3D\3E\3E\3E\3E\3F\3F\3")
        buf.write("F\3G\3G\3H\3H\3I\3I\3J\3J\3K\3K\3L\3L\3M\3M\3N\3N\3N\3")
        buf.write("N\3N\3O\3O\3O\3P\3P\3P\3Q\3Q\3R\3R\3S\6S\u01e1\nS\rS\16")
        buf.write("S\u01e2\3T\6T\u01e6\nT\rT\16T\u01e7\3T\3T\7T\u01ec\nT")
        buf.write("\fT\16T\u01ef\13T\3T\7T\u01f2\nT\fT\16T\u01f5\13T\3T\3")
        buf.write("T\6T\u01f9\nT\rT\16T\u01fa\3T\6T\u01fe\nT\rT\16T\u01ff")
        buf.write("\3T\3T\5T\u0204\nT\3T\6T\u0207\nT\rT\16T\u0208\3T\7T\u020c")
        buf.write("\nT\fT\16T\u020f\13T\3T\3T\6T\u0213\nT\rT\16T\u0214\3")
        buf.write("T\3T\5T\u0219\nT\3T\6T\u021c\nT\rT\16T\u021d\5T\u0220")
        buf.write("\nT\3U\3U\3U\3U\3U\3U\3U\3U\3U\5U\u022b\nU\3V\3V\3V\3")
        buf.write("V\7V\u0231\nV\fV\16V\u0234\13V\3V\3V\3V\3W\3W\3W\3W\7")
        buf.write("W\u023d\nW\fW\16W\u0240\13W\3W\3W\3W\3W\3W\3X\3X\7X\u0249")
        buf.write("\nX\fX\16X\u024c\13X\3X\3X\3X\3X\3Y\3Y\3Y\3Y\7Y\u0256")
        buf.write("\nY\fY\16Y\u0259\13Y\3Y\3Y\3Z\6Z\u025e\nZ\rZ\16Z\u025f")
        buf.write("\3Z\3Z\3[\3[\5[\u0266\n[\3[\3[\3[\7[\u026b\n[\f[\16[\u026e")
        buf.write("\13[\3\\\3\\\7\\\u0272\n\\\f\\\16\\\u0275\13\\\3\\\3\\")
        buf.write("\7\\\u0279\n\\\f\\\16\\\u027c\13\\\3\\\3\\\3\\\3]\3]\7")
        buf.write("]\u0283\n]\f]\16]\u0286\13]\3]\3]\7]\u028a\n]\f]\16]\u028d")
        buf.write("\13]\3]\3]\3]\3^\3^\7^\u0294\n^\f^\16^\u0297\13^\3^\3")
        buf.write("^\3_\3_\3_\4\u023e\u024a\2`\3\2\5\2\7\2\t\2\13\2\r\2\17")
        buf.write("\2\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'")
        buf.write("\2)\2+\2-\2/\2\61\2\63\2\65\2\67\29\2;\3=\4?\5A\6C\7E")
        buf.write("\bG\tI\nK\13M\fO\rQ\16S\17U\20W\21Y\22[\23]\24_\25a\26")
        buf.write("c\27e\30g\31i\32k\33m\34o\35q\36s\37u w!y\"{#}$\177%\u0081")
        buf.write("&\u0083\'\u0085(\u0087)\u0089*\u008b+\u008d,\u008f-\u0091")
        buf.write(".\u0093/\u0095\60\u0097\61\u0099\62\u009b\63\u009d\64")
        buf.write("\u009f\65\u00a1\66\u00a3\67\u00a58\u00a79\u00a9:\u00ab")
        buf.write(";\u00ad<\u00af=\u00b1>\u00b3?\u00b5@\u00b7A\u00b9B\u00bb")
        buf.write("C\u00bdD\3\2\'\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGg")
        buf.write("g\4\2HHhh\4\2IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2")
        buf.write("NNnn\4\2OOoo\4\2PPpp\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4")
        buf.write("\2UUuu\4\2VVvv\4\2WWww\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{")
        buf.write("{\4\2\\\\||\3\2\62;\4\2C\\c|\3\2\"\"\7\2\n\f\16\17$$)")
        buf.write(")^^\n\2$$))^^ddhhppttvv\4\2\f\f\17\17\5\2\n\f\16\17\"")
        buf.write("\"\5\2\f\f\17\17$$\4\2\13\13))\t\2$$^^ddhhppttvv\3\2$")
        buf.write("$\2\u02a1\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2")
        buf.write("\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2")
        buf.write("\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2")
        buf.write("\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3")
        buf.write("\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i")
        buf.write("\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2")
        buf.write("s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2")
        buf.write("\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2")
        buf.write("\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2")
        buf.write("\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091")
        buf.write("\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2")
        buf.write("\2\2\u0099\3\2\2\2\2\u009b\3\2\2\2\2\u009d\3\2\2\2\2\u009f")
        buf.write("\3\2\2\2\2\u00a1\3\2\2\2\2\u00a3\3\2\2\2\2\u00a5\3\2\2")
        buf.write("\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2\2\2\u00ab\3\2\2\2\2\u00ad")
        buf.write("\3\2\2\2\2\u00af\3\2\2\2\2\u00b1\3\2\2\2\2\u00b3\3\2\2")
        buf.write("\2\2\u00b5\3\2\2\2\2\u00b7\3\2\2\2\2\u00b9\3\2\2\2\2\u00bb")
        buf.write("\3\2\2\2\2\u00bd\3\2\2\2\3\u00bf\3\2\2\2\5\u00c1\3\2\2")
        buf.write("\2\7\u00c3\3\2\2\2\t\u00c5\3\2\2\2\13\u00c7\3\2\2\2\r")
        buf.write("\u00c9\3\2\2\2\17\u00cb\3\2\2\2\21\u00cd\3\2\2\2\23\u00cf")
        buf.write("\3\2\2\2\25\u00d1\3\2\2\2\27\u00d3\3\2\2\2\31\u00d5\3")
        buf.write("\2\2\2\33\u00d7\3\2\2\2\35\u00d9\3\2\2\2\37\u00db\3\2")
        buf.write("\2\2!\u00dd\3\2\2\2#\u00df\3\2\2\2%\u00e1\3\2\2\2\'\u00e3")
        buf.write("\3\2\2\2)\u00e5\3\2\2\2+\u00e7\3\2\2\2-\u00e9\3\2\2\2")
        buf.write("/\u00eb\3\2\2\2\61\u00ed\3\2\2\2\63\u00ef\3\2\2\2\65\u00f1")
        buf.write("\3\2\2\2\67\u00f3\3\2\2\29\u00f5\3\2\2\2;\u00f7\3\2\2")
        buf.write("\2=\u00fd\3\2\2\2?\u0106\3\2\2\2A\u010a\3\2\2\2C\u010d")
        buf.write("\3\2\2\2E\u0114\3\2\2\2G\u0117\3\2\2\2I\u011a\3\2\2\2")
        buf.write("K\u011f\3\2\2\2M\u0124\3\2\2\2O\u012b\3\2\2\2Q\u0131\3")
        buf.write("\2\2\2S\u0136\3\2\2\2U\u013c\3\2\2\2W\u0140\3\2\2\2Y\u0149")
        buf.write("\3\2\2\2[\u0153\3\2\2\2]\u0157\3\2\2\2_\u015a\3\2\2\2")
        buf.write("a\u0162\3\2\2\2c\u0167\3\2\2\2e\u016f\3\2\2\2g\u0176\3")
        buf.write("\2\2\2i\u017c\3\2\2\2k\u017e\3\2\2\2m\u0180\3\2\2\2o\u0182")
        buf.write("\3\2\2\2q\u0184\3\2\2\2s\u0188\3\2\2\2u\u018c\3\2\2\2")
        buf.write("w\u018f\3\2\2\2y\u0193\3\2\2\2{\u019f\3\2\2\2}\u01ac\3")
        buf.write("\2\2\2\177\u01ae\3\2\2\2\u0081\u01b1\3\2\2\2\u0083\u01b3")
        buf.write("\3\2\2\2\u0085\u01b5\3\2\2\2\u0087\u01b8\3\2\2\2\u0089")
        buf.write("\u01bb\3\2\2\2\u008b\u01bf\3\2\2\2\u008d\u01c2\3\2\2\2")
        buf.write("\u008f\u01c4\3\2\2\2\u0091\u01c6\3\2\2\2\u0093\u01c8\3")
        buf.write("\2\2\2\u0095\u01ca\3\2\2\2\u0097\u01cc\3\2\2\2\u0099\u01ce")
        buf.write("\3\2\2\2\u009b\u01d0\3\2\2\2\u009d\u01d5\3\2\2\2\u009f")
        buf.write("\u01d8\3\2\2\2\u00a1\u01db\3\2\2\2\u00a3\u01dd\3\2\2\2")
        buf.write("\u00a5\u01e0\3\2\2\2\u00a7\u021f\3\2\2\2\u00a9\u022a\3")
        buf.write("\2\2\2\u00ab\u022c\3\2\2\2\u00ad\u0238\3\2\2\2\u00af\u0246")
        buf.write("\3\2\2\2\u00b1\u0251\3\2\2\2\u00b3\u025d\3\2\2\2\u00b5")
        buf.write("\u0265\3\2\2\2\u00b7\u026f\3\2\2\2\u00b9\u0280\3\2\2\2")
        buf.write("\u00bb\u0291\3\2\2\2\u00bd\u029a\3\2\2\2\u00bf\u00c0\t")
        buf.write("\2\2\2\u00c0\4\3\2\2\2\u00c1\u00c2\t\3\2\2\u00c2\6\3\2")
        buf.write("\2\2\u00c3\u00c4\t\4\2\2\u00c4\b\3\2\2\2\u00c5\u00c6\t")
        buf.write("\5\2\2\u00c6\n\3\2\2\2\u00c7\u00c8\t\6\2\2\u00c8\f\3\2")
        buf.write("\2\2\u00c9\u00ca\t\7\2\2\u00ca\16\3\2\2\2\u00cb\u00cc")
        buf.write("\t\b\2\2\u00cc\20\3\2\2\2\u00cd\u00ce\t\t\2\2\u00ce\22")
        buf.write("\3\2\2\2\u00cf\u00d0\t\n\2\2\u00d0\24\3\2\2\2\u00d1\u00d2")
        buf.write("\t\13\2\2\u00d2\26\3\2\2\2\u00d3\u00d4\t\f\2\2\u00d4\30")
        buf.write("\3\2\2\2\u00d5\u00d6\t\r\2\2\u00d6\32\3\2\2\2\u00d7\u00d8")
        buf.write("\t\16\2\2\u00d8\34\3\2\2\2\u00d9\u00da\t\17\2\2\u00da")
        buf.write("\36\3\2\2\2\u00db\u00dc\t\20\2\2\u00dc \3\2\2\2\u00dd")
        buf.write("\u00de\t\21\2\2\u00de\"\3\2\2\2\u00df\u00e0\t\22\2\2\u00e0")
        buf.write("$\3\2\2\2\u00e1\u00e2\t\23\2\2\u00e2&\3\2\2\2\u00e3\u00e4")
        buf.write("\t\24\2\2\u00e4(\3\2\2\2\u00e5\u00e6\t\25\2\2\u00e6*\3")
        buf.write("\2\2\2\u00e7\u00e8\t\26\2\2\u00e8,\3\2\2\2\u00e9\u00ea")
        buf.write("\t\27\2\2\u00ea.\3\2\2\2\u00eb\u00ec\t\30\2\2\u00ec\60")
        buf.write("\3\2\2\2\u00ed\u00ee\t\31\2\2\u00ee\62\3\2\2\2\u00ef\u00f0")
        buf.write("\t\32\2\2\u00f0\64\3\2\2\2\u00f1\u00f2\t\33\2\2\u00f2")
        buf.write("\66\3\2\2\2\u00f3\u00f4\t\34\2\2\u00f48\3\2\2\2\u00f5")
        buf.write("\u00f6\t\35\2\2\u00f6:\3\2\2\2\u00f7\u00f8\5\5\3\2\u00f8")
        buf.write("\u00f9\5%\23\2\u00f9\u00fa\5\13\6\2\u00fa\u00fb\5\3\2")
        buf.write("\2\u00fb\u00fc\5\27\f\2\u00fc<\3\2\2\2\u00fd\u00fe\5\7")
        buf.write("\4\2\u00fe\u00ff\5\37\20\2\u00ff\u0100\5\35\17\2\u0100")
        buf.write("\u0101\5)\25\2\u0101\u0102\5\23\n\2\u0102\u0103\5\35\17")
        buf.write("\2\u0103\u0104\5+\26\2\u0104\u0105\5\13\6\2\u0105>\3\2")
        buf.write("\2\2\u0106\u0107\5\r\7\2\u0107\u0108\5\37\20\2\u0108\u0109")
        buf.write("\5%\23\2\u0109@\3\2\2\2\u010a\u010b\5)\25\2\u010b\u010c")
        buf.write("\5\37\20\2\u010cB\3\2\2\2\u010d\u010e\5\t\5\2\u010e\u010f")
        buf.write("\5\37\20\2\u010f\u0110\5/\30\2\u0110\u0111\5\35\17\2\u0111")
        buf.write("\u0112\5)\25\2\u0112\u0113\5\37\20\2\u0113D\3\2\2\2\u0114")
        buf.write("\u0115\5\t\5\2\u0115\u0116\5\37\20\2\u0116F\3\2\2\2\u0117")
        buf.write("\u0118\5\23\n\2\u0118\u0119\5\r\7\2\u0119H\3\2\2\2\u011a")
        buf.write("\u011b\5)\25\2\u011b\u011c\5\21\t\2\u011c\u011d\5\13\6")
        buf.write("\2\u011d\u011e\5\35\17\2\u011eJ\3\2\2\2\u011f\u0120\5")
        buf.write("\13\6\2\u0120\u0121\5\31\r\2\u0121\u0122\5\'\24\2\u0122")
        buf.write("\u0123\5\13\6\2\u0123L\3\2\2\2\u0124\u0125\5%\23\2\u0125")
        buf.write("\u0126\5\13\6\2\u0126\u0127\5)\25\2\u0127\u0128\5+\26")
        buf.write("\2\u0128\u0129\5%\23\2\u0129\u012a\5\35\17\2\u012aN\3")
        buf.write("\2\2\2\u012b\u012c\5/\30\2\u012c\u012d\5\21\t\2\u012d")
        buf.write("\u012e\5\23\n\2\u012e\u012f\5\31\r\2\u012f\u0130\5\13")
        buf.write("\6\2\u0130P\3\2\2\2\u0131\u0132\5/\30\2\u0132\u0133\5")
        buf.write("\23\n\2\u0133\u0134\5)\25\2\u0134\u0135\5\21\t\2\u0135")
        buf.write("R\3\2\2\2\u0136\u0137\5\5\3\2\u0137\u0138\5\13\6\2\u0138")
        buf.write("\u0139\5\17\b\2\u0139\u013a\5\23\n\2\u013a\u013b\5\35")
        buf.write("\17\2\u013bT\3\2\2\2\u013c\u013d\5\13\6\2\u013d\u013e")
        buf.write("\5\35\17\2\u013e\u013f\5\t\5\2\u013fV\3\2\2\2\u0140\u0141")
        buf.write("\5\r\7\2\u0141\u0142\5+\26\2\u0142\u0143\5\35\17\2\u0143")
        buf.write("\u0144\5\7\4\2\u0144\u0145\5)\25\2\u0145\u0146\5\23\n")
        buf.write("\2\u0146\u0147\5\37\20\2\u0147\u0148\5\35\17\2\u0148X")
        buf.write("\3\2\2\2\u0149\u014a\5!\21\2\u014a\u014b\5%\23\2\u014b")
        buf.write("\u014c\5\37\20\2\u014c\u014d\5\7\4\2\u014d\u014e\5\13")
        buf.write("\6\2\u014e\u014f\5\t\5\2\u014f\u0150\5+\26\2\u0150\u0151")
        buf.write("\5%\23\2\u0151\u0152\5\13\6\2\u0152Z\3\2\2\2\u0153\u0154")
        buf.write("\5-\27\2\u0154\u0155\5\3\2\2\u0155\u0156\5%\23\2\u0156")
        buf.write("\\\3\2\2\2\u0157\u0158\5\37\20\2\u0158\u0159\5\r\7\2\u0159")
        buf.write("^\3\2\2\2\u015a\u015b\5\5\3\2\u015b\u015c\5\37\20\2\u015c")
        buf.write("\u015d\5\37\20\2\u015d\u015e\5\31\r\2\u015e\u015f\5\13")
        buf.write("\6\2\u015f\u0160\5\3\2\2\u0160\u0161\5\35\17\2\u0161`")
        buf.write("\3\2\2\2\u0162\u0163\5%\23\2\u0163\u0164\5\13\6\2\u0164")
        buf.write("\u0165\5\3\2\2\u0165\u0166\5\31\r\2\u0166b\3\2\2\2\u0167")
        buf.write("\u0168\5\23\n\2\u0168\u0169\5\35\17\2\u0169\u016a\5)\25")
        buf.write("\2\u016a\u016b\5\13\6\2\u016b\u016c\5\17\b\2\u016c\u016d")
        buf.write("\5\13\6\2\u016d\u016e\5%\23\2\u016ed\3\2\2\2\u016f\u0170")
        buf.write("\5\'\24\2\u0170\u0171\5)\25\2\u0171\u0172\5%\23\2\u0172")
        buf.write("\u0173\5\23\n\2\u0173\u0174\5\35\17\2\u0174\u0175\5\17")
        buf.write("\b\2\u0175f\3\2\2\2\u0176\u0177\5\3\2\2\u0177\u0178\5")
        buf.write("%\23\2\u0178\u0179\5%\23\2\u0179\u017a\5\3\2\2\u017a\u017b")
        buf.write("\5\63\32\2\u017bh\3\2\2\2\u017c\u017d\7-\2\2\u017dj\3")
        buf.write("\2\2\2\u017e\u017f\7/\2\2\u017fl\3\2\2\2\u0180\u0181\7")
        buf.write(",\2\2\u0181n\3\2\2\2\u0182\u0183\7\61\2\2\u0183p\3\2\2")
        buf.write("\2\u0184\u0185\5\35\17\2\u0185\u0186\5\37\20\2\u0186\u0187")
        buf.write("\5)\25\2\u0187r\3\2\2\2\u0188\u0189\5\33\16\2\u0189\u018a")
        buf.write("\5\37\20\2\u018a\u018b\5\t\5\2\u018bt\3\2\2\2\u018c\u018d")
        buf.write("\5\37\20\2\u018d\u018e\5%\23\2\u018ev\3\2\2\2\u018f\u0190")
        buf.write("\5\3\2\2\u0190\u0191\5\35\17\2\u0191\u0192\5\t\5\2\u0192")
        buf.write("x\3\2\2\2\u0193\u0194\5\37\20\2\u0194\u0196\5%\23\2\u0195")
        buf.write("\u0197\t\36\2\2\u0196\u0195\3\2\2\2\u0197\u0198\3\2\2")
        buf.write("\2\u0198\u0196\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u019a")
        buf.write("\3\2\2\2\u019a\u019b\5\13\6\2\u019b\u019c\5\31\r\2\u019c")
        buf.write("\u019d\5\'\24\2\u019d\u019e\5\13\6\2\u019ez\3\2\2\2\u019f")
        buf.write("\u01a0\5\3\2\2\u01a0\u01a1\5\35\17\2\u01a1\u01a3\5\t\5")
        buf.write("\2\u01a2\u01a4\t\36\2\2\u01a3\u01a2\3\2\2\2\u01a4\u01a5")
        buf.write("\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6")
        buf.write("\u01a7\3\2\2\2\u01a7\u01a8\5)\25\2\u01a8\u01a9\5\21\t")
        buf.write("\2\u01a9\u01aa\5\13\6\2\u01aa\u01ab\5\35\17\2\u01ab|\3")
        buf.write("\2\2\2\u01ac\u01ad\7?\2\2\u01ad~\3\2\2\2\u01ae\u01af\7")
        buf.write(">\2\2\u01af\u01b0\7@\2\2\u01b0\u0080\3\2\2\2\u01b1\u01b2")
        buf.write("\7>\2\2\u01b2\u0082\3\2\2\2\u01b3\u01b4\7@\2\2\u01b4\u0084")
        buf.write("\3\2\2\2\u01b5\u01b6\7>\2\2\u01b6\u01b7\7?\2\2\u01b7\u0086")
        buf.write("\3\2\2\2\u01b8\u01b9\7@\2\2\u01b9\u01ba\7?\2\2\u01ba\u0088")
        buf.write("\3\2\2\2\u01bb\u01bc\5\t\5\2\u01bc\u01bd\5\23\n\2\u01bd")
        buf.write("\u01be\5-\27\2\u01be\u008a\3\2\2\2\u01bf\u01c0\7<\2\2")
        buf.write("\u01c0\u01c1\7?\2\2\u01c1\u008c\3\2\2\2\u01c2\u01c3\7")
        buf.write("]\2\2\u01c3\u008e\3\2\2\2\u01c4\u01c5\7_\2\2\u01c5\u0090")
        buf.write("\3\2\2\2\u01c6\u01c7\7*\2\2\u01c7\u0092\3\2\2\2\u01c8")
        buf.write("\u01c9\7+\2\2\u01c9\u0094\3\2\2\2\u01ca\u01cb\7=\2\2\u01cb")
        buf.write("\u0096\3\2\2\2\u01cc\u01cd\7.\2\2\u01cd\u0098\3\2\2\2")
        buf.write("\u01ce\u01cf\7<\2\2\u01cf\u009a\3\2\2\2\u01d0\u01d1\7")
        buf.write("\"\2\2\u01d1\u01d2\7\60\2\2\u01d2\u01d3\7\60\2\2\u01d3")
        buf.write("\u01d4\7\"\2\2\u01d4\u009c\3\2\2\2\u01d5\u01d6\7*\2\2")
        buf.write("\u01d6\u01d7\7,\2\2\u01d7\u009e\3\2\2\2\u01d8\u01d9\7")
        buf.write(",\2\2\u01d9\u01da\7+\2\2\u01da\u00a0\3\2\2\2\u01db\u01dc")
        buf.write("\7}\2\2\u01dc\u00a2\3\2\2\2\u01dd\u01de\7\177\2\2\u01de")
        buf.write("\u00a4\3\2\2\2\u01df\u01e1\5\67\34\2\u01e0\u01df\3\2\2")
        buf.write("\2\u01e1\u01e2\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e2\u01e3")
        buf.write("\3\2\2\2\u01e3\u00a6\3\2\2\2\u01e4\u01e6\5\67\34\2\u01e5")
        buf.write("\u01e4\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7\u01e5\3\2\2\2")
        buf.write("\u01e7\u01e8\3\2\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01ed\7")
        buf.write("\60\2\2\u01ea\u01ec\5\67\34\2\u01eb\u01ea\3\2\2\2\u01ec")
        buf.write("\u01ef\3\2\2\2\u01ed\u01eb\3\2\2\2\u01ed\u01ee\3\2\2\2")
        buf.write("\u01ee\u0220\3\2\2\2\u01ef\u01ed\3\2\2\2\u01f0\u01f2\5")
        buf.write("\67\34\2\u01f1\u01f0\3\2\2\2\u01f2\u01f5\3\2\2\2\u01f3")
        buf.write("\u01f1\3\2\2\2\u01f3\u01f4\3\2\2\2\u01f4\u01f6\3\2\2\2")
        buf.write("\u01f5\u01f3\3\2\2\2\u01f6\u01f8\7\60\2\2\u01f7\u01f9")
        buf.write("\5\67\34\2\u01f8\u01f7\3\2\2\2\u01f9\u01fa\3\2\2\2\u01fa")
        buf.write("\u01f8\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u0220\3\2\2\2")
        buf.write("\u01fc\u01fe\5\67\34\2\u01fd\u01fc\3\2\2\2\u01fe\u01ff")
        buf.write("\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff\u0200\3\2\2\2\u0200")
        buf.write("\u0201\3\2\2\2\u0201\u0203\t\6\2\2\u0202\u0204\5k\66\2")
        buf.write("\u0203\u0202\3\2\2\2\u0203\u0204\3\2\2\2\u0204\u0206\3")
        buf.write("\2\2\2\u0205\u0207\5\67\34\2\u0206\u0205\3\2\2\2\u0207")
        buf.write("\u0208\3\2\2\2\u0208\u0206\3\2\2\2\u0208\u0209\3\2\2\2")
        buf.write("\u0209\u0220\3\2\2\2\u020a\u020c\5\67\34\2\u020b\u020a")
        buf.write("\3\2\2\2\u020c\u020f\3\2\2\2\u020d\u020b\3\2\2\2\u020d")
        buf.write("\u020e\3\2\2\2\u020e\u0210\3\2\2\2\u020f\u020d\3\2\2\2")
        buf.write("\u0210\u0212\7\60\2\2\u0211\u0213\5\67\34\2\u0212\u0211")
        buf.write("\3\2\2\2\u0213\u0214\3\2\2\2\u0214\u0212\3\2\2\2\u0214")
        buf.write("\u0215\3\2\2\2\u0215\u0216\3\2\2\2\u0216\u0218\t\6\2\2")
        buf.write("\u0217\u0219\5k\66\2\u0218\u0217\3\2\2\2\u0218\u0219\3")
        buf.write("\2\2\2\u0219\u021b\3\2\2\2\u021a\u021c\5\67\34\2\u021b")
        buf.write("\u021a\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021b\3\2\2\2")
        buf.write("\u021d\u021e\3\2\2\2\u021e\u0220\3\2\2\2\u021f\u01e5\3")
        buf.write("\2\2\2\u021f\u01f3\3\2\2\2\u021f\u01fd\3\2\2\2\u021f\u020d")
        buf.write("\3\2\2\2\u0220\u00a8\3\2\2\2\u0221\u0222\t\25\2\2\u0222")
        buf.write("\u0223\t\23\2\2\u0223\u0224\t\26\2\2\u0224\u022b\t\6\2")
        buf.write("\2\u0225\u0226\t\7\2\2\u0226\u0227\t\2\2\2\u0227\u0228")
        buf.write("\t\r\2\2\u0228\u0229\t\24\2\2\u0229\u022b\t\6\2\2\u022a")
        buf.write("\u0221\3\2\2\2\u022a\u0225\3\2\2\2\u022b\u00aa\3\2\2\2")
        buf.write("\u022c\u0232\7$\2\2\u022d\u0231\n\37\2\2\u022e\u022f\7")
        buf.write("^\2\2\u022f\u0231\t \2\2\u0230\u022d\3\2\2\2\u0230\u022e")
        buf.write("\3\2\2\2\u0231\u0234\3\2\2\2\u0232\u0230\3\2\2\2\u0232")
        buf.write("\u0233\3\2\2\2\u0233\u0235\3\2\2\2\u0234\u0232\3\2\2\2")
        buf.write("\u0235\u0236\7$\2\2\u0236\u0237\bV\2\2\u0237\u00ac\3\2")
        buf.write("\2\2\u0238\u0239\7*\2\2\u0239\u023a\7,\2\2\u023a\u023e")
        buf.write("\3\2\2\2\u023b\u023d\13\2\2\2\u023c\u023b\3\2\2\2\u023d")
        buf.write("\u0240\3\2\2\2\u023e\u023f\3\2\2\2\u023e\u023c\3\2\2\2")
        buf.write("\u023f\u0241\3\2\2\2\u0240\u023e\3\2\2\2\u0241\u0242\7")
        buf.write(",\2\2\u0242\u0243\7+\2\2\u0243\u0244\3\2\2\2\u0244\u0245")
        buf.write("\bW\3\2\u0245\u00ae\3\2\2\2\u0246\u024a\7}\2\2\u0247\u0249")
        buf.write("\13\2\2\2\u0248\u0247\3\2\2\2\u0249\u024c\3\2\2\2\u024a")
        buf.write("\u024b\3\2\2\2\u024a\u0248\3\2\2\2\u024b\u024d\3\2\2\2")
        buf.write("\u024c\u024a\3\2\2\2\u024d\u024e\7\177\2\2\u024e\u024f")
        buf.write("\3\2\2\2\u024f\u0250\bX\3\2\u0250\u00b0\3\2\2\2\u0251")
        buf.write("\u0252\7\61\2\2\u0252\u0253\7\61\2\2\u0253\u0257\3\2\2")
        buf.write("\2\u0254\u0256\n!\2\2\u0255\u0254\3\2\2\2\u0256\u0259")
        buf.write("\3\2\2\2\u0257\u0255\3\2\2\2\u0257\u0258\3\2\2\2\u0258")
        buf.write("\u025a\3\2\2\2\u0259\u0257\3\2\2\2\u025a\u025b\bY\3\2")
        buf.write("\u025b\u00b2\3\2\2\2\u025c\u025e\t\"\2\2\u025d\u025c\3")
        buf.write("\2\2\2\u025e\u025f\3\2\2\2\u025f\u025d\3\2\2\2\u025f\u0260")
        buf.write("\3\2\2\2\u0260\u0261\3\2\2\2\u0261\u0262\bZ\3\2\u0262")
        buf.write("\u00b4\3\2\2\2\u0263\u0266\7a\2\2\u0264\u0266\59\35\2")
        buf.write("\u0265\u0263\3\2\2\2\u0265\u0264\3\2\2\2\u0266\u026c\3")
        buf.write("\2\2\2\u0267\u026b\5\67\34\2\u0268\u026b\7a\2\2\u0269")
        buf.write("\u026b\59\35\2\u026a\u0267\3\2\2\2\u026a\u0268\3\2\2\2")
        buf.write("\u026a\u0269\3\2\2\2\u026b\u026e\3\2\2\2\u026c\u026a\3")
        buf.write("\2\2\2\u026c\u026d\3\2\2\2\u026d\u00b6\3\2\2\2\u026e\u026c")
        buf.write("\3\2\2\2\u026f\u0273\7$\2\2\u0270\u0272\n#\2\2\u0271\u0270")
        buf.write("\3\2\2\2\u0272\u0275\3\2\2\2\u0273\u0271\3\2\2\2\u0273")
        buf.write("\u0274\3\2\2\2\u0274\u0276\3\2\2\2\u0275\u0273\3\2\2\2")
        buf.write("\u0276\u027a\t$\2\2\u0277\u0279\n#\2\2\u0278\u0277\3\2")
        buf.write("\2\2\u0279\u027c\3\2\2\2\u027a\u0278\3\2\2\2\u027a\u027b")
        buf.write("\3\2\2\2\u027b\u027d\3\2\2\2\u027c\u027a\3\2\2\2\u027d")
        buf.write("\u027e\7$\2\2\u027e\u027f\b\\\4\2\u027f\u00b8\3\2\2\2")
        buf.write("\u0280\u0284\7$\2\2\u0281\u0283\n#\2\2\u0282\u0281\3\2")
        buf.write("\2\2\u0283\u0286\3\2\2\2\u0284\u0282\3\2\2\2\u0284\u0285")
        buf.write("\3\2\2\2\u0285\u0287\3\2\2\2\u0286\u0284\3\2\2\2\u0287")
        buf.write("\u028b\7^\2\2\u0288\u028a\n%\2\2\u0289\u0288\3\2\2\2\u028a")
        buf.write("\u028d\3\2\2\2\u028b\u0289\3\2\2\2\u028b\u028c\3\2\2\2")
        buf.write("\u028c\u028e\3\2\2\2\u028d\u028b\3\2\2\2\u028e\u028f\7")
        buf.write("$\2\2\u028f\u0290\b]\5\2\u0290\u00ba\3\2\2\2\u0291\u0295")
        buf.write("\7$\2\2\u0292\u0294\n&\2\2\u0293\u0292\3\2\2\2\u0294\u0297")
        buf.write("\3\2\2\2\u0295\u0293\3\2\2\2\u0295\u0296\3\2\2\2\u0296")
        buf.write("\u0298\3\2\2\2\u0297\u0295\3\2\2\2\u0298\u0299\b^\6\2")
        buf.write("\u0299\u00bc\3\2\2\2\u029a\u029b\13\2\2\2\u029b\u029c")
        buf.write("\b_\7\2\u029c\u00be\3\2\2\2!\2\u0198\u01a5\u01e2\u01e7")
        buf.write("\u01ed\u01f3\u01fa\u01ff\u0203\u0208\u020d\u0214\u0218")
        buf.write("\u021d\u021f\u022a\u0230\u0232\u023e\u024a\u0257\u025f")
        buf.write("\u0265\u026a\u026c\u0273\u027a\u0284\u028b\u0295\b\3V")
        buf.write("\2\b\2\2\3\\\3\3]\4\3^\5\3_\6")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    BREAK = 1
    CONTINUE = 2
    FOR = 3
    TO = 4
    DOWNTO = 5
    DO = 6
    IF = 7
    THEN = 8
    ELSE = 9
    RETURN = 10
    WHILE = 11
    WITH = 12
    BEGIN = 13
    END = 14
    FUNC = 15
    PROC = 16
    VAR = 17
    OF = 18
    BOOLEAN = 19
    REAL = 20
    INT = 21
    STRING = 22
    ARRAY = 23
    ADD = 24
    SUB = 25
    MUL = 26
    DIVIDE = 27
    NOT = 28
    MOD = 29
    OR = 30
    AND = 31
    ORELSE = 32
    ANDTH = 33
    EQ = 34
    NEQ = 35
    LT = 36
    GT = 37
    LTEQ = 38
    GTEQ = 39
    INTDIV = 40
    ASIGN = 41
    LS = 42
    RS = 43
    LB = 44
    RB = 45
    SEMI = 46
    COMMA = 47
    COLON = 48
    TWODOTS = 49
    LCMT = 50
    RCMT = 51
    LP = 52
    RP = 53
    IntLit = 54
    RealLit = 55
    BoolLit = 56
    StrLit = 57
    BLOCKCMT1 = 58
    BLOCKCMT2 = 59
    LINECMT = 60
    WS = 61
    ID = 62
    ILLEGAL_CHAR = 63
    ILLEGAL_ESCAPE = 64
    UNCLOSE_STRING = 65
    ERROR_CHAR = 66

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'+'", "'-'", "'*'", "'/'", "'='", "'<>'", "'<'", "'>'", "'<='", 
            "'>='", "':='", "'['", "']'", "'('", "')'", "';'", "','", "':'", 
            "' .. '", "'(*'", "'*)'", "'{'", "'}'" ]

    symbolicNames = [ "<INVALID>",
            "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", 
            "ELSE", "RETURN", "WHILE", "WITH", "BEGIN", "END", "FUNC", "PROC", 
            "VAR", "OF", "BOOLEAN", "REAL", "INT", "STRING", "ARRAY", "ADD", 
            "SUB", "MUL", "DIVIDE", "NOT", "MOD", "OR", "AND", "ORELSE", 
            "ANDTH", "EQ", "NEQ", "LT", "GT", "LTEQ", "GTEQ", "INTDIV", 
            "ASIGN", "LS", "RS", "LB", "RB", "SEMI", "COMMA", "COLON", "TWODOTS", 
            "LCMT", "RCMT", "LP", "RP", "IntLit", "RealLit", "BoolLit", 
            "StrLit", "BLOCKCMT1", "BLOCKCMT2", "LINECMT", "WS", "ID", "ILLEGAL_CHAR", 
            "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "ERROR_CHAR" ]

    ruleNames = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", 
                  "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", 
                  "W", "X", "Y", "Z", "DIGIT", "CHARACTER", "BREAK", "CONTINUE", 
                  "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", "ELSE", "RETURN", 
                  "WHILE", "WITH", "BEGIN", "END", "FUNC", "PROC", "VAR", 
                  "OF", "BOOLEAN", "REAL", "INT", "STRING", "ARRAY", "ADD", 
                  "SUB", "MUL", "DIVIDE", "NOT", "MOD", "OR", "AND", "ORELSE", 
                  "ANDTH", "EQ", "NEQ", "LT", "GT", "LTEQ", "GTEQ", "INTDIV", 
                  "ASIGN", "LS", "RS", "LB", "RB", "SEMI", "COMMA", "COLON", 
                  "TWODOTS", "LCMT", "RCMT", "LP", "RP", "IntLit", "RealLit", 
                  "BoolLit", "StrLit", "BLOCKCMT1", "BLOCKCMT2", "LINECMT", 
                  "WS", "ID", "ILLEGAL_CHAR", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", 
                  "ERROR_CHAR" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[84] = self.StrLit_action 
            actions[90] = self.ILLEGAL_CHAR_action 
            actions[91] = self.ILLEGAL_ESCAPE_action 
            actions[92] = self.UNCLOSE_STRING_action 
            actions[93] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def StrLit_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            self.text=self.text[1:-1]
     

    def ILLEGAL_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:

            pos = tab = self.text.find('\t')
            sq = self.text.find('\'')
            if tab == -1:
                pos = sq
            raise IllegalCharInString(self.text[1: pos +1])
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

            slash = self.text.find('\\')
            raise IllegalEscapeInString(self.text[1: slash+2])
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
            raise UnclosedString(self.text[1:])
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 4:
            raise ErrorToken(self.text)
     


