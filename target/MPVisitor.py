# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#decls.
    def visitDecls(self, ctx:MPParser.DeclsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#varDecl.
    def visitVarDecl(self, ctx:MPParser.VarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#varList.
    def visitVarList(self, ctx:MPParser.VarListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#retType.
    def visitRetType(self, ctx:MPParser.RetTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#priType.
    def visitPriType(self, ctx:MPParser.PriTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#compoType.
    def visitCompoType(self, ctx:MPParser.CompoTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcDecl.
    def visitFuncDecl(self, ctx:MPParser.FuncDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paramList.
    def visitParamList(self, ctx:MPParser.ParamListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procDecl.
    def visitProcDecl(self, ctx:MPParser.ProcDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#body.
    def visitBody(self, ctx:MPParser.BodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr.
    def visitExpr(self, ctx:MPParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ifStmt.
    def visitIfStmt(self, ctx:MPParser.IfStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ifElse.
    def visitIfElse(self, ctx:MPParser.IfElseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ifNoElse.
    def visitIfNoElse(self, ctx:MPParser.IfNoElseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stmt.
    def visitStmt(self, ctx:MPParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#asignStmt.
    def visitAsignStmt(self, ctx:MPParser.AsignStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#lhs.
    def visitLhs(self, ctx:MPParser.LhsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#whileDo.
    def visitWhileDo(self, ctx:MPParser.WhileDoContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#forloop.
    def visitForloop(self, ctx:MPParser.ForloopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#withDo.
    def visitWithDo(self, ctx:MPParser.WithDoContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#retNoExp.
    def visitRetNoExp(self, ctx:MPParser.RetNoExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#retWExp.
    def visitRetWExp(self, ctx:MPParser.RetWExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#dataTypeLit.
    def visitDataTypeLit(self, ctx:MPParser.DataTypeLitContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcCall.
    def visitFuncCall(self, ctx:MPParser.FuncCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procCall.
    def visitProcCall(self, ctx:MPParser.ProcCallContext):
        return self.visitChildren(ctx)



del MPParser